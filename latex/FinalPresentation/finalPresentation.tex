% !TeX program = luatex
\documentclass[]{beamer}
\usepackage[backend=biber, style=authortitle, doi=false,isbn=false,url=false,eprint=false]{biblatex}
\addbibresource{../biblioThesis.bib}

\beamertemplatenavigationsymbolsempty
%\includeonlyframes{do}

%\includeonly{results}


%Inputs
\input{../preambleMath.tex}
\input{../preambleTables.tex}
\input{../preamblePlotting.tex}
%\usetikzlibrary{external}
%\tikzexternalize
%\tikzsetexternalprefix{./build/}
\input{../preambleColors.tex}
\DeclarePairedDelimiterX{\rvect}[1]{[}{]}{\,\makervect{#1}\,}
\ExplSyntaxOn
\NewDocumentCommand{\makervect}{m}
{
	\seq_set_split:Nnn \l_tmpa_seq { , } { #1 }
	\setlength\arraycolsep{3pt}\begin{matrix}
		\seq_use:Nn \l_tmpa_seq { & }
	\end{matrix}
}
\ExplSyntaxOff

\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}

%\usetheme{Madrid}
\usetheme{Frankfurt}
%\usetheme{kuleuven2}

%\definecolor{WITcolor}{cmyk}{0.9, 0.94, 0.02, 0.07}
%\setbeamercolor{section in head/foot}{bg=WITcolor}

\AtBeginSection[]
{
	\begin{frame}
		\frametitle{Table of Contents}
		\tableofcontents[currentsection]
	\end{frame}
}

\begin{document}
\title[Risk-averse MPC of bioreactors]{Risk-averse model predictive control of bioreactors}
\subtitle{Thesis defence}
\author[]{Maurits Descamps\\[\baselineskip] 
	\parbox[t]{2.5cm}{\raggedleft
		Supervisors\texorpdfstring{\\[2\baselineskip]}{}
		Mentors
	}
	\hspace{0.2cm}
	\parbox[t]{4cm}{\raggedright
		Prof. Jan Van Impe
		Prof. Pantelis Sopasakis\texorpdfstring{\\[\baselineskip]}{}
		Dr. Satyajeet Bhonsale
		Dr. Mihaela Sbarciog
	}
}
\institute[]{
	Departent of Computer Science\\
	KU Leuven}


\begin{frame}[plain,noframenumbering]
	\titlepage
\end{frame}

% Table of Contents
\begin{frame}{Outline}
	\hfill	{\large \parbox{.961\textwidth}{\tableofcontents[hideothersubsections]}}
\end{frame}

\include{bioreactors}
\include{rroc}
\include{rc}
\include{results}
\include{conclusion}

\begin{frame}[t,allowframebreaks]{References}
	\printbibliography
\end{frame}

\begin{frame}[plain,c]
	\begin{center}
		\Huge Questions?
	\end{center}
\end{frame}

\appendix
\section{Conditional risk mappings}
\begin{frame}{Conditional risk mappings}
		\begin{columns}
		\begin{column}{0.55\linewidth}
			Definitions:
			\begin{itemize}[<+->]
				\item Each node has a unique ID $i$\\
				$\rightarrow$ cost at node $i$: $Z^i$
				\item Cost at children of $i$: $Z^{[i]}=(Z^{i_+})_{i_+ \in \child(i)},$
				\item Cost at time $t$: $Z_t=(Z^i)_{i \in \nodes(t)}$
				\item Risk measure on $\child(i)$: $\rho^i \colon \REAL^{\abs{\child(i)}}\to \REAL$
				\item Conditional risk mapping $\rhocond{t} \colon \REAL^{\abs{\nodes(t+1)}} \to \REAL^{\abs{\nodes(t)}}$,
				$\rhocond{t}[Z_{t+1}] = \left(\rho^i\left[Z^{[i]}\right]\right)_{i \in \nodes(t)}$.
			\end{itemize}
		\end{column}
		\hspace*{-2cm}
		\begin{column}{0.45\linewidth}
			\input{../Figures/treeRisk2.tikz}
		\end{column}
	\end{columns}
\end{frame}

\section[Risk constraints]{Overapproximating nested risk constraints}

\begin{frame}{Overapproximating $\overline{\AVAR}_\beta$ with $\AVAR_{\beta^\star}$}
	Task: find $\beta^\star$ such that
	\begin{equation*}
		\overline{\AVAR}_\beta[G] \leq \AVAR_{\beta^\star}[G] \quad \forall G,
	\end{equation*}
	or equivalently
	\begin{equation*}
		\epi \AVAR_{\beta^\star} \subseteq \epi \overline{\AVAR}_\beta.
	\end{equation*}
	This can be checked for a given $\beta^\star$ by constructing
	\begin{align*}
		\epi \AVAR_{\beta^\star} &= \left\{x\in\REAL^{n+1} \Middle Gx\leq g\right\}\\
		\epi \overline{\AVAR}_\beta &= \left\{x\in\REAL^{n+1} \Middle Hx\leq h\right\}
	\end{align*} 
	and checking whether
	\begin{equation*}
		\max_{Gy\leq g} H_i^\top x \leq h_i
	\end{equation*} 
	holds for all $i$.
\end{frame}

\begin{frame}{Bisection algorithm}
	\begin{algorithm}[H]
		\DontPrintSemicolon
		\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
		\Input{Tolerance $\epsilon$, }
		\Output{$\beta^\star$}
		Construct $\overline{\AVAR}_\beta$\;
		$a\leftarrow 0$\;
		$b\leftarrow 1$\;
		\While{$b-a>\epsilon$}{
			$\beta^\star\leftarrow \frac{a+b}{2}$\;
			Construct $\epi \AVAR_{\beta^\star}$\;
			\eIf{$\epi \AVAR_{\beta^\star} \subseteq \epi \bar{\rho_t}$}{
				$a\leftarrow \beta^\star$\;
			}{
				$b\leftarrow \beta^\star$\;
			}
		}
	\end{algorithm}
\end{frame}

\begin{frame}{Constructing the epigraphs}
	From the definition of $\AVAR$ we have
	\begin{align*}
		\epi \AVAR_{_\beta} &= 
		\left\{(Y,\gamma)\in \REAL^{n+1} \Middle \boldinf_t  t+ \frac{1}{\beta}\EX[(Y-t)_+]\leq \gamma\right\}\\
		&= \left\{(Y,\gamma)\in \REAL^{n+1} \Middle
		\renewcommand{\arraystretch}{1.2}
		\begin{tabular}{@{}l@{}}
			$\exists t\in \REAL,\; \xi \in\REAL_{\geq0}^n\colon$\\
			$Y-t\leq \xi,\; t+\frac{1}{\beta}\pi^\top \xi\leq \gamma$
		\end{tabular}
		\right\},
	\end{align*}
	which can be written as 
	\begin{equation*}
		\epi \AVAR_{\beta} = \{x = (Y,\gamma, t,\xi)\in\REAL^{2n+2}| G'x\leq g'\}.
	\end{equation*}
	Eliminating $t$ and $\xi$, for example via \textit{Fourier-Motzkin elimination}, then gives 
	\begin{equation*}
		\epi \overline{\AVAR}_\beta = \left\{x\in\REAL^{n+1} \Middle Hx\leq h\right\}
	\end{equation*}
\end{frame}

\begin{frame}{Constructing the epigraphs}
		Similar approach for $\overline{\AVAR}$, based on
		\begin{equation*}\label{eq:epi_nested}
		\epi \bar{r}_t =
		\left\{(Y_{t+1},Y_0) \in  \REAL^{\abs{\nodes(t+1)}+1}
		\Middle
		\begin{tabular}{@{}l@{}}
			$\exists (Y_j)_{j\in \NAT_{[1,t]}} ,\, Y_j \in \REAL^{\abs{\nodes(j)}},$\\
			$(Y_{j+1}, Y_j) \in \epi \rhocond{j}, j\in \NAT_{[0,t]}$
		\end{tabular}
		\right\}.
	\end{equation*}
\end{frame}


\begin{frame}{Results}
	\begin{figure}
		\centering
		\begingroup
		\tikzset{every picture/.style={scale=0.9}}%
		%\tikzset{external/export next=false}
		\input{../Figures/alphaStar.tikz}
		\endgroup
		\caption{$\beta^\star$ for different nested risk measures $\overline{\AVAR}_\beta$ and different branching horizons with conditional probability vector $\pi^\top = \rvect{0.25,0.5,0.25}$.}
	\end{figure}
\end{frame}
\end{document}