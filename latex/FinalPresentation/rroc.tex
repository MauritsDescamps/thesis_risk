\section{Dealing with uncertainty}
\begin{frame}{Two popular approaches}
	\begin{figure}\centering
		\input{../Figures/worstcase.tikz}
		\vspace{-1cm}
	\end{figure}
	\begin{columns}[t]
		\begin{column}<2->{0.45\textwidth}
			\begin{block}{Stochastic}
				\begin{itemize}
					\item Minimize expected cost
					\item Downside: requires exact knowledge of distribution
				\end{itemize}
			\end{block}
		\end{column}
		\begin{column}<3->{0.45\textwidth}
			\begin{block}{Worst-case}
				\begin{itemize}
					\item Minimize worst-case cost\\
					\item Downside: conservative
				\end{itemize}
			\end{block}
		\end{column}
	\end{columns}
	\begin{block}<4>{}
		\centering
		Can we do something in between?
	\end{block}
\end{frame}

\begin{frame}{Risk measures}
	\begin{itemize}
		\item A risk measure $\rho$ maps random variables  $Z$ to real numbers\\
		$\rightarrow$ just like the average and maximum operators
		\item Interpolation between stochastic and robust approach
	\end{itemize}
	\begin{figure}\centering
		\input{../Figures/risk.tikz}
	\end{figure}
	\vspace{-3mm}
	\begin{block}<2->{Assumption}
		Sample space $\Omega = \{\omega_i\}_{i=1}^n$, Probability vector $\pi$ with $\Prob[\{\omega_i\}]=\pi_i$, Discrete random variable $Z \colon \Omega \to \REAL$ with $Z(\omega_i) = Z_i$.
	\end{block}
\end{frame}

\begin{frame}{Coherent risk measures}
	Special class of risk measures with nice properties.
	\begin{block}{Requirements for coherent risk measures\footnotemark}
		\begin{enumerate}
			\itemsep1em
			\item Convexity:
			$\rho(\lambda Z+(1-\lambda)Z') \leq \lambda\rho(Z) + (1-\lambda)\rho(Z') \quad \forall \lambda\in [0,1]$
			\item Monotonicity: If $Z_i \leq Z_i'$ for all $i \iffalse \in \N_{[1,n]}\fi$, then $\rho(Z) \leq \rho(Z')$
			\item Translational equivariance: $\rho(Z+a)=\rho(Z)+a$ with $a\in\REAL$
			\item Positive homogeneity: $\rho(tZ)=t\rho(Z)$ with $0<t\in\REAL$
		\end{enumerate}
	\end{block}
	Popular choices: 
	\begin{itemize}
		\item Average value-at-risk ($\AVAR$)
		\item Entropic value-at-risk ($\EVAR$)
	\end{itemize}
	\footnotetext{\cite[Sec. 6.3]{SHAPIRO}}
\end{frame}

\begin{frame}{Average value-at-risk}
	\begin{itemize}[<+->]
		\item Definition: 
		\begin{equation*}
			\AVAR_{\alpha}(Z) \coloneqq \inf_{t\in \REAL}\{ t+\alpha^{-1} \EX[Z-t]_+\} \quad \text{with } 0 < \alpha \leq 1
		\end{equation*}
		\item Interpretation: Expected value of $100\alpha\%$ worst cases \footcite[Thm. 6.2]{SHAPIRO}
		\item Interpolation between Expectation based and worst-case
	\end{itemize}
	\vskip0pt plus 1filll
	\begin{figure}\centering
		%\fontsize{8pt}{9pt}\selectfont%
		\only<1-3|handout:1>{
			\input{../Figures/avar60.tikz}\\
		}
		\only<4|handout:2>{
			\input{../Figures/avar15.tikz}\\
		}
		\only<5|handout:3>{
			\input{../Figures/avar90.tikz}\\
		}
	\end{figure}
\end{frame}

\begin{frame}{Dual representation}
	All coherent risk measures can be written as \footcite[Thm. 6.5]{SHAPIRO}
	\begin{equation*}
		\rho[Z] = \boldmax_{\mu \in \ambiguity^\rho(\pi)}\EX_{\mu}[Z].
	\end{equation*}
	where $\ambiguity^\rho(\pi)$ is the \textit{ambiguity set}, which is a closed and convex set of probability vectors which contains $\pi$.\\
	\uncover<2->{Examples:}
	\begin{itemize}[<+(1)->]
		\item $\ambiguity^{\EX}(\pi)=\{\pi\}$,
		\item $\ambiguity^{\textrm{essmax}}(\pi)=\mathcal{D}_n$, with $\mathcal{D}_n$ the entire probability simplex,
		\item $\ambiguity^{\AVAR_\alpha} = \left\{\mu \in \REAL^n  \Middle \sum_{i=1}^n \mu_i=1, 0 \leq \alpha\mu \leq \pi\right\}$.
	\end{itemize}
\end{frame}

\begin{frame}{Dual representation}
	The ambiguity set of any coherent risk measure can be written using conic inequalities\footnote{A conic inequality $x \preccurlyeq_{\K} y$ is be interpreted as $y-x\in \K$.}:
	\begin{equation*}
		\rho[Z] = \boldmax_{\mu \in \REAL^n, \nu \in \REAL^r}\{\mu^\top Z \mid E\mu + F\nu \preccurlyeq_{\K}b\},
	\end{equation*}
	where $\K$ is a closed, convex cone.\\
	In the case of $\AVAR$, this can be done with regular inequalities:
	\begin{equation*}
		\AVAR_\alpha(\pi)= \boldmax_{\mu \in \REAL^n} \left\{\mu^\top Z \Middle
		\begin{bmatrix}
			1_n^\top\\
			-1_n^\top\\
			\alpha I_n\\
			-I_n
		\end{bmatrix}
		\mu \leq
		\begin{bmatrix}
			1\\
			-1\\
			\pi\\
			0_n
		\end{bmatrix}
		\right\},
	\end{equation*}
\end{frame}

\begin{frame}{Scenario trees}
	\begin{columns}[t]
		\begin{column}{0.5\linewidth}
			\begin{minipage}[t][5cm][c]{\textwidth}
				\only<1-2>{
					\input{../Figures/tree3.tikz}}
				\only<3->{
					\input{../Figures/tree3robust.tikz}}
			\end{minipage}
		\end{column}
		\begin{column}{0.5\linewidth}
			\begin{block}{}
				Assume $S_{in} \in \{\omega_1, \cdots \omega_n\}$\\
				at every time step.\\
				$\rightarrow$ Discrete prob. distribution
			\end{block}
		\end{column}
	\end{columns}
	\onslide<2->{
		\vspace*{-0.5cm}
		\begin{block}{\centering Trade off}
			\begin{minipage}{0.4\linewidth}
				\centering
				Good representation of real distribution\footnotemark
			\end{minipage}%
			\begin{minipage}{0.2\linewidth}
				\centering
				VS
			\end{minipage}%
			\begin{minipage}{0.4\linewidth}
				\centering
				Simple tree, low computational complexity
			\end{minipage}
		\end{block}
		\only<2->{\footnotetext{\cite{treeGeneration}}}
	}
\end{frame}


\begin{frame}{Risk-averse optimal control problem}
		\centering
		\begin{equation*}
			\boldminim_{u_0,\dots u_{N-1}} \quad \sum_{i=0}^{N-1} -P_{i+1}^2+ \frac{1}{2}\Delta u_i^2
		\end{equation*}
		\uncover<2->{
			\hspace{5.2cm}$\boldsymbol{\Bigg\downarrow}$\hspace{0.2cm}\parbox{5cm}{conditional risk mappings $\rhocond{t} \colon \REAL^{\abs{\nodes(t+1)}} \to \REAL^{\abs{\nodes(t)}}$}
			\begin{alignat*}{2}
				\boldminim_{u_0,\dots u_{N-1}} \quad&
					\begin{aligned}[t]
						\frac{1}{2}\Delta u_0+ \rho_{\mid 0} &\Bigg[-P_1^2 + \frac{1}{2}\Delta u_1
						+\rho_{\mid 1}  \bigg[-P_2^2 + \frac{1}{2}\Delta u_2\dots\\
						+\rho_{\mid N-2}  &\bigg[-P_{N-1}^2 + \frac{1}{2}\Delta u_{N-1}+
						\rho_{\mid N-1}  \big[-P_N^2\big]\Big]\dots \bigg] \Bigg]
				\end{aligned}&&
			\end{alignat*}
		}
\end{frame}

\begin{frame}{Risk constraints}
	What about random variables in the constraints?
	\begin{center}
		\begin{table}
			\centering
			\begin{tabular}{cl}
				$\alert{X_i} \leq X_{max}$&\\
				\onslide<2->{$\downarrow$&\\
					$P[X_i\geq X_{max}] \leq \alpha \quad$& 
					\begin{tabular}{@{}l@{}}
						Difficult to enforce, \\ leads to integer programming
					\end{tabular}\\}
				\onslide<3->{$\downarrow$&\\
					$\AVAR_\alpha[X_i] \leq X_{max} \quad$&
					\begin{tabular}{@{}l@{}}
						Easier to enforce due to\\ dual representation
					\end{tabular}\\}
				\onslide<4->{$\implies$&\\
					$P[X_i\geq X_{max}] \leq \alpha$&\\}
			\end{tabular}
		\end{table}
	\end{center}
	\vspace{-4mm}
	\uncover<5->{Two types:
	\begin{itemize}
		\item Multistage Nested Risk Constraints\\
		$\rightarrow$ Composition of conditional risk mappings
		\item Stagewise Risk constraints
	\end{itemize}}
\end{frame}


\begin{frame}{Risk-averse, risk-constrained Optimal Control Problem}
		Problem formulation:\footnote{Tractable reformulation using dual representation in \cite{SopasakisPantelis2019Rroc}.}
		\begin{alignat*}{2}
			\boldminim_{u_0,\dots u_{N-1}} \quad&
			\mathrlap{
				\begin{aligned}[t]
					\frac{1}{2}\Delta u_0+ \rho_{\mid 0} &\Bigg[-P_1^2 + \frac{1}{2}\Delta u_1
					+\rho_{\mid 1}  \bigg[-P_2^2 + \frac{1}{2}\Delta u_2\dots\\
					+\rho_{\mid N-2}  &\bigg[-P_{N-1}^2 + \frac{1}{2}\Delta u_{N-1}+
					\rho_{\mid N-1}  \big[-P_N^2\big]\Big]\dots \bigg] \Bigg]
			\end{aligned}}&&\\
			\st \quad & x_0 =  x \quad&&\\
			& x_{t+1} = x_{t} + \Delta t F(x_{t+1}, u_t, \Sin_{,t}) \quad&&\textrm{ for } t \in \NAT_{[0,N-1]}\\
			& r_t[X_t -3.7]\leq 0 \quad&&\textrm{ for } t \in \NAT_{[1,N]}\\
			&  \Delta u_t = u_t-u_{t-1} 													   \quad&&\textrm{ for } t \in \NAT_{[0,N-1]}\\
			& u_{-1} = u^\star_{0\mid \text{i-1}}.											&&
		\end{alignat*}
\end{frame}
