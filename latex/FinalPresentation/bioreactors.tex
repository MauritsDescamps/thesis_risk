\section{Problem setting}
\begin{frame}[label=do]{Fed-batch reactor}
	\begin{columns}
		\begin{column}{0.6\textwidth}
			\begin{itemize}
				\item Initial concentration of biomass $X$ and substrate $S$ 
				\item Biomass grows and consumes S\\
				$\rightarrow$ production of product $P$
				\item Continuous feed with substrate concentration $\Sin$
				\item Control input: feed rate $u$ $[\si{\liter/\hour}]$
				\item Goal: maximize $P$ at $T_{final}$
			\end{itemize}
		\end{column}
		\begin{column}{0.4\textwidth}
			%\tikzexternaldisable
			\input{../Figures/reactor-fed-batch.tikz}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Mathematical description}
	\begin{columns}
		\begin{column}{0.58\textwidth}
			\begin{block}{System dynamics\footnotemark}
				\begin{align*}
					\dot{X} &= \mu(S)X - \frac{u}{V}X,\\
					\dot{S} &= \frac{-\mu(S)}{Y_x}X - \frac{\nu(S)}{Y_p}X + \frac{u}{V}(\Sin-S)\\
					\dot{P} &= \nu(S)X - \frac{u}{V}P\\
					\dot{V} &= u\\
					\midrule
					&\mu(S) =\mu_{max}\frac{S}{K_m + S + S^2/K_i}\\
					&\nu(S) = \alert<2>{\nu_{\max}\uncover<2>{\frac{S}{K_\nu + S}}}
				\end{align*}
			\end{block}
		\end{column}
		\begin{column}{0.42\textwidth}
			\begin{table}\centering
				\begin{tabular}{lll}
					\multicolumn{3}{c}{Notation}\\\midrule
					$X$ & $\si{\gram/\liter}$ &Biomass conc.\\
					$S$ & $\si{\gram/\liter}$ & Substrate conc.\\
					$P$ &  $\si{\gram/\liter}$ &Product conc.\\
					$V$& $\si{\liter}$&Volume\\
					$\Sin$ &  $\si{\gram/\liter}$ &\begin{tabular}[t]{@{}c@{}}Feed conc. \end{tabular}\\
					$u$ & $\si{\liter/\hour}$  & Feed rate\\
					$Y_x$ &  $1$ &Yield coefficient\\
					$Y_p$ &  $1$ &Yield coefficient\\
					$\mu$ & $\si{1/\hour}$ &Growth rate\\
					$\nu$&$\si{1/\hour}$&Production rate
				\end{tabular}
			\end{table}
		\end{column}
	\end{columns}
	\footnotetext{\cite{SRINIVASAN2}}
\end{frame}

\begin{frame}{Analytical solution}
	\begin{columns}
		\begin{column}{0.3\textwidth}
			Analytical solution for
			\begin{align*}
				T_{final} &= \SI{150}{\hour},\\
				X_0 &= \SI{1}{\gram/\liter},\\
				S_0 &= \SI{0.5}{\gram/\liter},\\
				P_0 &= \SI{0}{\gram/\liter},\\
				V_0 &= \SI{150}{\liter},\\
				X_{max} &= \SI{3.7}{\gram / \liter}.
			\end{align*}
		\end{column}
		\begin{column}{0.7\textwidth}
			\begin{figure}
				\centering
				\only<1>{\input{../Figures/optimalSolution.tikz}}
				\only<2>{\input{../Figures/optimalSolutionX.tikz}}
				\caption{Reproduced from \cite{SRINIVASAN2}}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}{Nominal Optimization Problem}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{align*}
				\boldminim_{u_0,\dots u_{N-1}} \quad &\sum_{i=0}^{N-1} \alert<1>{-P_{i+1}^2}+ \alert<2>{\frac{1}{2}\Delta u_i^2}\\
				\st \quad &\alert<3>{x_{i+1} = f(x_i, u_i)},\\
				&\alert<4>{x_0=x},\\
				&\alert<5>{u_{min} \leq u_i \leq u_{max}},\\
				&\alert<6>{X_i \leq X_{max}}
			\end{align*}
			with $x_i$ the state vector at time $i$: $\rvect{X_i, S_i, P_i, V_i}^\intercal$.
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{itemize}[<+->]
				\item Maximize product
				\item Smoother control action
				\item Enforce system dynamics\\
				$\rightarrow$ Euler, Runge-Kutta$\dots$
				\item Initial conditions
				\item Input limits
				\item Biomass conc. constraint \footnotemark
			\end{itemize}
		\end{column}
	\end{columns}
	\only<5->{\footnotetext{Higher biomass concentrations would hinder oxygen transfer.}}
\end{frame}

\begin{frame}[t]{Model predictive control (MPC)}
	At each step:
	\begin{itemize}[<+(1)->]
		\item<2-> Read measurements
		\item<3-4,6-7,9-10> Determine optimal control sequence $\{u_0^\star,\dots,u_{N-1}^\star\}$
		\item<4,7,10> Execute $u_0^\star$
	\end{itemize}
	\uncover<3->{
		\begin{figure}\centering
			%\tikzexternaldisable
			\input{../Figures/mpc.tikz}
			%\tikzexternalenable
			\only<3->{\caption{MPC concept\footnote{Figure from MPC lecture notes by Professor Panos Patrinos.}}}
		\end{figure}
	}
\end{frame}

\begin{frame}{Introducing uncertainty}
	\uncover<2->{Uncertain and varying substrate concentration in feed:}
	\begin{align*}
		\boldminim_{u_0,\dots u_{N-1}} \quad &\alert<4->{\sum_{i=0}^{N-1} -P_{i+1}^2+ \frac{1}{2}\Delta u_i^2}\\
		\st \quad & x_{i+1} = f(x_i, u_i\only<2->{,\alert{\Sin_{,i}}}),\\
		&u_{min} \leq u_i \leq u_{max},\\
		&X_i \leq X_{max}.
	\end{align*}
	
	\uncover<3->{$\rightarrow$ The objective depends on the realizations of $\Sin$}
	\begin{block}<4->{}
		\centering
		How to minimize a random cost?
	\end{block}
\end{frame}
