\chapter{Case study: fed-batch penicillin fermentation}
\label{chap:bio}
%In this chapter, the methods from \cref{chap:uncertainty} will be applied to the case of a penicillin fermentation bioreactor. First, in \cref{s:bioreactors}, a general description of bioreactors will be given. Then in 

This chapter presents the bioreactor model to which the methods from \cref{chap:rampc} will be applied. First, \cref{s:bioreactors} will give a general introduction to bioreactors. Then the specific model that will be used, namely that for fed-batch penicillin fermentation, is introduced. Next, in \cref{s:nominal-problem} the nominal control problem is presented together with the analytical solution from \cite{SRINIVASAN2}. Finally, a robust control implementation used in \cite{Lucia2013} for this problem is discussed in \cref{s:lucia}.

%This chapter discusses the general concept of bioreactors. First, the three main operation modes are reviewed which can be used to classify bioreactors. Then one of the classes, the fed-batch bioreactor, is analyzed in more detail and a mathematical model is described. 
%\todo{Chapter description}

\section{Bioreactors}\label{s:bioreactors}
In general, a bioreactor is a vessel sustaining a microbial culture. Its goal is the production of a substance which is the result of the growth of the microorganisms. To sustain the growth, a substrate has to be provided. The way this substrate is provided, together with the way the product is extracted, determines the \textit{operation mode} \cite{tutorial}. The three different operation modes are visualized in \cref{fig:modes}. 
First, there is \textit{batch operation}, where the reactor is filled with an initial amount of biomass and substrate and is then left alone. After a certain amount of time, when the substrate is all consumed or when enough product is present, the reactor is emptied and the product is extracted. Next there is \textit{fed-batch operation} which is similar to batch operation, the only difference being that a continuous feed provides the reactor with additional substrate during its operation. Finally, there is the \textit{continuous operation} mode where, in addition to a substrate feed, there is also an output or effluent stream. This effluent contains product, which can be extracted, but also leftover biomass and substrate. In this case the reactor can, in theory, operate indefinitely. In practice, the operation will have to be interrupted from time to time for maintenance and cleaning. 

In the next section, a mathematical description of the penicillin fermentation process will be given. This process, which is of the fed-batch type, will be used as a case study to test the control methods discussed in \cref{chap:rampc}. 

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\tikzsetnextfilename{reactor-batch}
		\input{../Figures/reactor-batch.tikz}
		\caption{Batch operation}
	\end{subfigure}
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\tikzsetnextfilename{reactor-fed-batch}
		\input{../Figures/reactor-fed-batch.tikz}
		\caption{Fed-Batch operation}
	\end{subfigure}
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\tikzsetnextfilename{reactor-continuous}
		\input{../Figures/reactor-continuous.tikz}
		\caption{Continuous operation}
		\label{fig:cont}
	\end{subfigure}
	\caption{Reactor operation modes}
	\label{fig:modes}
\end{figure}

\section{Fed-batch penicillin fermentation} \label{s:penicillin}
The model that will be used as a case study in this thesis is based on the fed-batch fermentation process of penicillin described in \cite{SRINIVASAN2}. The state variables in this model are the concentration of biomass $X$, the concentration of substrate $S$, the concentration of product $P$ and the volume of the mixture $V$. Note that the fact that the concentrations are described by single values implies that the reactor is assumed to be well mixed. In what follows, the rate of the feed is denoted by $u$ and the substrate concentration in the feed by $\Sin$. The evolution of the state variables is described by the following differential equations\cite{SRINIVASAN2}:

\begin{subequations}\label{eq:penicillin}
	\begin{align}
		\dot{X} &= \mu(S)X - \frac{u}{V}X,\label{eq:p-X}\\
		\dot{S} &= \frac{-1}{Y_x}\mu(S)X - \frac{1}{Y_p}\nu(S)X + \frac{u}{V}(\Sin-S),\label{eq:p-S}\\
		\dot{P} &= \nu(S)X-\frac{u}{V}P,\label{eq:p-P}\\
		\dot{V} &=u,
	\end{align}
\end{subequations}
where $\mu(S)$ and $\nu(S)$ are the specific growth rate and the production rate respectively. For the analytical expression of $\mu$ and $\nu$, multiple options have been suggested \cite{bastin84}:
\begin{alignat}{3}
	&\text{Constant}\qquad&\mu(S) &= \mu_{\textrm{max}}\\
	&\text{Monod} 				&\mu(S)  &=\mu_{\textrm{max}}\frac{S}{K_m + S}\\
	&\text{Blackman}		&\mu(S)  &= 
		\left\{
		\begin{array}{@{}lr@{}}
					\mu_{\textrm{max}}\frac{S}{K_b} &\text{if } S\leq k_b\\
					\mu_{\textrm{max}}&\text{if } S>K_b
		\end{array}
		\right.\\
	&\text{Haldane}				&\mu(S)  &=\mu_{\textrm{max}}\frac{S}{K_m+S+S^2/K_i}\\
	&\text{Contois}				&\mu(S,X)  &=\mu_{\textrm{max}}\frac{S}{K_cX + S}.
\end{alignat}

These functions, which are visualized in \cref{fig:kinetics}, are shown here for the growth rate, but they can analogously be used for the production rate. The model in \cite{SRINIVASAN2} uses a constant production rate $\nu_{\textrm{max}}$ and Haldane kinetics for the growth rate. The values of all the parameters in the model are shown in \cref{tab:params}.

In what follows, the following shorthand notation for the model in \cref{eq:penicillin} will be used:
\begin{equation}
	\dot{x} = F(x,u,w)
\end{equation}
where $x = \rvect{X, S, P, V}^\top$ and $w$ is a vector containing any number of considered parameters, $\Sin$ and $Y_x$ for example.

\tikzFig{kinetics}{Different kinetic models for the growth rate $\mu$.}{kinetics}

An important remark here is that there exist more complex models that describe this process more accurately, see \cite{TILLER} for example. As stated in \cite{tutorial}, these types of models are typically based on very detailed descriptions of all the reactions that are taking place and can contain hundreds of state variables and kinetic parameters. For control purposes, however, a simpler, macroscopic model such as the one discussed above can already prove to be useful \cite{tutorial}.

\begin{table}
	\centering
	\begin{tabular}{{@{}lll@{}}}\toprule
		Model parameter & Value &Units\\ \midrule
		$\mu_{\textrm{max}}$& 0.02 & \si{1/\hour}\\
		$K_m$& 0.05 & \si{\gram/\liter}\\
		$K_i$& 5 & \si{\gram/\liter}\\
		$\nu_{\textrm{max}}$& 0.004& \si{1/\hour}\\
		$Y_x$& 0.5 & \si{\gram[X]/\gram[S]}\\
		$Y_p$& 1.2 & \si{\gram[P]/\gram[S]}\\
		%$K_\nu$& \num{1e-5}&\si{\gram/\liter}\\
		$\Sin$& 200&\si{\gram/\liter}\\
		\bottomrule
	\end{tabular}
	\caption{Parameters for the penicillin fermentation model in \cref{eq:penicillin}.}
	\label{tab:params}
\end{table}

\section{Nominal problem and analytical solution}\label{s:nominal-problem}
The goal is to find a feed rate $u(t)$ for $t \in [0, T_{\textrm{final}}]$ which maximizes the final product concentration. This has to be done under certain operating bounds and given some initial concentrations and volume. The feed rate is constrained to $\SI{1}{\liter/\hour}$ and the reactor operates for $\SI{150}{\hour}$. The biomass concentration $X$ has to stay below \SI{3.7}{\gram/\liter} because higher concentrations hinder the transfer of oxygen \cite{SRINIVASAN2}. Initially, the mixture only contains biomass and substrate. Putting it all together, the problem to be solved is

\begin{subequations}\label{eq:problem}
	\begin{align}
			\boldmaxim_{u(t)} \quad &P(150)\\
			\st \quad & \textrm{Model Equations \ref{eq:penicillin}},\\
			& X_0 = \SI{1}{\gram/\liter},\quad S_0 = \SI{0.5}{\gram/\liter},\\
			& P_0 = \SI{0}{\gram/\liter},\quad V_0 = \SI{150}{\liter},\\
			& X(t) \leq \SI{3.7}{\gram/\liter}\textrm{ for } t \in [0, 150].
		\end{align}
\end{subequations}

The analytical solution to this problem, derived in \cite{SRINIVASAN1}, is reproduced in \cref{fig:analytical}. Once the biomass concentration reaches the constraint, the optimal input is such that the biomass and substrate concentrations stay constant at $X_e=3.7$ and $S_e$. The steady-state $S_e$ and the corresponding optimal input, which is denoted by $u_{path}$ in \cite{SRINIVASAN1}, are found as
\begin{align*}
	S_e &= 1.606 \cdot 10^{-4}\\
	u_{\textrm{path}} &= \mu(S_e)V= 6.404\cdot 10^{-5}V
\end{align*}
by using $\dot{X}=0$, $\dot{S}=0$ and $X=3.7$ in \cref{eq:p-X,eq:p-S}. The optimal final product concentration is given by $P^\star(150) = \SI{1.68}{\gram/\liter}$.

%Note that these solutions are determined with constant production rate $\nu$, but since $K_\nu$ is chosen small, they can still be used as a point of reference.

\tikzFig{analyticalSolution2x2}{Optimal trajectories for the nominal problem, reproduced from \cite{SRINIVASAN2}.}{analytical}


\section{Stochastic MPC for penicillin fermentation}\label{s:lucia}
In \cite{Lucia2013}, a scenario-based SMPC approach is taken to solve problem (\ref{eq:problem}) under uncertainty in the yield coefficient $Y_x$ and the feed substrate concentration $\Sin$. Specifically, $Y_x$ is assumed to be in the range $[0.3, 0.5]$ and constant throughout the batch. The feed concentration is assumed to vary every hour and have a normal distribution, $\Sin \sim \mathcal{N}(200,25)$, truncated at $150$ and $250$.

For the discrete approximation of the uncertainty, the considered uncertain values are $Y_x \in$$\{0.3, 0.4, 0.5\}$ and $\Sin \in \{150, 200, 250\}$. Combining these two sets, there are 9 possible values of the uncertainty or \textit{uncertainty levels}. A robust horizon of $N_r = 1$ and prediction horizon of $N_p = 30$ are used, so the tree splits into $9$ branches at the root node and then stays constant for the next 29 stages. The prediction horizon is kept constant until it needs to shrink to fit into the remaining time frame. This starts to happen when $t = T_{\textrm{final}}-N_p=120$ since the time is conveniently discretized in steps of 1 hour, or $\Delta t=1$.

The optimization problem that is solved at every step is given by
\begin{subequations}\label{eq:problem-lucia}
	\begin{alignat}{2}
		\boldminim_{u_0,\dots u_{29}} \quad&\EX \left[\sum_{t=0}^{N_p-1} \Delta u_t^\top R \Delta u_t  + x_{t+1}^\top Qx_{t+1}\right]&&\\
		\st \quad & x_0 =  x \quad&&\\
		& x_{t+1} = x_{t} + \Delta t F(x_{t+1}, u_t, w_{t}) \quad&&\textrm{ for } t \in \NAT_{[0,29]}\label{eq:implicit}\\
		& X_t \leq \SI{3.7}{\gram/\liter}					\quad&&\textrm{ for } t \in \NAT_{[1,30]}\\
		&  \Delta u_t = u_t-u_{t-1} 								\quad&&\textrm{ for } t \in \NAT_{[0,29]}\\
		& u_{-1} = u^\star_{0\mid \text{i-1}}.			&&
	\end{alignat}
\end{subequations}
where the expectation is taken over the 9 scenarios, $x_t = \rvect{X_t, S_t, P_t, V_t}^\top$ is the state vector at time $t$, $u^\star_{0\mid \text{i-1}}$ is the optimal first input from the previous MPC step, and the tuning matrices $Q$ and $R$ are given by
\begin{equation*}
	Q= \begin{bmatrix}
		0& 0& 0& 0\\
		0& 0& 0& 0\\
		0& 0& -1& 0\\
		0& 0& 0& 0
	\end{bmatrix}
	\quad \text{and} \quad R = 0.5.
\end{equation*}
Furthermore, constraint (\ref{eq:implicit}) implements the implicit Euler discretization of the model equations. The parameter $w_{t}$ in the state transition contains the different values of $Y_x$ and $\Sin$ that are active for $t\in[t, t+1]$ in the different scenarios. When simulating the system, the initial state $x$ is obtained by integrating the model equations over one time step given the previously found optimal control and a sample from $\Sin$. The very first initial state is given by $\rvect{1,0.5,0,150}$. 

Note also that the term  $\Delta u_0^\top R \Delta u_0$ does not depend on any uncertain variables and could therefore be taken out of the expectation. This is not done here to keep the formulation simple. The purpose of this term is to link the solution of the current OP to the previously applied input. 

Using this approach proved successful in robustly satisfying the constraint on $X$ while outperforming \textit{necessary conditions for optimality tracking} (NCO tracking), another robust control method proposed in \cite{SRINIVASAN2}. In the next part of this thesis, this problem setup will be used as a starting point to test the methods from \cref{chap:rampc}.




%\begin{align}
%	\mu(S) &= \mu_{max}\frac{S}{K_m+S+K_i S^2},\\
%	%\nu(S) &=\nu_{max}\frac{S}{K_\nu+S} \label{eq:nu}
%	\nu(S) &=\nu_{max} \label{eq:nu}
%\end{align}













%\begin{table}
%	\centering
%	\begin{tabular}{{@{}lll@{}}}\toprule
%		Parameter & Value &Units\\ \midrule
%		$u_{min}$& 0&\si{\liter/\hour}\\
%		$u_{max}$& 1&\si{\liter/\hour}\\
%		$X_{max}$& 3.7&\si{\gram/\liter}\\
%		$T_{final}$& 150&\si{\hour}\\
%		$X_0$& 1&\si{\gram/\liter}\\
%		$S_0$& 0.5&\si{\gram/\liter}\\
%		$P_0$& 0&\si{\gram/\liter}\\
%		$V_0$& 150&\si{\liter}\\
%		\bottomrule
%	\end{tabular}
%	\caption{Operating bounds and initial conditions.}
%	\label{tab:operatingConditions}
%\end{table}
%
%\section{Continuous bioreactors}
%For continuous operation, the volume of reactor contents has to stay constant. To this end, the effluent rate is kept equal to the feed rate $F$ $[\si{\liter / \hour}]$.  It is also essential that the mixture is well mixed. This allows the use of global parameters for the concentrations of biomass, substrate and product, denoted by $X$, $S$ and $P$ respectively. The evolution of these variables over time can be described by following nonlinear differential equations
%
%\begin{subequations}\label{eq:dynamics}
%	\begin{alignat}{3}
%		\dot{X} &= &&\mu(S)X - DX \label{eq:dynX}\\
%		\dot{S} &= \frac{-1}{Y_x}&&\mu(S)X + D(S_{in}-S) \label{eq:dynS}\\
%		\dot{P} &= Y_p&&\mu(S)X - DP \label{eq:dynP}
%	\end{alignat}
%\end{subequations}
%where $\mu(S)$ is the specific growth rate (in $\si{1/\hour}$), $Y_x$ and $Y_p$ are yield coefficients, $\Sin$ is the inlet substrate concentration and $D$ is the dilution rate, which is defined as the feed rate $F$ divided by the volume. The specific growth rate describes can be described by the Monod law:
%\begin{equation}
%	\mu(S) = \mu_{max}\frac{S}{K_s+S} \label{eq:monod}
%\end{equation}
%with maximum growth rate $\mu_{max}$ (in $\si{1/\hour}$). This equation reflects that for high substrate concentrations the growth will no longer be limited by the availability of $S$ and will approach a maximum value $\mu_{max}$ determined by other factors.
%
%Each equation in \ref{eq:dynamics} has a term corresponding to the growth process and a term corresponding to the dilution. The growth term is negative for $S$, since substrate is consumed, and positive for $X$ and $P$. The term $D(\Sin-S)$ in \cref{eq:dynS} can be derived by considering the change in amount of substrate over a small amount of time caused by the feed and effluent:
%\begin{alignat}{3}
%	&&VS(t+\Delta t) &= VS(t)+F\Sin\Delta t - FS(t)\Delta t\\
%	\implies&& S(t+\Delta t) &= S(t)+D(\Sin - S(t))\Delta t\\
%	\implies&& \dot{S} &= D(\Sin-S)
%\end{alignat}
%Similar reasoning for $\dot{X}$ and $\dot{P}$ results in the terms in $-DX$ and $-DP$ .





