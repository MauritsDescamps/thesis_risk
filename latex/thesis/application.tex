\chapter{Risk-averse MPC of the penicillin fermentation process}\label{chap:application}
This chapter will describe how the risk-averse, risk-constrained optimal control formulation will be applied to the penicillin fermentation model presented in \cref{chap:bio}. First the problem is stated in \cref{s:problem}. Then in \cref{s:mpc-sim}, the details of the closed-loop MPC simulation are discussed. And finally, after an overview in \cref{s:software} of the software used for the implementation, the research goals are stated in \cref{s:goals}.

\section{Problem formulation} \label{s:problem}
The description of the problem setup can be split into three parts, which will be addressed separately below. First the model equations are discussed. Then the assumptions regarding the uncertainty are covered. Finally, the risk-averse, risk-constrained OCP is given.

\subsection{Model equations}
The problem setup that will be used is similar to that in \cite{Lucia2013} which was discussed in \cref{s:lucia}. The main differences are the introduction of risk constraints, and the replacement of the stochastic objective with a risk-objective. Furthermore, there is also a small difference in the reactor model used. In \cite{Lucia2013}, the model described in \cref{eq:penicillin} from \cite{SRINIVASAN2} was used with Haldane kinetics for the specific growth rate $\mu$ and a constant production rate $\nu$. Here, a Monod law will be used for $\nu$ with $\nu_{max}=\SI{0.004}{1/\hour}$ and $K_\nu = 10^{-7}$. With this modification, the new model is given by
\begin{subequations}\label{eq:penicillinNew}
	\begin{align}
		\dot{X} &= \mu(S)X - \frac{u}{V}X,\\
		\dot{S} &= \frac{-1}{Y_x}\mu(S)X - \frac{1}{Y_p}\nu(S)X + \frac{u}{V}(\Sin-S),\\
		\dot{P} &= \nu(S)X-\frac{u}{V}P,\\
		\dot{V} &=u,\\
		\nu(S) &= \nu_{max}\frac{S}{K_\nu+S},\\
		\mu(S)  &=\mu_{max}\frac{S}{K_m+S+S^2/K_i}.
	\end{align}
\end{subequations}
The reason for the modification is that when there is no substrate, there can be no production. As a consequence of this, the model becomes consistent with the fact that the concentrations cannot become negative. The parameter $K_\nu$ is chosen small such that $\nu(S)$ quickly approaches $\nu_{\mathrm{max}}$, which is set to the same value as the constant used in \cite{SRINIVASAN2}. In fact, the steady-state optimal solution after $X$ has reached $3.7$ now becomes
\begin{equation}
	u_{\mathrm{path}} = 6.400\cdot 10^{-5}V
\end{equation}
with equilibrium substrate concentration $S_e=1.605 \cdot 10^{-4}$. This hardly differs from the $u_{\mathrm{path}} = 6.404\cdot 10^{-5}V$ and $S_e=1.606 \cdot 10^{-4}$ derived using the original model in \cref{chap:bio}. The difference between the two models has the most impact when $S$ is small, but since the new equilibrium $S_e$ is still three orders of magnitude larger than $K_\nu$ the impact is minimal.

\subsection{Uncertainty and scenario tree}
In \cite{Lucia2013}, uncertainty in both $\Sin$ and $Y_x$ was taken into account. This thesis will assume the same distribution in $\Sin$ but consider no uncertainty in $Y_x$. The distribution used for $\Sin$ is a truncated normal distribution with mean $200$ and standard deviation $25$ truncated at $150$ and $250$.

For the scenario tree, a number of options will be investigated. The shape, however, will be restricted to trees with a constant branching factor up to some robust horizon, such as the tree shown in \cref{fig:treeRobust}. The different considered uncertainty levels for $\Sin$ that characterize every branching of the tree are given by $\{150, 200, 250\}$, as was the case in \cite{Lucia2013}. The corresponding probability vector is chosen to be $\pi = \rvect{0.25, 0.5, 0.25}^\top$. This was a design choice, as the probabilities used in \cite{Lucia2013} are not reported. With these values, the entire range of possible $\Sin$ values is covered, and the nominal case is well represented. This is in accordance with the guidelines suggested in \cite{LuciaTiago2013} and discussed in \cref{s:tree}.

\subsection{RROCP formulation}
Putting it all together, and using the same weighting matrices $Q$ and $R$ as in \cite{Lucia2013}, the RROCP for the penicillin fermentation process becomes:
\begin{subequations}\label{eq:problem-formulation}
	\begin{alignat}{2}
		\boldminim_{u_0,\dots u_{N-1}} \quad&
		\mathrlap{
			\begin{aligned}[t]
				\frac{1}{2}\Delta u_0+ \rho_{\mid 0} &\Bigg[-P_1^2 + \frac{1}{2}\Delta u_1
				+\rho_{\mid 1}  \bigg[-P_2^2 + \frac{1}{2}\Delta u_2\dots\\
				+\rho_{\mid N-2}  &\bigg[-P_{N-1}^2 + \frac{1}{2}\Delta u_{N-1}+
				\rho_{\mid N-1}  \big[-P_N^2\big]\Big]\dots \bigg] \Bigg]
		\end{aligned}}&&\\
		\st \quad & x_0 =  x \quad&&\\
		& x_{t+1} = x_{t} + \Delta t F(x_{t+1}, u_t, \Sin_{,t}) \quad&&\textrm{ for } t \in \NAT_{[0,N-1]}\label{eq:Fconstraint}\\
		& r_t[X_t -3.7]\leq 0 \quad&&\textrm{ for } t \in \NAT_{[1,N]} \label{eq:Xconstraint}\\
		&  \Delta u_t = u_t-u_{t-1} 													   \quad&&\textrm{ for } t \in \NAT_{[0,N-1]}\\
		& u_{-1} = u^\star_{0\mid \text{i-1}}.											&&
	\end{alignat}
\end{subequations}
where the initial $x$ is obtained by integrating the model equations over one time step given the previously applied input and a sampled value for $\Sin$. Note also that constraint (\ref{eq:Fconstraint}) is actually a set of $\abs{\nodes(t+1)}$ constraints. The variable $\Sin_{,t}$ contains the different possible realizations of $\Sin$. Both for the risk measure in the objective and for the one in the risk-constraints, $\rho_{\mid t}$ and $r_t$ respectively, the average value-at-risk will be used. This way the formulations discussed in \cref{chap:rampc} can be used and the conic inequalities become regular ones. Furthermore, the time will be discretized in steps of 1 hour, so $\Delta t=1$ from now on.


\section{MPC simulation}\label{s:mpc-sim}
As stated above, for the closed-loop MPC simulation the steps followed at every time step are:
\begin{enumerate}
	\item solve RROCP (\ref{eq:problem-formulation}) to obtain $u_0^\star$,
	\item sample $\Sin$,
	\item integrate the reactor model given $u_0^\star$ and $\Sin$ to obtain the next state vector.
\end{enumerate}
Receding horizon MPC with a prediction horizon smaller than the batch time will be used until the prediction horizon reaches $T_{\textrm{final}}$. After that, the horizon will start shrinking. To compare results, and to test the implementation, nominal MPC will be simulated as well. In that case, the risk constraint is replaced by a regular one and a "scenario tree" with only one scenario is used, eliminating the need for a risk measure in the objective. This does, however, pose a problem. The biomass concentration is exactly $\SI{3.7}{\gram/\liter}$ in the latter half of the optimal trajectory, as can be seen in \cref{fig:analytical}. This means that there will be plenty of constraint violations when using nominal MPC under uncertainty, rendering the problem infeasible most of the time. To address this, the constraint can be replaced with an extra objective term penalizing violations. This method, called constraint relaxation, is explained below.

The following optimization problem will serve as an example
\begin{subequations}
	\begin{align}
		\boldminim\quad& f(x)\\
		\st \quad& g(x) \leq 0. \label{eq:relax-c}
	\end{align}
\end{subequations}
A straightforward idea to relax the constraint would be to replace it with an additional penalty term
 $c\cdot \boldmax\{0,g(x)\}$ in the objective. This does, however, make the objective non-smooth which complicates solving the problem. An alternative but equivalent approach is to introduce an auxiliary variable $\zeta$, as follows
\begin{subequations}
	\begin{align}
		\boldminim\quad& f(x) + c\cdot\xi\\
		\st \quad&0\leq \xi,\\
		&g(x)\leq \xi
	\end{align}
\end{subequations}
where the weighting coefficient $c$ tunes how severely constraint violations should be penalized. This method can be repeated for every constraint.
%\todo{Reference to risk-constraint relaxation}

\section{Software}\label{s:software}
The problem described above will be implemented and solved in \MATLAB{} with the help of two toolboxes. For dealing with scenario trees, the toolbox \textit{Marietta} will be used \cite{Marietta}. Given a discrete probability distribution, a robust horizon and a prediction horizon, Marietta lets the user define a scenario tree with constant branching factor up to the robust horizon. The unique IDs of the nodes of the tree can then be accessed through functions like \texttt{getNodesAtStage($\cdot$)}, \texttt{getChildrenOfNode($\cdot$)} and \texttt{getAncestorOfNode($\cdot$)}. This is actually only a small part of what Marietta has to offer. It also provides a framework for constructing and solving RROCPs. In this thesis, however, the problem is constructed and solved in \CASADI{} \cite{CASADI}. This tool provides a framework for constructing general nonlinear optimization problems. Through the \CASADI{} function \texttt{nlpsol}, an interface to a number of solvers is provided, such as the interior point solver IPOPT \cite{IPOPT} which will be used here.
For the integration step in the MPC simulations, the ODE solver CVodes \cite{CVODES} from the Sundials software suite \cite{Sundials} is used. This and other ODE solvers can be interfaced through the \CASADI{} function \texttt{integrator}.


\section{Goals}\label{s:goals}
The problem formulation described above still leaves a number of design options open for investigation. The main ones are the $\alpha$ values associated with the $\AVAR$ risk measures. Furthermore, both nested and stage-wise risk constraints can be used. Regarding the scenario tree, the robust and prediction horizon need to be chosen. The effect of these choices will be investigated through simulations. Additionally, the effect of the changed production rate dynamics will be investigated by comparing the results with those reported in \cite{Lucia2013}. The results are discussed in \cref{chap:results}.






