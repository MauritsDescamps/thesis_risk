\chapter{Results}
\the\textwidth
\label{chap:results}
In this chapter, the effect of the risk measure parameters and the prediction horizon will be investigated through the simulation approach outlined in \cref{chap:application}. First, \cref{s:predHor} will look at the effect of the prediction horizon with a number of nominal MPC simulations in the absence of uncertainty. Then, \cref{s:stochMPC} compares stochastic and nominal MPC in the presence of uncertainty in $\Sin$. Next, \cref{s:RRMPC} discusses the results for risk-averse MPC and for risk-constrained MPC. In \cref{s:highHor}, the effect of the robust horizon on the solution time and the problem size is discussed. Finally, \cref{s:conclusion} summarizes the results.

All simulations are performed using a $\SI{2.2}{\giga \hertz}$ quad-core Intel i7 processor with 16 GB of RAM.

\section{Prediction horizon}\label{s:predHor}
To investigate the effect of the prediction horizon $N$, the problem is first simulated without any uncertainty. During this simulation, with the setup described in \cref{chap:application}, the OCP became infeasible as soon as $X$ reached the constraint. These infeasibilities are caused by constraint violations in the initial state vectors supplied to the OCP, obtained via CVodes integration. A first attempt at solving this problem was to relax the constraints with the method described in \cref{chap:application}. The results of these simulations are shown in \cref{fig:horizonRelax}. There is a considerable amount of overshoot in biomass concentration, and the substrate concentration overshoots to zero before reaching its equilibrium value as can bee seen in the bottom left subfigure. Furthermore, the penicillin concentration does not reach the optimal $\SI{1.68}{\gram/\liter}$.

A second attempt was to directly address the source of the constraint violations, being the difference between the model discretization used in the OCP and the more accurate CVodes integrator used to update the state during simulation. An input that does not cause a constraint violation in any scenario when taking an implicit Euler step might still result in a violation during the simulation due to this difference. A logical improvement is therefore to replace the implicit Euler scheme with a higher order one, such as the Crank-Nicolson method:
\begin{equation}
	x_{t+1} = x_{t} + \Delta t\frac{1}{2}\left(F(x_{t}, u_t, \Sin_{,t+1}) + F(x_{t+1}, u_t, \Sin_{,t+1})\right).
\end{equation} 
The results using this second order implicit method are shown in \cref{fig:horizonCN}. The problems with the results for relaxed constraints discussed above are no longer present. For horizons of $20$ and higher, the optimal penicillin concentration is reached. For all horizons but the smallest one, the equilibrium substrate concentration converges to the $S_e=1.6\cdot 10^{-4}$ found in \cref{chap:application} without overshooting. Furthermore, the results are very similar to those reported for nominal MPC in \cite{Lucia2013}. An overview of the performance and computation times is given in \cref{tab:NMCP-CN}.

\begin{table}
	\centering
	\pgfplotstableread{"../FigData/Nominal/crankNicolson_dynamic_nu/info.txt"}{\tabledata}
	\input{../Tables/nominaltable.tex}
	\caption{Nominal MPC simulation results without uncertainty using Crank-Nicolson discretization for different prediction horizons.}
	\label{tab:NMCP-CN}
\end{table}
In the remainder of this chapter, the prediction horizon will be fixed at $N=30$, and the Crank-Nicolson method will be used for the discretization.

\begin{figure}
	\centering
	\def\dataPath{../FigData/Nominal/implicitEuler_dynamic_nu_1e+05/}
	\input{../Figures/nominalMPC.tikz}
	\caption{Nominal MPC simulations without uncertainty, using implicit Euler in the OCP and relaxed constraints ($c=10^5$), for different prediction horizons.}
	\label{fig:horizonRelax}
\end{figure}

\begin{figure}
	\centering
	\def\dataPath{../FigData/Nominal/crankNicolson_dynamic_nu/}
	\input{../Figures/nominalMPC.tikz}
	\caption{Nominal MPC simulations without uncertainty, using Crank-Nicolson instead of implicit Euler in the OCP, for different prediction horizons.}
	\label{fig:horizonCN}
\end{figure}

\section{Nominal and stochastic MPC with uncertainty} \label{s:stochMPC}
As a benchmark for the performance of RRMPC, the problem is simulated with uncertainty in $\Sin$. This is first done with a nominal MPC controller. As discussed in \cref{chap:application}, there will be many constraint violations since the optimal solution runs exactly along the edge of the feasible region. To avoid infeasibility problems, the biomass constraint is therefore relaxed with a weighting coefficient of $c=10^4$. The results are shown in \cref{fig:nominal}. 
\begin{figure}
	\centering
	\def\dataPath{../FigData/MonteCarlo/nominal_crankNicolson_dynamic_nu_N30_Scenarios1_150_soft1e+04/}
	\def \zoomPymin{1.55}
	\def \zoomPymax{1.70}
	\def \zoomBymin{3.696}
	\def \zoomBymax{3.705}
	\input{../Figures/MonteCarlo.tikz}
	\caption{Results of nominal MPC simulations under uncertainty.}
	\label{fig:nominal}
\end{figure}
Surprisingly, the final penicillin concentration never comes close to the optimal value despite the many constraint violations. This can be explained by looking at the trajectories of the substrate concentrations in the bottom left of \cref{fig:nominal}. Due to the introduction of uncertainty, $S$ regularly drops below $S_e$ and even to zero. Since the production rate is described by a Monod law, it goes to zero with $S$, causing lower penicillin production.

In \cref{fig:stochastic}, simulation results for stochastic MPC are shown. The scenario tree used has a robust horizon of $1$, analogous to the approach in \cite{Lucia2013} which was discussed in \cref{chap:bio}. Only $15$ simulations are plotted in the figure, but out of $150$ samples there were no constraint violations. There is however a decrease in performance, which is demonstrated in \cref{fig:hist}. The average final penicillin concentration out of $150$ simulations was $\SI{1.624}{\gram/\liter}$.

\begin{figure}
	\centering
	\def\dataPath{../FigData/MonteCarlo/stochastic_crankNicolson_dynamic_nu_N30_Scenarios3_150/}
	\def \zoomPymin{1.55}
	\def \zoomPymax{1.70}
	\def \zoomBymin{3.696}
	\def \zoomBymax{3.705}
	\input{../Figures/MonteCarlo.tikz}
	\caption{Results of stochastic MPC simulations under uncertainty.}
	\label{fig:stochastic}
\end{figure}
\tikzFig{histogramNS}{Histogram of the final penicillin concentrations for the stochastic and nominal MPC simulations under uncertainty. In both cases, $150$ simulations were performed.}{hist}


\section{Risk-averse, risk-constrained MPC}\label{s:RRMPC}
In this section RROCP based MPC will be simulated. The different parameters to investigate are the robust horizon and the $\alpha$-values in the objective risk measure and in the constraint risk measure. Remember that the different uncertainty levels are chosen to be $\{150, 200, 250\}$ with probability vector $\pi = \rvect{0.25, 0.5, 0.25}^\top$. The first simulations will use a robust horizon of $1$. This means that there is no difference between stage-wise and nested risk constraints.

One problem that arose during some simulations, was that the solution from the OC problems had spikes at the end of the prediction horizon. An example of this is shown \cref{fig:spike}. For the most part this had little effect on $u_0^\star$, but when the final time comes closer and the horizon starts shrinking there were cases where the control action was influenced. To circumvent this problem, receding horizon MPC is used until the end instead of switching to shrinking horizon MPC when the prediction horizon reaches the batch time.
\begin{figure}
	\centering
	\includegraphics[width=8cm]{spike.png}
	\caption{Example of a spike in the OCP solution, in this case from a simulation of stochastic MPC with risk constraints.}
	\label{fig:spike}
\end{figure}

\subsection{Risk-averse MPC with regular constraints}
In \cref{fig:avarObj}, the distribution of the final product is shown for simulations with regular constraints and conditional $\AVAR_\alpha$ risk mappings in the objective for different values of $\alpha$. The top boxplot, corresponding to $\alpha=1$, shows the results for regular stochastic MPC. The impact of $\alpha$ seems to be minimal. Low values of $\alpha$ should in theory result in an increase of the worst case penicillin concentrations but this is not really visible in the results. A possible explanation for this could be the short robust horizon of $1$. \Cref{tab:avarObj} gives an overview of the average final penicillin concentration and the number of simulations with constraint violations for different $\alpha$.


\tikzFig{boxplot_AVAR_Obj}{Boxplots of the final penicillin concentration using regular constraints and nested $\AVAR_\alpha$ in the objective with different values of $\alpha$. Sample size: $150$.}{avarObj}

\begin{table}
	\centering
	\pgfplotstableread{"../FigData/MonteCarlo/results_AVAR_OBJ.txt"}{\tabledata}
	\pgfplotstableread{
		1 1.624 0
	}\stoch
	\pgfplotstablevertcat{\tabledata}{\stoch}
	\pgfplotstabletypeset[
	highlight last row,
	precision=3,
	columns/0/.style={string type},
	every head row/.style={
		output empty row,
		before row={\toprule
			alpha&
			\shortstack{Mean final\\ conc. [$\si{\gram/\liter}$]}&
			\shortstack{Num. of\\constraint violations}\\}
		,after row=\midrule},
	every last row/.style={after row=\bottomrule}]{\tabledata}
	\caption{Simulation results for stochastic MPC with regular constraints and $\AVAR_\alpha$ objective with different $\alpha$-values. Total number of simulations: $150$.}
	\label{tab:avarObj}
\end{table}


\subsection{Stochastic MPC with risk constraints}
In \cref{fig:avarRC}, the distribution of the final product is shown for simulations with a stochastic objective and $\AVAR_\alpha$ risk constraints for different values of $\alpha$. Detailed simulation results for $\alpha=0.01$ and $\alpha=0.99$ are shown in \cref{fig:stochastic-RC1,fig:stochastic-RC99} respectively. The bottom boxplot in \cref{fig:avarRC} shows the results for regular stochastic MPC since $\alpha=0$ corresponds to regular constraints in this setting. Looking at the results, especially for lower $\alpha$-values, there is clearly something wrong here. As $\alpha$ approaches zero, the results should approach those of stochastic MPC with regular constraints. In the simulations, however, the distributions of $P_{150}$ had very heavy left tales for low values of $\alpha$. Additionally, for higher $\alpha$-values, the constraint becomes less strict and a better final product concentration was expected. This is however not the case. \Cref{tab:avarRC} gives an overview of the average final penicillin concentration and the number of simulations with constraint violations for different $\alpha$.

The cause of these unexpected results is the fact that in a lot of the simulations, the IPOPT solver reported an infeasibility larger than the tolerance for some of the OCPs. For $\alpha=0.01$, infeasibilities were encountered in 32 out of the 150 simulations. For higher $\alpha$ this number increased and for $\alpha=0.9$ and higher, infeasibilities were encountered in all of the simulations. Two possible explanations for these infeasibilities can be suggested. First, the problem could be of numerical nature. To explore this possibility, further experiments with different solver settings and perhaps other solvers are needed. A second possibility could be that the problem formulation with risk constraints is not recursively feasible. If that is the case, a possible solution could be provided by the methods described in \cite{Recursive}. There, a recursively feasible risk-averse MPC controller is formulated for a discrete Markov jump linear system. Adapting this method to the problem studied here, perhaps through a linearization of the reactor model, would be an interesting direction for future work.


\tikzFig{boxplot_AVAR_RC}{Boxplots of the final penicillin concentration using a stochastic objective and $\AVAR_\alpha$ risk constraints with different values of $\alpha$. Sample size: $150$.}{avarRC}

\begin{table}
	\centering
	\pgfplotstableread{
		0 1.624 0
	}\tabledata
	\pgfplotstableread{"../FigData/MonteCarlo/results_AVAR_RC.txt"}{\tabledataRest}
	\pgfplotstablevertcat{\tabledata}{\tabledataRest}
	\pgfplotstabletypeset[
	highlightrow={1},
	precision=3,
	columns/0/.style={string type},
	every head row/.style={
		output empty row,
		before row={\toprule
			$\alpha$&
			\shortstack{Mean final\\ conc. [$\si{\gram/\liter}$]}&
			\shortstack{Num. of\\constraint violations}\\}
		,after row=\midrule},
	every last row/.style={after row=\bottomrule}]{\tabledata}
	\caption{Simulation results for MPC with a stochastic objective and $\AVAR_\alpha$ risk constraints with different values of $\alpha$. Total number of simulations: $150$.}
	\label{tab:avarRC}
\end{table}

\begin{figure}
	\centering
	\def\dataPath{../FigData/MonteCarlo/stochastic_RC1_crankNicolson_dynamic_nu_N30_Scenarios3_150/}
	\def \zoomPymin{1.45}
	\def \zoomPymax{1.70}
	\def \zoomBymin{3.68}
	\def \zoomBymax{3.705}
	\input{../Figures/MonteCarlo.tikz}
	\caption{Results of stochastic MPC with $\AVAR_{0.01}$ risk constraints.}
	\label{fig:stochastic-RC1}
\end{figure}

\begin{figure}
	\centering
	\def\dataPath{../FigData/MonteCarlo/stochastic_RC99_crankNicolson_dynamic_nu_N30_Scenarios3_150/}
	\def \zoomPymin{1.45}
	\def \zoomPymax{1.70}
	\def \zoomBymin{3.696}
	\def \zoomBymax{3.705}
	\input{../Figures/MonteCarlo.tikz}
	\caption{Results of stochastic MPC with $\AVAR_{0.99}$ risk constraints.}
	\label{fig:stochastic-RC99}
\end{figure}


\section{Higher robust horizon}\label{s:highHor}
In the previous section, the robust horizon was limited to $1$. \Cref{tab:size} lists the computation times and problem sizes for different problem settings and robust horizons. An exponential increase in both the iteration time and the number of optimization variables is observed with growing robust horizon. Note that the iteration times in the table are averaged over the $150$ OCPs. These times are heavily influenced by the second halve of the iterations, where $X$ has reached the constraint, because the problem with infeasibilities persisted. For this reason, no Monte Carlo simulations have been performed with longer horizons. If a way could be found to make the problem recursively feasible, it would be interesting to perform simulations to investigate the effect of the length of the robust horizon.


\begin{table}
	\centering
	\begin{tabular}{@{} l *{2}{cc} @{}}
		\toprule
		\multirow{3}{*}{Problem type} & \multicolumn{4}{c@{}}{Robust horizon}\\
		\cmidrule(l){2-5}
		& \multicolumn{2}{c}{1} 
		& \multicolumn{2}{c@{}}{2} \\
		\cmidrule(lr){2-3} \cmidrule(l){4-5}
		& {Time/iter. [$\si{\s}$]} & {\#opt. var.} & {Time/iter. [$\si{\s}$]} & {\#opt. var.} \\
		\midrule
		\begin{tabular}{@{}l@{}}Stochastic cost, \\ Regular constraints\end{tabular} & 0.22 & 448 & 0.98 & 1312 \\
		\begin{tabular}{@{}l@{}}Stochastic cost,\\ $\AVAR$ constraints\end{tabular}  & 0.44 & 778   & 16.6 & 2164 \\
		\begin{tabular}{@{}l@{}}$\AVAR$ cost,\\ Regular constraints\end{tabular}  & 0.61 &986   & 11.2 &  2882\\
		\begin{tabular}{@{}l@{}}$\AVAR$ cost,\\ $\AVAR$ constraints\end{tabular} & 0.82 & 1316   & 21.86 & 3734 \\
		\bottomrule
	\end{tabular}
	\caption{Average computation time per iteration and number of optimization variables for different problem settings.}
	\label{tab:size}
\end{table}

\section{Conclusion}\label{s:conclusion}
In the absence of uncertainty, and when using the more precise Crank-Nicolson discretization, the nominal MPC results were very similar to the ones reported in \cite{Lucia2013}. In the presence of uncertainty, however, the optimal product concentration decreases because of the adapted production rate kinetics. With uncertainty in $\Sin$, stochastic MPC proved to yield reasonable performance and complete constraint satisfaction. The results for RRMPC were influenced by the fact that the solver often converged to a point of local infeasibility. The cause of this problem could be either of numerical nature or inherent to the problem formulation. Either way, a solution needs to be found first in order to get a better view on the potential of the method for this application. This would likely also reduce the simulation times, making it possible to perform simulations with larger robust horizons.