\chapter{Distributionally robust multistage chance constraints}\label{chap:riskConstraints}
In \cref{chap:rampc}, two types of risk constraints were introduced: stage-wise risk constraints and multistage nested risk constraints.  The benefit of using MNRMs is that they account for how the ambiguity propagates through the scenario tree. This is motivated with an example in \cref{s:example}. Next, in \cref{s:ACS}, a problem formulation is given where MNRCs naturally arise. Implementing such nested risk constraints does, however, involve a lot of auxiliary variables and constraints, as was seen in \cref{eq:nestedRC}. Therefor, \cref{s:AS} proposes a method for overapproximating MNRCs with SWRCs. Finally, \cref{s:AS-results} shows some preliminary results.


\section{Motivating example}\label{s:example}
\def\horizon{4}
\def\alphaAvar{0.8}
\def\win{2}
\def\loose{120}
\def\pnom{0.7}
\def\preal{0.63}
\def\nested{0.4126}

Suppose a sports team plays $\horizon$ games and is estimated to have a $\evaluate[0]{100*(1-\pnom)}\%$ chance of winning each individual game. The outcomes of the games can be seen as an independently and identically distributed stochastic process $(w_t)_{t\in \NAT_{[1,\horizon]}}$, where $w_t\in \{1,0\}$ with nominal probability vector $\pi=[\pnom \; \evaluate{1-\pnom}]^\top$. Furthermore, it is assumed that the true probability vector lies in the ambiguity set $\ambiguity^{\AVAR}_{\alphaAvar}(\pi)$, which can be derived using \cref{eq:ambiavar} as
\begin{equation}\label{eq:ambiExmpl}
	\ambiguity^{\AVAR}_{\alphaAvar}\left(
	\begin{bmatrix}
		\pnom\\
		\evaluate{1-\pnom}
	\end{bmatrix}
	\right) = 
	\left\{
	\begin{bmatrix}
		p\\
		1-p
	\end{bmatrix}
	\in \REAL^2
	\Middle \evaluate[3]{1-(1-\pnom)/\alphaAvar} \leq p \leq \evaluate[3]{\pnom/\alphaAvar}
	\right\}.
\end{equation}

Imagine now that someone proposes a bet on the number of games the team wins after $\horizon$ plays. If the team wins at least once, you receive \euro$\win$, but if they loose all $\horizon$ games you have to pay \euro$\loose$. To put this in the terminology of \cref{chap:rampc}, the cost variable $G_4 = \left(G^i\right)_{i\in\nodes(4)}$ is defined with respect to the scenario tree in \cref{fig:treeBet}. The individual elements of $G_4$ are defined as the negatives of the corresponding payoffs of the bet. This way, a loss is represented as a cost that can be minimized or constrained.

\tikzFig{treeBet}{Visualization of the scenario tree associated with the betting example in \cref{s:example}}{treeBet}

Intuitively, this bet does not seem very interesting, but out of curiosity one might want to know whether the expected profit is positive. A simple criterion to determine whether or not the bet is worth it, could be to require that the expected cost is negative, which can be written as
\begin{equation}
	\EX_{\pi_{\horizon}}[G_4] \leq 0 \label{eq:negExpCost}
\end{equation}
where the expectation is taken with respect to the nominal probability vector on $\nodes(\horizon)$. In this example, this requirement is met:
\begin{align*}
	\EX_{\pi_4}[G_4] &=\pi_4^\top G_4\\
	&= -\win +\evaluate[3]{(1-\pnom)^\horizon}\cdot (\loose+\win)\\
	&= \evaluate[3]{-\win+(\loose+\win)*(1-\pnom)^\horizon},
\end{align*}
where $\evaluate[3]{(1-\pnom)^\horizon}$ is the probability of $\horizon$ losses, calculated as $\evaluate{1-\pnom}^\horizon$. The problem with this criterion is that it does not account for the ambiguity in $\pi$, which was assumed to be characterized by $\ambiguity^{\AVAR}_{\alphaAvar}$. To address this, \cref{eq:negExpCost} can be replaced with 
\begin{equation}\label{eq:robustExp}
	\max_{\mu \in \ambiguity} \EX_{\mu}[G_4] \leq 0,
\end{equation}
where $\ambiguity \subseteq \simplex_{\abs{\nodes(\horizon)}} $ is an appropriate ambiguity set. One approach would be to assume the same amount of ambiguity on $\pi_4$, and take $\ambiguity = \ambiguity^{\AVAR}_{\alphaAvar}(\pi_4)$. With this ambiguity set, \cref{eq:robustExp} can be written as the following SWRC:
\begin{equation}
	\AVAR_{\alphaAvar}[G_4] \leq 0.
\end{equation}
Evaluating this can be done as follows
\begin{equation*}
	\begin{aligned}
		\AVAR_{\alphaAvar}[G_4]  &= -\win+\AVAR_{\alphaAvar}[G_4+\win]\\
		&= -\win+\max_{\mu \in \ambiguity^{\AVAR}_{\alphaAvar}}\mu^\top (G_4+\win)\\
		&= -\win+\evaluate[3]{(1-\pnom)^\horizon/\alphaAvar}\cdot (\loose+\win)\\
		&= \evaluate[3]{-\win+((0.3^\horizon)/\alphaAvar)*(\loose+\win)},
	\end{aligned}
\end{equation*}
where $\evaluate[3]{(1-\pnom)^\horizon/\alphaAvar}$ is the largest probability of $\horizon$ losses that is still within $\ambiguity^{\AVAR}_{\alphaAvar}$. It is calculated as $\frac{\evaluate{1-\pnom}^\horizon}{\alphaAvar}$. Although this criterion rates the bet less attractive, it still deems it worth taking. 

There is, however, a problem with this approach. As was briefly touched on in \cref{chap:rampc}, stage-wise risk constraints do not reflect how the ambiguity propagates through the tree. To illustrate this, suppose that the real probability of winning is actually $\preal$ instead of $\pnom$. The corresponding probability vector $\tilde{\pi} = [\preal\; \evaluate{1-\preal}]^\top$ is within $\ambiguity^{\AVAR}_{\alphaAvar}(\pi)$, as can be seen in \cref{eq:ambiExmpl}. Using this new, true probability vector, the probability of $\horizon$ losses becomes $\evaluate{1-\preal}^\horizon = \evaluate[3]{(1-\preal)^\horizon}$, and the expected cost is equal to
\begin{align*}
	\EX_{\tilde{\pi}_4}[G_4] &= -\win +\evaluate[3]{(1-\preal)^\horizon}\cdot (\loose+\win)\\
	&= \evaluate[3]{-\win+(\loose+\win)*(1-\preal)^\horizon}.
\end{align*}
This expected cost is higher than $\AVAR_{\alphaAvar}[G_4]$ which proves that the set $\ambiguity^{\AVAR}_{\alphaAvar}(\pi_4)$ is too small to reflect all the possible probability vectors at $t=4$ resulting from the initial ambiguity. Specifically, there is no $\mu \in \ambiguity^{\AVAR}_{\alphaAvar}(\pi_4)$ such that $\mu[\{30\}]=\evaluate{1-\preal}^\horizon$, because the largest possible probability of four losses in $\ambiguity^{\AVAR}_{\alphaAvar}(\pi_4)$ was found to be $\evaluate[3]{(1-\pnom)^\horizon/\alphaAvar}$ previously.

As was discussed in \cref{chap:rampc}, this problem can be solved using multistage nested risk measures which were defined as
\begin{equation}
	\phantom{\ref{eq:nested_RC} revisited} \bar{r}_t[G_{t}] = \rcond{0}[\rcond{1}[\dots \rcond{t-1}[G_{t}]]] \leq 0.\tag{\ref{eq:nested_RC} revisited}
\end{equation}

In this example, the multistage nested risk of $G_4$ is given by
\begin{equation}
	\overline{\AVAR}_{\alphaAvar,3}[G_4] = \nested,
\end{equation}
which was calculated by evaluating the risks node per node using the definition of conditional risk mappings in \cref{eq:conditional}. This value is higher than $\EX_{\tilde{\pi}_4}[G_4]$, it accounts for deviations from $\pi_4$ as extreme as $\tilde{\pi}_4$.



\section{Ambiguous chance constraints} \label{s:ACS}
To use chance constraints of the form $\Prob[G_t>0]\leq \alpha$, the node probabilities $\pi^{i}$ need to be known exactly. This is, however, rarely the case since the scenario tree and corresponding probabilities are usually determined from data. To accommodate for this imperfect knowledge, ambiguous chance constraints can be used:
\begin{equation}\label{eq:chanceConstraint}
	\tilde{\Prob}[G_t>0]\leq \alpha,\; \forall \tilde{\Prob}\in \ambiP_t
\end{equation}
where $\mathscr{P}_t$ is an ambiguity set. So far, ambiguity sets have been defined as sets of probability vectors, but with discrete random variables a probability vector uniquely defines a probability measure and vice versa. 

Now define $E_t = \left\{i\in \nodes(t)\Middle G_t^i > 0\right\}$ as the set of nodes where the constraint is violated and let $\indicator_{E_t}$ be its indicator:
\begin{equation}
	\indicator_{E_t} \colon \nodes(t) \to \{0,1\},\; i \mapsto \indicator_{E_t}(i) =
	\begin{cases*}
		1 \quad \text{if}\; i\in E_t\\
		0 \quad \text{if}\; i\notin E_t\\
	\end{cases*}.
\end{equation}
With these definitions, \cref{eq:chanceConstraint} can equivalently be written as
\begin{equation}\label{eq:ambiC}
	\boldmax\left\{\EX_{\tilde{\Prob}}\left[\indicator_{E_t}\right] \Middle \tilde{\Prob}\in \ambiP_t\right\}\leq\alpha,
\end{equation}
where the following risk measure emerges
\begin{equation}\label{eq:maxPtilde}
	r_t [Z] = \boldmax_{\tilde{\Prob}\in \ambiP_t} \EX_{\tilde{\Prob}}[Z].
\end{equation}
Using this definition, \cref{eq:chanceConstraint} can be written as
\begin{equation}\label{eq:AS-RC}
	r_t[1_{E_t}]\leq \alpha.
\end{equation}


To obtain an expression for $\ambiP_t$, the following procedure can be used. First, using statistical methods as in \cite{SafeLearningBased}, the ambiguity in the conditional probability vectors $\pi^{[i]}$ is determined for every non-leaf node $i$. In the example above, the ambiguity sets on $\pi^{[i]}$ were all given by  $\ambiguity^{\AVAR}_{\alphaAvar}(\pi)$, since the game results were independent and identically distributed. This is the simplest case, but in practice, different ambiguity sets can be chosen for every node. For example, when the the branching factor is not constant the ambiguity sets will be different.
Next, these ambiguity sets on  $\pi^{[i]}$ give rise to conditional risk mappings $\rho_{\mid t}$, which in turn can be used to construct the multistage nested risk measure $\bar{\rho}_t$. Using this risk measure as $r_t$ and its ambiguity set as $\ambiP_t$ in \cref{eq:maxPtilde} ensures that the propagation of ambiguity through the tree is accounted for. In the next section, a conservative approximation of such a risk constraint will be constructed in the form of a chance constraint.


\section{Approximating multistage nested risk constraints}\label{s:AS}
The goal in this section is to find an $\alpha^\star$ such that imposing the chance constraint $\Prob_t[E_t]\leq \alpha^\star$ implies the multistage nested risk constraint $\bar{\rho}_t[1_{E_t}]\leq \alpha$.

If a risk measure $r_t$ is law invariant, i.e. $r_t[Z_1] = r_t[Z_2]$ whenever $Z_1$ and $Z_2$ have the same distribution, then there is a function $p_t\colon [0, 1]\to \REAL$ such that $r_t[1_{E_t}]=p_t(\Prob_t[E_t])$ \cite[Sec. 6.4]{SHAPIRO}. For example, if $r_t=\AVAR_\alpha$, then $p_t(x) = \min\{1,\alpha^{-1}x\}$, which is a nondecreasing function, for $x\in[0,1]$. If such a function exists, \cref{eq:AS-RC} can equivalently be written as 
\begin{equation}
	p_t(\Prob_t[E_t])\leq \alpha,
\end{equation}
and if $p_t$ is nondecreasing, $\alpha^\star$ can be found as
\begin{equation}\label{eq:AS-max-pt}
	\alpha^\star = \max \left\{x\in [0,1] \Middle p_t(x)\leq \alpha\right\}.
\end{equation}


\begin{figure}
	\def\pLI{0.8}
	\def\alphaLI{0.8}
	\centering
	\begin{subfigure}[b]{0.475\textwidth}
		\centering
		\input{../Figures/lawInvariance2.tikz}
		\caption{$\overline{\AVAR}_{\alphaLI}=2$}
	\end{subfigure}
	\begin{subfigure}[b]{0.475\textwidth}
		\centering
		\input{../Figures/lawInvariance1.tikz}
		\vspace*{7mm}
		\caption{$\overline{\AVAR}_{\alphaLI}=1.8$}
	\end{subfigure}
	\caption{The node values at the final stage of these two trees have the same probability distribution, but  $\overline{\AVAR}$ assigns them a different risk.}
	\label{fig:LI}
\end{figure}

In the problem considered here, $r_t$ is a multistage nested risk measure. Such risk measures are generally speaking not law invariant, as is illustrated for $\overline{\AVAR}$ in \cref{fig:LI}, which means that the method described above can't be used. A way around is to first find a $\beta^\star$ such that
\begin{equation}
	\bar{\rho_t}[1_{E_t}] \leq \AVAR_{\beta^\star}[1_{E_t}]\
\end{equation}
for any sets $E_t$, for which it suffices that the epigraph of $\AVAR_{\beta^\star}$ is a subset of the epigraph of $\bar{\rho_t}$:
\begin{equation}\label{eq:epi-set-inclusion}
	\epi \AVAR_{\beta^\star} \subseteq \epi \bar{\rho_t}.
\end{equation}
Using the definition of $\AVAR_\alpha$, its epigraph on an $n$-dimensional probability space with probability vector $\pi$ can be written as the following polyhedron
\begin{align}
	\epi \AVAR_{_\alpha} &= 
	\left\{(Y,\gamma)\in \REAL^{n+1} \Middle \boldinf_t  t+ \frac{1}{\alpha}\EX[(Y-t)_+]\leq \gamma\right\}\\
	&= \left\{(Y,\gamma)\in \REAL^{n+1} \Middle \exists t\in \REAL\colon  t+ \frac{1}{\alpha}\pi^\top(Y-t)_+\leq \gamma\right\}\\
	&= \left\{(Y,\gamma)\in \REAL^{n+1} \Middle
	\renewcommand{\arraystretch}{1.2}
	\begin{tabular}{@{}l@{}}
		$\exists t\in \REAL,\; \xi \in\REAL_{\geq0}^n\colon$\\
		$Y-t\leq \xi,\; t+\frac{1}{\alpha}\pi^\top \xi\leq \gamma$
	\end{tabular}
	\right\}.\label{eq:epiAVAR}
\end{align}
where $n$ is the number of nodes at stage $t$. This polyhedron can concisely be written as 
\begin{equation}
	\epi \AVAR_{\alpha} = \left\{x\in\REAL^{n+1} \Middle Gx\leq g\right\}. \label{eq:epiAVAR-simple}
\end{equation}
To obtain this formulation, a polyhedral set of the form $\{x = (Y,\gamma, t,\xi)\in\REAL^{2n+2}|$ $G'x\leq g'\}$ is first constructed, where $G'x\leq g'$ implements the constraints in \cref{eq:epiAVAR}. This set is then projected onto its first $n+1$ dimensions to obtain \cref{eq:epiAVAR-simple}. A common algorithm for performing such projections is \textit{Fourier-Motzkin elimination} (FME), which was introduced by Fourier in 1826 \cite{Fourier} and rediscovered by Motzkin in 1936 \cite{Motzkin}. 

If $\bar{\rho_t}$ is a composition of $\AVAR_{\beta}$-type conditional risk mappings, then $\epi \bar{\rho_t}$ is a polyhedral set as well, given by \cref{eq:epi_nested}. Using the method described above, this polyhedral set can also be written as
\begin{equation}
	\epi \bar{\rho_t} = \left\{x\in\REAL^{n+1} \Middle Hx\leq h\right\}. \label{eq:epinAVAR-simple}
\end{equation}
Using \cref{eq:epinAVAR-simple,eq:epiAVAR-simple}, the set inclusion in \cref{eq:epi-set-inclusion} can be seen to hold if $Hy\leq h$ for any $y\in \epi \AVAR_{\beta^\star}$, or equivalently if
\begin{equation}\label{eq:LP}
	\max_{Gy\leq g} H_i^\top x \leq h_i
\end{equation}
holds for all $i$, where $H_i$ is the $i$-th row of $H$ and $h_i$ the $i$-th element of $h$. The smallest $\beta^\star$ for which \cref{eq:epi-set-inclusion} holds, can now be determined using the bisection method described in \cref{alg:bisection}.
It should be noted that the projections involved in obtaining the two polyhedral sets is the most computationally expensive step in this method. Especially for $\epi \bar{\rho_t}$, where the number of dimensions that need to be eliminated is proportional to the number of nodes in the tree. For $\epi \AVAR_{\alpha}$, this number is equal to $\nodes(t)+1$. There might, however, be a way to check the epigraph inclusion, without explicitly performing the projections. This would be an interesting search direction for future work. 

\begin{algorithm}
	\DontPrintSemicolon
	\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
	\Input{Tolerance $\epsilon$, }
	\Output{$\beta^\star$}
	Construct $\overline{\AVAR}_\beta$\;
	$a\leftarrow 0$\;
	$b\leftarrow 1$\;
	\While{$b-a>\epsilon$}{
		$\beta^\star\leftarrow \frac{a+b}{2}$\;
		Construct $\epi \AVAR_{\beta^\star}$\;
		\eIf(\tcp*[h]{Check via \cref{eq:LP}}){$\epi \AVAR_{\beta^\star} \subseteq \epi \bar{\rho_t}$}{
			$a\leftarrow \beta^\star$\;
		}{
			$b\leftarrow \beta^\star$\;
		}
	}
	\caption{Bisection algorithm for determining $\beta^\star$.}
	\label{alg:bisection}
\end{algorithm}

The last step is now to find an $\alpha^\star$ such that $\Prob[E_t]\leq \alpha^\star$ implies $\AVAR_{\beta^\star}[1_{E_t}]\leq \alpha$. This is done using $\AVAR_{\beta^\star}[1_{E_t}] =\min\{1,\frac{1}{\beta^\star}\Prob[E_t]\}$ and \cref{eq:AS-max-pt}:
\begin{align}
	\alpha^\star &= \max \left\{x\in [0,1] \Middle \min\{1,\frac{x}{\beta^\star}\}\leq \alpha\right\}\\
	&= \alpha \beta^\star.
\end{align}
With this $\alpha^\star$, the chance constraint $\Prob[E_t]\leq \alpha^\star$ implies $\AVAR_{\beta^\star}[1_{E_t}]\leq \alpha$, and since $\beta^\star$ was chosen such that $\AVAR_{\beta^\star}$ is always larger than $\overline{\AVAR}_\beta$, this in turn implies that $\bar{\rho}_t[1_{E_t}]\leq \alpha$.  The value of $\beta^\star$ can consequently be seen as the scaling factor needed to switch between ambiguous chance constraints and regular ones.



\section{Early results}\label{s:AS-results}
In \cref{fig:AS-3D2,fig:AS-3D3}, the values of $\beta^\star$ are shown for different nested risk measures $\overline{\AVAR}_\beta$, for horizon length $2$ and $3$ respectively. For larger horizons, the ambiguity propagates and grows, and smaller $\beta^\star$ are needed\footnote{For $\beta^\star=0$, the $\AVAR_{\beta^\star}$ ambiguity set is equal to the entire probability simplex and the ambiguity is maximal.}. For horizon length $4$, \MATLAB{} halted during the projection needed to obtain the polyhedral set of the nested risk measure.


\begin{figure}
	\centering
	\pgfplotstableread{"../FigData/betaStar/3D_horizon2_branch2_tol1.00e-03_numberOfalpha21_numerOfp11.txt"}{\plotdata}
	\tikzsetnextfilename{alphaStar3D_2}
	\input{../Figures/alphaStar3D.tikz}
	\caption{$\beta^\star$ for different nested risk measures $\overline{\AVAR}_\beta$ when using a binary tree with a horizon of $2$ and probability vector $[p\quad 1-p]^\top$ for different $p$.}
	\label{fig:AS-3D2}
\end{figure}

\begin{figure}
	\centering
	\pgfplotstableread{"../FigData/betaStar/3D_horizon3_branch2_tol1.00e-03_numberOfalpha21_numerOfp11.txt"}{\plotdata}
	\tikzsetnextfilename{alphaStar3D_3}
	\input{../Figures/alphaStar3D.tikz}
	\caption{$\beta^\star$ for different nested risk measures $\overline{\AVAR}_\beta$ when using a binary tree with a horizon of $3$ and probability vector $[p\quad 1-p]^\top$ for different $p$.}
	\label{fig:AS-3D3}
\end{figure}

In \cref{fig:AS-2D}, the resulting $\beta^\star$-values from \cref{alg:bisection} are shown for different nested risk measures $\overline{\AVAR}_\beta$ with conditional probability vector $\pi^\top = \rvect{0.25,0.5,0.25}$ and branching horizon 1 and 2. At first it might surprise that $\beta$ and $\beta^\star$ is not identical when the branching horizon is 1, since MNRMs and SWRMs are equivalent in that case. The reason why $\beta^\star$ never goes below $0.25$ is that the $\AVAR_{\beta^\star}$-ambiguity set for $\pi^\top = \rvect{0.25,0.5,0.25}$ is already the entire simplex at $\beta^\star=0.25$. Going below that value would make no difference. Finally, it is also important to note that $\beta^\star$ can be both larger and smaller than $\beta$, so there is no simple inequality relation between the two.

\begin{figure}
	\centering
	\input{../Figures/alphaStar.tikz}
	\caption{$\beta^\star$ for different nested risk measures $\overline{\AVAR}_\beta$ and different branching horizons with conditional probability vector $\pi^\top = \rvect{0.25,0.5,0.25}$.}
	\label{fig:AS-2D}
\end{figure}

\section{Conclusion}
In \cref{s:example}, the use of multistage nested risk constraints was motivated with an example. Next, \cref{s:ACS} gave a use case for MNRCs. Motivated by the fact that the implementation of MNRCs requires numerous auxiliary variables and therefore increases the problem size significantly, \cref{s:AS} introduced a method for over-approximating them with stage-wise $\AVAR$-risk constraint. The method is rather computationally expensive as well because it involves polyhedral projections, but the benefit is that these computations can be performed in advance. Investigating the possibility of avoiding the polyhedral projections is suggested as a direction for future work. The results presented in \cref{s:AS-results} are still preliminary, but they show that the method for finding an overapproximating $\AVAR_{\beta^\star}$ can be used in practice. A further case study is needed, however, to investigate how close the approximation is and how much there is to gain computationally.


%From the definition of the average value-at-risk, the following formulation can be derived for $\AVAR_{\alpha}$:
%\begin{align}
%	\phantom{\text{3.7 revisited}}\AVAR_\alpha[Y] &= \boldinf_{t} t+ \frac{1}{\alpha}\EX[(Y-t)_+]\tag{\ref{eq:avar-definition} revisited}\\
%	&= \boldinf_{t\in \REAL,\;\xi\in\REAL_{\geq0}^n} t+ \frac{1}{\alpha}\pi^\top\xi \quad \st \; Y-t\leq\xi
%\end{align}
%Using this, the epigraph of $\AVAR_{\alpha}^\lambda$ can be found as
%\begin{align}
%	\epi \AVAR_{\alpha}^{\lambda} &= 
%	\left\{(Y,\gamma)\in \REAL^{n+1} \Middle 
%	(1-\lambda)\AVAR_{\alpha}[Y] + \lambda\pi^\top Y\leq \gamma\right\}\\
%	&= \left\{(Y,\gamma)\in \REAL^{n+1} \Middle
%	\renewcommand{\arraystretch}{1.2}
%	\begin{tabular}{@{}l@{}}
%		$\exists t\in \REAL,\; \xi \in\REAL_{\geq0}^n\colon Y-t\leq \xi$ \\
%		$(1-\lambda)(t+\frac{1}{\alpha}\pi^\top \xi) + \lambda\pi^\top Y\leq \gamma$
%	\end{tabular}
%	\right\}.
%\end{align}


%\begin{equation}
%	\text{epiAV@R}^\lambda_\alpha =
%	\left\{
%	\begin{bmatrix}
%		X\\
%		a
%	\end{bmatrix}
%	\in  \mathbb{R}^{n+1}
%	\Middle
%	\begin{tabular}{@{}l@{}}
%		$\exists y_{1} \in \mathbb{R}_{\geq 0}^{n}, y_{2} \in \mathbb{R}_{\geq 0}^{n},$ \\
%		$y_{3} \in \mathbb{R} \colon y_{1}-y_{2}+y_{3} 1_{n}^{\top}=X$                        \\
%		$\lambda^{\prime} \pi^{\top} y_{1}-\lambda \pi^{\top} y_{2}+y_{3} \leq a$
%	\end{tabular}
%	\right\}
%\end{equation}
