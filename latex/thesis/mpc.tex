\chapter{Model predictive control}\label{chap:mpc}
This chapter discusses the APC method \textit{model predictive control}, or MPC. In \cref{s:oc}, the concept of optimal control (OC) is introduced. This provides the main building block needed for the MPC formulation in \cref{s:mpc}. Finally, in \cref{s:uncertain-mpc}, two ways of dealing with uncertainty in MPC are discussed.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Optimal control}\label{s:oc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The general goal of control is to manipulate the inputs of a dynamical system, or plant, in order to achieve desirable behavior or outputs. In optimal control, the applied input values are determined by minimizing a cost function over a usually finite prediction horizon.

To make this concrete, assume that the plant can be described by following discrete-time nonlinear system:
\begin{equation}
	x_{t+1} = f(x_t,u_t), \label{eq:sysplus}
\end{equation}
where $u_t \in \REAL^{n_u}$ is the input applied to the system and $x_t \in \REAL^{n_x}$ is a vector describing the state of the system, both at time $t$. The discrete state-transition map $f\colon \REAL^{n_x}  \times \REAL^{n_u} \to \REAL^{n_x}$ is usually a discretization of a set of nonlinear differential equations. In this setting, a general optimal control problem (OCP)  can be formulated as
\begin{subequations}
	\label{eq:mpc}
	\begin{alignat}{2}
		\boldminim_{u_0,\dots u_{N_1}} \quad &\sum_{t=0}^{N-1}\ell(x_t, u_t) + \ell_N(x_{N})&&\\
		\st \quad & x_0 = x,&&\\
		& x_{t+1} = f(x_t, u_t),&\quad&t\in \NAT_{[0,N-1]}, \label{eq:mpc:state_transition}\\
		&(x_t, u_t) \in \Zset \subseteq \REAL^{n_x} \times \REAL^{n_u},&&t\in \NAT_{[0,N-1]},\label{eq:mpc-constraint}\\
		&x_{N} \in \Xset_f,&&
	\end{alignat}
\end{subequations}
where $\ell$ is the stage cost, $\ell_N$ is the terminal cost, $x$ is the initial state, $N$ is the prediction horizon, $\Zset$ is the set of allowed state-input combinations and $\Xset_f \subseteq \REAL^{n_x}$ constrains the final state. If there are no mixed constraints, then  constraint (\ref{eq:mpc-constraint}) becomes $x_t \in \Xset \subseteq \REAL^{n_x}$ and $u_t \in \Uset \subseteq \REAL^{n_x}$ or equivalently $\Zset = \Xset \times \Uset$. This formulation corresponds to that used in \cite[Chap. 2]{MPC}.

The result of the optimization is the control sequence $u_0^\star$, $u_1^\star$, $\dots$, $u_{N-1}^\star$. A naive way of using these controls would be to simply apply them to the system over the next $N$ time instants. In such an open-loop implementation, however, the behavior of  the system can quickly diverge from that predicted by the model due to several reasons including model inaccuracies, discretization errors and measurement errors on the initial state. The next section will discuss MPC, which addresses this problem.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Model predictive control}\label{s:mpc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In MPC, the OCP from (\ref{eq:mpc}) is solved repeatedly. This means that only the first input $u_0^\ast$ is applied to the system and the optimization is repeated at the next time instant, starting with a new measurement for the initial state. This way, feedback is introduced. When the prediction horizon stays constant and advances forward step by step, this method is called \textit{receding horizon} MPC. The general idea is visualized in \cref{fig:mpc}. When the plant operates only up to a fixed final time, then a shrinking prediction horizon can be used such that the end of the horizon matches with the final operation time.

\tikzFig{mpcText}{Visualization of receding horizon MPC.}{mpc}

Depending on the type of stage costs, MPC methods can be divided into tracking MPC and economic MPC. In tracking MPC, the control system is decomposed into two levels. In the first level, an optimal \textit{steady-state} operating point is determined taking into account constraints and operating costs. Then, in the second level, MPC is used to drive the plant to that optimal operating point \cite{EMPC} as quickly as possible. In this case, the cost functions are chosen to be positive semi-definite with respect to the steady-state $x_{s}$ and $u_{s}$. This means that 
\begin{align}
	\left. 
		\begin{array}{l}
			0 = \ell(x_s, u_s) \leq \ell(x, u)\\
			0 = \ell_f(x_s) \leq \ell_f(x)\\
		\end{array}
	\right\} \quad \forall (x,u) \in \Zset.
\end{align}
Although this hierarchical setup usually saves on computational complexity, it does not take into account the transient costs associated with reaching the setpoint \cite{EMPC}. Especially when the setpoint often changes, and the plant is therefore often not in steady state, this can be a problem.

In economic MPC (EMPC) on the other hand, the stage costs directly reflect the economics of the plant. Typically, these stage costs would include the price of the raw materials, the price of the utilities needed to operate the plant, and the value of the product \cite[Chap. 2.8]{MPC}. Suppose, for example, that the amount of product produced is a function $P\colon \REAL^{n_x} \to \REAL, x \mapsto P(x)$. The stage costs could then include a term $-P(x)^2$, representing a profit or negative cost. A consequence of this is that the minimizer of the stage cost is not necessarily an equilibrium point. There might be a feasible combination $(u,x)$ for which the stage cost is lower than that in the optimal steady-state operating point.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Uncertainty in MPC}\label{s:uncertain-mpc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Now suppose that the state transitions are no longer deterministic, but depend on the realizations of a stochastic process $w = (w_t)_{t=0}^{N}$ in $\Wset \subseteq \REAL^{n_w}$. These random disturbances can represent both measurement errors and model disturbances. The new state transition then becomes
\begin{equation}
	x_{t+1} = f(x_t,u_t,w_t). \label{eq:sysplusxi}
\end{equation}

If this new, stochastic state transition would be used in the original OCP instead of \cref{eq:mpc:state_transition}, the objective function would become a random variable. There is, however, no straightforward answer to what it means to minimize a random variable. Two popular ways to deal with uncertainty in MPC are the worst-case and the stochastic approach \cite{Sopasakis2019RAMPC}. 

\subsection{Worst-case MPC}
In worst-case MPC, the uncertainties or disturbances $w_t$ are assumed to be bounded and the worst-case cost is minimized \cite{MPC}. There are two variants of worst-case MPC, namely open- and closed-loop \cite{minimax}. The closed-loop variant uses the following OCP:

\begin{subequations}
	\label{eq:minimaxClosed}
	\begin{alignat}{2}
		\boldminim_{u_0} \quad&
		\mathrlap{
			\begin{aligned}[t]
				\ell(x_0,u_0) &+ \boldmax_{w_1 \in \Wset} \bigg[\boldmin_{u_1}\ell(x_1,u_1) \\
				&+ \boldmax_{w_2 \in \Wset} \Big[\boldmin_{u_2}\ell(x_2,u_2) +\dots\\
				&+ \boldmax_{w_N \in \Wset} \big[\ell_N(x_N) \big]\dots \Big] \bigg]\\
		\end{aligned}}&&\\
		\st \quad &  x_0 = x,&&\\
		&x_{t+1} = f(x_t,u_t,w_t),&\quad&t\in \NAT_{[0,N-1]}\\
		&(x_t, u_t) \in \Zset \subseteq \Xset \times \Uset,&&t\in \NAT_{[0,N-1]}\\
		&x_{N} \in \Xset_f.&&
	\end{alignat}
\end{subequations}
It is called closed-loop because of the nesting of the minimizers, which reflects that at future time steps the best input will be selected given the information that is then available. For the open-loop variant, the constraints stay the same, but the objective function is first maximized over all possible disturbances and then minimized over all inputs:

\begin{equation}
	\label{eq:minimaxOpen}
	\begin{aligned}
		\boldminim_{u_0, u_1 \dots, u_{N-1}} \boldmax_{w_1, w_2\dots, w_N \in \Wset} \sum_{t=0}^{N-1} \ell(x_t,u_t) + \ell_N(x_N)\\
	\end{aligned}
\end{equation}
This approach does not take into account the feedback that will be applied in the future and is therefore unnecessarily conservative \cite{minimax}. Because of the combination of minimizers and maximizers, worst-case MPC is often referred to as minimax MPC. 

Though less conservative than open-loop minimax MPC, the closed-loop variant is still very conservative. It prepares for the worst case without taking into account the possibly low probabilities of such high effect events. 

\subsection{Stochastic MPC}
Stochastic or \textit{risk-neutral} MPC does not have the problem of being conservative, because instead of the worst-case cost, the average cost is minimized \cite{SMPCreview}. Similar to the closed-loop minimax approach, the OCP for stochastic MPC (SMPC) can be written as \cite[Sec. 3.1]{SHAPIRO}:

\begin{equation}
	\label{eq:smpc}
	\begin{aligned}
		\boldminim_{u_0} \ell(x_0,u_0) &+ \EX_{w_0} \bigg[\boldmin_{u_1}\ell(x_1,u_1)\\
		&+ \EX_{w_1} \Big[\boldmin_{u_2}\ell(x_2,u_2) +\dots\\
		&+ \EX_{w_{N-1}} \big[\ell_N(x_N) \big]\dots \Big] \bigg]\\
	\end{aligned}
\end{equation}
subject to the same constraints as \ref{eq:minimaxClosed}. Though it is less conservative than robust MPC, this approach also has its drawbacks. Firstly, the distributional information about the uncertainties needed for practical implementations can not always be estimated from available data. Another problem is that the expectation operator does not account for the shape or the right tail of the distribution, it is said to be \textit{risk-neutral}.

Worst-case MPC and stochastic MPC can be seen as two extreme ways of dealing with \textit{high effect low probability} (HELP) events. While worst-case MPC focuses on avoiding the high effect events, the stochastic approach focuses on best performance on average without regard for high effect events. Risk-averse MPC provides a way of interpolating between the two, using risk measures. These mathematical tools are discussed in \cref{chap:rampc}.
