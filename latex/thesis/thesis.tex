 % !TeX program = luatex
\documentclass[master=ewit, inputenc=utf8]{kulemt}
%\includeonly{results}
\setup{% Remove the "%" on the next line when cusing UTF-8 character encoding
 inputenc=utf8,
  title={Risk-averse, risk constrained model predictive control of bioreactors},
  author={Maurits Descamps},
  promotor={Prof. dr. ir. J. Van Impe \and Prof. dr. ir. P. Sopasakis},
  assessor={Prof. dr. ir. P. Patrinos \and Prof. dr. ir. W. Michiels},
  assistant={Dr. ir. Satyajeet Bhonsale \and Dr. ir. Mihaela Sbarciog}}
% Remove the "%" on the next line for generating the cover page
%\setup{coverpageonly}
% Remove the "%" before the next "\setup" to generate only the first pages
% (e.g., if you are a Word user).
%\setup{frontpagesonly}
% Choose the main text font (e.g., Latin Modern)
\setup{font=lm}

% Bibliography
\usepackage[backend=biber, style=numeric-comp, sorting=none, doi=false,isbn=false,url=false,eprint=false]{biblatex}
\addbibresource{../biblioThesis.bib}



%Inputs
\input{../preambleMath.tex}
\input{../preambleTables.tex}
\input{../preamblePlotting.tex}
\usetikzlibrary{external}
\tikzexternalize
\tikzsetexternalprefix{./build/}
\input{../preambleColors.tex}

%Other
\usepackage{microtype}
\usepackage{csquotes}
\usepackage{tocloft}
\setlength{\cftpartnumwidth}{2em}
\setlength{\cftchapterindent}{2em}
\setlength{\cftchapternumwidth}{1.5em}
\setlength{\cftsectionindent}{3.5em}

\usepackage{subcaption}
\usepackage{todonotes}
\newcommand{\todoQ}[1]{\todo[linecolor=blue,backgroundcolor=blue!25,bordercolor=blue]{#1}}
\makeatletter
\renewcommand{\todo}[2][]{\tikzexternaldisable\@todo[#1]{#2}\tikzexternalenable}
\makeatother


\DeclarePairedDelimiterX{\rvect}[1]{[}{]}{\,\makervect{#1}\,}
\ExplSyntaxOn
\NewDocumentCommand{\makervect}{m}
{
	\seq_set_split:Nnn \l_tmpa_seq { , } { #1 }
	\setlength\arraycolsep{3pt}\begin{matrix}
		\seq_use:Nn \l_tmpa_seq { & }
	\end{matrix}
}
\ExplSyntaxOff

%\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}
\usepackage[pdfusetitle,hidelinks,plainpages=false]{hyperref}
\usepackage[capitalise, nameinlink, noabbrev]{cleveref}

%\includeonly{chap-n}
\begin{document}
\begin{preface}
This thesis marks the end of a five-year engineering degree and an important crossroads in my life. I will use this opportunity to thank the numerous people who have helped me along the way.

First of all, I want to thank my parents, Wim and Kathy, for providing a warm and carefree environment and for inspiring me to achieve my ambitions. My sister Pauline deserves a special thanks as well because there was never a dull moment with you at home.

I would also like to thank my mentors and promoters for guiding me through this adventure. I use the word "adventure" because writing this thesis made me venture into uncharted territory in multiple ways. One year ago I knew only vaguely what bioreactors were, but when I read the thesis proposal my curiosity was sparked. Thank you professor Van Impe for providing this thesis opportunity and for introducing me to the fascinating world of bioreactors. For helping me navigate the realm of risk, chance and uncertainty, I would like to thank professor Sopasakis. While I first saw uncertainty as an annoying problem, I can now appreciate the beauty in the methods we use to deal with it. Besides the subject of the thesis, also the circumstances under which it was written were new to me and, to some degree, to everyone involved. Coordinating this project during these strange past months was challenging at times. Thank you Satyajeet and Mihaela for giving me all the support I needed, despite the constraints imposed on us by the pandemic.

Finally, I am grateful for all the beautiful people I have met along the way. Although my engineering degree ends here, I am confident that the friendships that were forged in Kortrijk, Leuven and München will last, whatever road I take.


\end{preface}

\tableofcontents

%English abstract
\begin{abstract}
The primary objective of this thesis is to study \textit{risk-averse, risk-constrained model predictive control} (RRMPC) and its potential as a way of controlling bioreactors under uncertainty. Regular MPC is a widespread advanced control method that provides a framework for controlling multiple-input multiple-output systems under constraints. The key idea behind it is the repeated solution of a constrained optimization problem or optimal control problem. The method does, however, generally assume that all model parameters are perfectly known, which is rarely the case in practice. Some variables might only be known by estimation, or they might vary from time to time according to a random process. Two popular approaches have been used in the past to deal with this uncertainty in MPC, namely stochastic and worst-case MPC. The method studied here unifies these two approaches and provides a way of interpolating between them.

The key idea behind risk-averse MPC is the use of risk measures, which are functions that map random variables to real numbers. Using these risk measures, the objective function in the optimal control problem, which becomes a random variable under uncertainty, can be mapped to a real number that can be minimized. Additionally, these risk measures can also be used for a stochastic interpretation of constraints. The most common way of incorporating uncertainty in constraints is to limit the probability of constraint violations. These chance constraints do, however, lead to computationally expensive optimization problems. Using risk measure-based constraints, or risk constraints, addresses this problem.

To investigate the potential of the method for bioreactor control, it is applied to the penicillin fermentation process. The control objective in this problem is to maximize the concentration of penicillin at the end of the batch. During the practical implementation, twlatelao main challenges arose. Firstly, when using risk constraints the optimal control problem became unfeasible under certain conditions. This suggests that more research is needed towards a recursively feasible formulation for this specific application. The second challenge is the size of the optimization problem, which grows exponentially with the level of detail in the description of the uncertainty.
\end{abstract}

%\renewcommand{\abstractname}{Abstract - Nederlands}
%\begin{abstract}
%Het hoofddoel van deze thesis is het bestuderen van risicomijdende, risicobegrensde model-predictieve controle (RRMPC) en haar potentieel als techniek voor het regelen van bioreactoren in de aanwezigheid van onzekerheid. Standaard model-predictieve controle (MPC) is een veelgebruikte geavanceerde regeltechniek die een kader voorziet voor het regelen van van systemen met meerdere in- en uitgangen onderhevig aan randvoorwaarden. De kerngedachte in MPC is het herhaaldelijk oplossen van een begrensd optimalisatieprobleem of optimale controle-probleem (OCP). De methode veronderstelt in het algemeen dat alle modelparameters perfect gekend zijn, maar dat is in werkelijkheid zelden het geval. Het is mogelijk dat sommige variabelen enkel geschat kunnen worden, of dat ze afhankelijk zijn van een stochastisch proces. Twee methodes zijn in het verleden voorgesteld om met onzekerheid om te gaan in het kader van MPC, namelijk stochastische MPC en worst-case MPC. De techniek die in deze tekst bestudeerd wordt, verenigt de twee, en voorziet een manier om ertussen te interpoleren.
%
%Centraal in RRMPC staan risicomaten, wat wiskundige instrumenten zijn die stochastische variabelen omzetten naar reële getallen. De kostfunctie van het optimale controle-probleem, die een stochastische variabele wordt in de aanwezigheid van onzekerheid, kan via dergelijke risicomaten omgezet worden naar een reëel getal dat geminimaliseerd kan worden. Daarnaast kunnen risicomaten ook gebruikt worden voor een stochastische interpretatie van grensvoorwaarden. De meest voorkomende manier om onzekerheid en grensvoorwaarden te verenigen, is het begrenzen van de kans dat een grens overschreden wordt. Dergelijke kansgrenzen leiden echter tot computationeel dure optimalisatieproblemen. Grensvoorwaarden gebaseerd op risicomaten, of risicogrenzen, bieden een efficiënter alternatief.
%
%Om het potentieel van de techniek voor de regeling van bioreactoren te onderzoeken, wordt ze toegepast op het penicilline fermentatieproces. Het doel in dit proces is het maximaliseren van de penicillineconcentratie in de reactor aan het einde van de looptijd. Tijdens de praktische RRMPC implementatie kwamen twee uitdagingen naar voor. Ten eerste waren er numerieke problemen wanneer risicogrenzen gebruikt werden. De tweede uitdaging was de grootte van het optimalisatieprobleem die exponentieel toeneemt met de nauwkeurigheid van de beschrijving van de onzekerheid. 
%\end{abstract}

% A list of figures and tables is optional
%\listoffigures
%\listoftables
% If you only have a few figures and tables you can use the following instead
\listoffiguresandtables
% The list of symbols is also optional.
% This list must be created manually, e.g., as follows:
\chapter{List of Abbreviations and Symbols}
\section*{Abbreviations}
\begin{flushleft}
  \renewcommand{\arraystretch}{1.1}
  \begin{tabularx}{\textwidth}{@{}p{15mm}X@{}}
  	APC&Advanced Process Control\\
  	PID&Proportional-Integral-Derivative\\
  	%OC&Optimal Control\\
  	OCP&Optimal Control Problem\\
    MPC   & Model Predictive Control \\
    EMPC & Economic Model Predictive Control\\
    SMPC & Stochastic Model Predictive Control\\
    RAMPC & Risk-averse Model Predictive Control\\
    RRMPC& Risk-averse, Risk-constrained Model Predictive Control\\
    MNRM& Multistage Nested Risk Measure\\
    MNRC& Multistage Nested Risk Constraint\\
    SWRC& Stage-Wise Risk Constraint\\
    HELP	& High Effect, Low Probability
  \end{tabularx}
\end{flushleft}
\section*{Symbols}
\begin{flushleft}
  \renewcommand{\arraystretch}{1.1}
  \begin{tabularx}{\textwidth}{@{}p{15mm}X@{}}
    $1_n$   & Column vector of $n$ ones \\
    $0_n$   & Column vector of $n$ zeros \\
    $\VAR$   & Value-at-risk \\
    $\AVAR$ & Average  value-at-risk \\
    $\EVAR$ & Entropic  value-at-risk \\
    $\mathcal{D}_n$ & Probability simplex in $n$ dimensions\\
    $\epi$ & Epigraph\\
    $\ambiguity$&Ambiguity set\\
    $X$&Biomass concentration\\
    $S$&Substrate concentration\\
    $P$&Product concentration\\
    $V$&Volume\\
    $\Delta u_t$& $u_t-u_{t-1}$
  \end{tabularx}
\end{flushleft}

% Now comes the main text
\mainmatter
\part{Introduction and literature study}
\include{intro}
\include{mpc}
\include{risk-measures}
\include{bioreactors}
\part{Design and methodology}
\include{application}
\include{risk-constraints}
\part{Results and conclusion}
\include{results}
\include{conclusion}

% If you have appendices:
%\appendixpage*          % if wanted
%\appendix

\backmatter
% The bibliography comes after the appendices.
% You can replace the standard "abbrv" bibliography style by another one.
\printbibliography
\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
