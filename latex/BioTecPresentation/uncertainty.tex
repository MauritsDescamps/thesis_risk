\section{Dealing with uncertainty}
\begin{frame}{Sources of uncertainty}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\centering
				\input{../Figures/reactor-uncertainty.tikz}
			\end{figure}
		\end{column}
		\begin{column}{0.5\textwidth}
			Uncertainty in:
			\pause
			\begin{itemize}[<+->]
				\item Feed concentration $S_{in}$
				\item Kinetics
			\end{itemize}
			\begin{block}<+>{}
				Initial focus on uncertainty in $S_{in}$
			\end{block}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Scenario trees}
	\begin{columns}[t]
		\begin{column}{0.5\linewidth}
			\input{../Figures/tree3.tikz}
		\end{column}
		\begin{column}{0.5\linewidth}
			\begin{block}{}
				Assume $S_{in} \in \{\omega_1, \cdots \omega_n\}$\\
				at every time step.\\
				$\rightarrow$ Discrete prob. distribution
			\end{block}
		\end{column}
	\end{columns}
	\onslide<2>{
		\vspace*{-0.5cm}
		\begin{block}{\centering Trade off}
			\begin{minipage}{0.4\linewidth}
				\centering
				Good representation of real distribution\footnotemark
			\end{minipage}%
			\begin{minipage}{0.2\linewidth}
				\centering
				VS
			\end{minipage}%
			\begin{minipage}{0.4\linewidth}
				\centering
				Simple tree, low computational complexity
			\end{minipage}
		\end{block}
		\only<2->{\footnotetext{\cite{treeGeneration}}}
	}
\end{frame}

\begin{frame}{Problem}
	\begin{align*}
		\text{Minimize} \quad &\sum_{i=0}^{N-1} \alert<3->{-P_{i+1}^2}+ \frac{1}{2}\Delta u_i^2\\
		\text{subject to} \quad &\alert<2->{x_{i+1}} = f(x_i, u_i, \alert{S_{in}}),\\
		&u_{min} \leq u_i \leq u_{max},\\
		&\alert<3->{-X_i }\leq X_{max}
	\end{align*}
	\onslide<4->{$\rightarrow$ Objective is now a random variable.}
	\part{\begin{block}<5>{Question}
		How to minimize a random cost?
	\end{block}}
\end{frame}

\begin{frame}{Two popular approaches}
	\begin{figure}\centering
		\input{../Figures/worstcase.tikz}
		\vspace{-1cm}
	\end{figure}
	\begin{columns}[t]
		\begin{column}<2->{0.45\textwidth}
			\begin{block}{Stochastic}
				\begin{itemize}
					\item Minimize expected value of the cost
					\item Downside: No regard for tail of distribution
				\end{itemize}
			\end{block}
		\end{column}
		\begin{column}<3->{0.45\textwidth}
			\begin{block}{Robust}
				\begin{itemize}
					\item Minimize worst-case cost\\
					\item Downside:\\
					very conservative
				\end{itemize}
			\end{block}
		\end{column}
	\end{columns}
	\begin{block}<4>{}
		Can we do something in between?
	\end{block}
\end{frame}

\begin{frame}{Risk measures}
	\begin{itemize}
		\item A risk measure $\rho$ maps random variables  $Z$ to real numbers\\
		$\rightarrow$ just like the average and maximum operators
		\item Interpolation between stochastic and robust approach
	\end{itemize}
	\begin{figure}\centering
		\input{../Figures/risk.tikz}
	\end{figure}
\end{frame}

\begin{frame}{Coherent risk measures}
	Special class of risk measures with nice properties.
	\begin{block}{Requirements for coherent risk measures\footnotemark}
		\begin{enumerate}
			\itemsep1em
			\item Convexity:
			$\rho(\lambda Z+(1-\lambda)Z') \leq \lambda\rho(Z) + (1-\lambda)\rho(Z') \quad \forall \lambda\in [0,1]$
			\item Monotonicity: If $Z_i \leq Z_i'$ for all $i \iffalse \in \N_{[1,n]}\fi$, then $\rho(Z) \leq \rho(Z')$
			\item Translational equivariance: $\rho(Z+a)=\rho(Z)+a$ with $a\in\REAL$
			\item Positive homogeneity: $\rho(tZ)=t\rho(Z)$ with $0<t\in\REAL$
		\end{enumerate}
	\end{block}
	Examples: 
	\begin{itemize}
		\item Average value-at-risk ($\AVAR$)
		\item Entropic value-at-risk ($\EVAR$)
	\end{itemize}
	\footnotetext{\cite[Sec. 6.3]{SHAPIRO}}
\end{frame}

\begin{frame}{Average value-at-risk}
	\vspace{1cm}
	\begin{equation*}
		\AVAR_{\alert<1>{\alpha}}(Z) \coloneqq \inf_{t\in \REAL}\{ t+\alert<1>{\alpha}^{-1} \EX[Z-t]_+\} \quad \text{with } 0 \leq \alpha \leq 1
	\end{equation*}
	\vskip0pt plus 1filll
	\begin{figure}
		\centering
		\only<2>{\input{../Figures/avar-risk100.tikz}}
		\only<3>{\input{../Figures/avar-risk50.tikz}}
		\only<4>{\input{../Figures/avar-risk15.tikz}}
		\only<5>{\input{../Figures/avar-risk0.tikz}}
	\end{figure}
\end{frame}

\begin{frame}{Risk constraints}
	What about random variables in the constraints?
	\begin{table}
		\centering
		\begin{tabular}{cl}
			$\alert{X_i} \leq X_{max}$&\\
			\onslide<2->{$\downarrow$&\\
				$P[X_i\geq X_{max}] \leq \alpha \quad$& Difficult to enforce\\}
			\onslide<3->{$\downarrow$&\\
				$\AVAR_\alpha[X_i] \leq X_{max} \quad$& Easier to enforce\\}
			\onslide<4->{$\implies$&\\
				$P[X_i\geq X_{max}] \leq \alpha$&\\}
		\end{tabular}
	\end{table}
\end{frame}