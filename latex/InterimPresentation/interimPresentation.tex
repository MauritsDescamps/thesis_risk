\documentclass[]{beamer}
\usepackage[backend=biber, style=authortitle, doi=false,isbn=false,url=false,eprint=false]{biblatex}
\addbibresource{../biblioThesis.bib}

\beamertemplatenavigationsymbolsempty
%\includeonlyframes{do}

%Inputs
\input{../preambleMath.tex}
\input{../preambleTables.tex}
\input{../preamblePlotting.tex}
\usetikzlibrary{external}
\tikzexternalize
\tikzsetexternalprefix{./build/}
\input{../preambleColors.tex}


\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}

%\usetheme{Madrid}
\usetheme{Frankfurt}
%\usetheme{kuleuven2}

%\definecolor{WITcolor}{cmyk}{0.9, 0.94, 0.02, 0.07}
%\setbeamercolor{section in head/foot}{bg=WITcolor}

\AtBeginSection[]
{
	\begin{frame}
		\frametitle{Table of Contents}
		\tableofcontents[currentsection]
	\end{frame}
}

\begin{document}
	\title[Risk-averse MPC of bioreactors]{Risk-averse model predictive control of bioreactors}
	\subtitle{Interim thesis presentation}
	\author[]{Maurits Descamps\\[\baselineskip] 
		\parbox[t]{2.5cm}{\raggedleft
			Supervised by\texorpdfstring{\\[2\baselineskip]}{}
			Guided by
		}
		\hspace{0.2cm}
		\parbox[t]{4cm}{\raggedright
			Prof. Jan Van Impe
			Prof. Pantelis Sopasakis\texorpdfstring{\\[\baselineskip]}{}
			Dr. Satyajeet Bhonsale
			Dr. Mihaela Sbarciog
		}
	}
	\institute[]{
		Departent of Computer Science\\
		KU Leuven}
	\date{December 14, 2020}
	
	\begin{frame}[plain,noframenumbering]
		\titlepage
	\end{frame}
	
	% Table of Contents
	\begin{frame}{Outline}
		\hfill	{\large \parbox{.961\textwidth}{\tableofcontents[hideothersubsections]}}
	\end{frame}
	
	\section{Bioreactors}
	\begin{frame}[label=do]{General bioreactor}
		\begin{columns}
			\begin{column}{0.6\textwidth}
				\begin{itemize}
					\item Vessel containing microorganisms involved in biochemical reactions
					\item Microbial growth
					\begin{itemize}
						\item consumes substrate
						\item produces product
					\end{itemize}
					\item Operation modes:
					\begin{itemize}
						\item<2-> Batch
						\item<3-> Fed-batch
						\item <4-> Continuous
					\end{itemize}
				\end{itemize}
			\end{column}
			\begin{column}{0.4\textwidth}
				\tikzexternaldisable
				\input{../Figures/reactor-beamer.tikz}
				\tikzexternalenable
			\end{column}
		\end{columns}
	\end{frame}
	
	\begin{frame}[label=do]{Continuous bioreactor}
		\label{fr:cbio}
		\begin{columns}
			\begin{column}{0.6\textwidth}
				\begin{itemize}
					\item Feed rate $F[\si{\liter/\hour}]$\\= Effluent rate\\
					$\rightarrow$ constant volume
					\item Feed contains substrate
					\item Effluent contains leftover substrate, product and biomass
					\item Well mixed\\
					$\rightarrow$ uniform concentrations\\
					$\rightarrow$  uniform temperature
				\end{itemize}
			\end{column}
			\begin{column}{0.4\textwidth}
				\input{../Figures/reactor-continuous.tikz}
			\end{column}
		\end{columns}
	\end{frame}
	
	\begin{frame}{Mathematical description}
		\begin{columns}
			\begin{column}{0.45\textwidth} 
				\begin{block}{System dynamics}
					\begin{align*}
						\dot{X} &= \mu(S)X - DX\\
						\dot{S} &= \frac{-1}{Y_x}\mu(S)X + D(S_{in}-S)\\
						\dot{P} &= Y_p\mu(S)X - DP\\
					\end{align*}
					\centering with specific growth rate
					$$\mu(S) =\mu_{max}\frac{S}{K_s+S}$$
				\end{block}
			\end{column}
			\begin{column}{0.55\textwidth}
				\only<1|handout:1>{
					\begin{table}\centering
						\begin{tabular}{lll}
							\multicolumn{3}{c}{Notation}\\\midrule
							$X$ & $\si{\gram/\liter}$ &Biomass conc.\\
							$S$ & $\si{\gram/\liter}$ & Substrate conc.\\
							$P$ &  $\si{\gram/\liter}$ &Product conc.\\
							$S_{in}$ &  $\si{\gram/\liter}$ &\begin{tabular}[t]{@{}c@{}}Feed substrate conc.\end{tabular}\\
							$D=\frac{F}{V}$ & $\si{1/\hour}$  & Dilution rate\\
							$Y_x$, $Y_p$ &  $1$ &Yield coefficients\\
						\end{tabular}
					\end{table}
				}
				\only<2|handout:2>{
					Properties
					\begin{itemize}
						\item Nonlinear
						\item Slow dynamics\\
						$\rightarrow$ typical sample time: 1h
						\item Control input: $D$ (or $S_{in}$)\\
						General notation: $u$
						\item \textbf{Risk} of washout:
						\begin{itemize}
							\item All biomass gone with effluent
							\item Reactions stop
							\item Happens when\\
							$D>\mu(S_{in}) \approx \mu_{max}$
						\end{itemize}
					\end{itemize}
				}
				\only<3|handout:3>{
					Discretization $x_{i+1} = f(x_i, u_i)$
					\begin{itemize}
						\item Implicit Euler
						\item Runge-Kutta
						\item $\dots$
					\end{itemize}
				}
			\end{column}
		\end{columns}
	\end{frame}
	
	
	\section{Control}
	\begin{frame}{Control Problem}
		\begin{block}{}
			Maximize Productivity $P\cdot F [\si{\gram/\hour}]$ or Yield  $\frac{\textrm{grams of product}}{\textrm{grams of substrate}}$\\
			Constraints:
			\begin{itemize}
				\item Limits on $D$
				\item \textit{Avoid washout} (see later)
			\end{itemize}
		\end{block}
	\end{frame}
	
	\begin{frame}[t]{Model predictive control (MPC)}
		At each step:
		\begin{itemize}[<+(1)->]
			\item<2-> Read measurements
			\item<3-4,6-7,9-10> Determine optimal control sequence $\{u_0^\star,\dots,u_{N-1}^\star\}$\\
			$\rightarrow$ constraints
			\item<4,7,10> Execute $u_0^\star$
		\end{itemize}
		\uncover<3->{
			\begin{figure}\centering
				\tikzexternaldisable
				\input{../Figures/mpc.tikz}
				\tikzexternalenable
				\only<3->{\caption{MPC concept\footnote{Figure from MPC lecture notes by Professor Panos Patrinos.}}}
			\end{figure}
		}
	\end{frame}
	
	
	
	
	\section{Dealing with uncertainty}
	\begin{frame}{Sources of uncertainty}
		\begin{columns}
			\begin{column}{0.44\textwidth}
				\begin{block}{System dynamics}
					\begin{align*}
						\dot{X} &= \mu(S)X - DX\\
						\dot{S} &= \frac{-1}{\alert<3>{Y_x}}\mu(S)X + D(\alert<2>{S_{in}}-S)\\
						\dot{P} &= \alert<3>{Y_p}\mu(S)X - DP\\
					\end{align*}
					\centering with specific growth rate
					$$\mu(S) =\alert<4>{\mu_{max}}\frac{ S}{\alert<4>{K_s}+S}$$
				\end{block}
			\end{column}
			\begin{column}{0.56\textwidth}
				Uncertainty in:
				\pause
				\begin{itemize}[<+->]
					\item Feed concentration $S_{in}$
					\item Yield coefficients $Y_x$, $Y_p$
					\item Specific growth $\mu_{max}$, $K_s$
				\end{itemize}
				\onslide<+->{General: random variable $\xi$}\\
				\begin{block}<+>{}
					Initial focus on uncertainty in $S_{in}$
				\end{block}
			\end{column}
		\end{columns}
	\end{frame}
	
	\begin{frame}{Discretization}
		\begin{columns}
			\begin{column}{0.44\textwidth}
				\begin{block}{System dynamics}
					\begin{align*}
						\dot{X} &= \mu(S)X - DX\\
						\dot{S} &= \frac{-1}{Y_x}\mu(S)X + D(S_{in}-S)\\
						\dot{P} &= Y_p\mu(S)X - DP\\
					\end{align*}
					\centering with specific growth rate
					$$\mu(S) =\mu_{max} \frac{S}{K_s+S}$$
				\end{block}
			\end{column}
			\begin{column}{0.56\textwidth}
				Discretization $x_{i+1} = f(x_i,u_i, \xi_{i+1})$\\
				with $\xi_i$ the realization of the uncertainty at stage $i$
			\end{column}
		\end{columns}
	\end{frame}
	
	\begin{frame}[label=current]{Cost functions}
		Total cost: $$\sum_{i=0}^{N-1} \ell(x_i,u_i) + \ell_f(x_N)$$
		\begin{itemize}
			\item Stage cost $\ell(x,u)$
			\item Terminal cost $\ell_f(x)$ 
		\end{itemize}
		$\rightarrow$ both depend on the realization of $\xi$ through $x$
		\begin{block}<2>{}
			How to minimize a random cost?
		\end{block}
	\end{frame}
	
	\begin{frame}{Two popular approaches}
		\begin{figure}\centering
			\input{../Figures/worstcase.tikz}
			\vspace{-1cm}
		\end{figure}
		\begin{columns}[t]
			\begin{column}<2->{0.45\textwidth}
				\begin{block}{Stochastic}
					\begin{itemize}
						\item Minimize expected value of the cost
						\item Downside: Exact knowledge of probability distributions needed
					\end{itemize}
				\end{block}
			\end{column}
			\begin{column}<3->{0.45\textwidth}
				\begin{block}{Robust}
					\begin{itemize}
						\item Minimize worst-case cost\\
						\item Downside:\\
						very conservative
					\end{itemize}
				\end{block}
			\end{column}
		\end{columns}
		\begin{block}<4>{}
			How to deal with high risk, low probability events?
		\end{block}
	\end{frame}
	
	\begin{frame}{Risk measures}
		\begin{itemize}[<+->]
			\item Mapping $\rho \colon$ random variable  $Z \to \REAL$
			\item Examples:
			\begin{itemize}[<+(-1)->]
				\item Essential maximum: $\essmax[Z]$
				\item Expected value: $\EX[Z]$
				\item Value-at-risk: $\VAR \alpha[Z]= \inf \{t : P(Z\leq t)\geq 1- \alpha\}$\\
				$\rightarrow$ for continuous cumulative distribution: $P(Z\geq \VAR)=\alpha$
			\end{itemize}
		\end{itemize}
		\vskip0pt plus 1filll
		\begin{figure}\centering
			\only<4>{
				\input{../Figures/var60.tikz}\\
			}
		\end{figure}
	\end{frame}
	
	\begin{frame}{Average value-at-risk}
		\begin{itemize}[<+->]
			\item $\AVAR_\alpha$: Expected value of $100\alpha\%$ worst cases \footcite[Thm. 6.2]{SHAPIRO}
			\item Interpolation between Expectation based and worst-case
		\end{itemize}
		\vskip0pt plus 1filll
		\begin{figure}\centering
			%\fontsize{8pt}{9pt}\selectfont%
			\only<1-2|handout:1>{
				\input{../Figures/avar60.tikz}\\
			}
			\only<3|handout:2>{
				\input{../Figures/avar15.tikz}\\
			}
			\only<4|handout:3>{
				\input{../Figures/avar90.tikz}\\
			}
		\end{figure}
	\end{frame}
	
	\begin{frame}{Multistage MPC}
		\begin{itemize}
			\item Nested risk measures
			\item Using conditional risk measures $\rho_{\mid\xi_{i}}$
		\end{itemize}
		
		\begin{align*}
			\only<1,4|handout:1>{
				\text{General}&:\quad \min_{u_0} \ell(x_0,u_0) + \rho_{\mid\xi_0} \bigg[\min_{u_1}\ell(x_1,u_1) \\
				&+ \rho_{\mid\xi_1} \Big[\min_{u_2}\ell(x_2,u_2) +\dots\\
				&+ \rho_{\mid\xi_{N-1}} \big[\ell_f(x_N) \big]\dots \Big] \bigg]\\
			}
			\only<2|handout:0>{
				N=1:\quad \min_{u_0} &\ell(x_0,u_0) + \rho_{\mid\xi_0} \big[\ell_f(x_1) \big]\\
			}
			\only<3|handout:0>{
				N=2:\quad \min_{u_0} &\ell(x_0,u_0) + \rho_{\mid\xi_0} \Big[\min_{u_1}\ell(x_1,u_1) + \rho_{\mid\xi_1} \big[\ell_f(x_2) \big] \Big]\\
			}
		\end{align*}
		$$\text{Subject to}\qquad x_{i+1} = f(x_i, u_i, \xi_{i+1})$$
		\onslide<4> $\rightarrow$ Composition of non-smooth operators!
		
	\end{frame}
	
	
	\section{Discretizing the uncertainty}
	\begin{frame}{Discretizing the uncertainty}
		\begin{itemize}
			\item Reason: Make solution tractable
			\item Assumption: \\
			Uncertainty has a finite sample space $\Omega=\{\omega_1,\dots,\omega_M\}$\\
			with probability vector $p$: $P(\omega_i)=p_i$
			\item Examples:
			\begin{itemize}
				\item One uncertain parameter: $S_{in}\in \{s_1,s_2,s_3\}$\\
				$\rightarrow 3$ elements in $\Omega$
				\item Two uncertain parameters: $S_{in}\in \{s_1,s_2,s_3\}$ and $Y_x \in \{y_1,y_2\}$
				$\rightarrow 6$ elements in $\Omega$
			\end{itemize}
		\end{itemize}
	\end{frame}
	
	
	\begin{frame}{Scenario trees}
		\begin{columns}
			\begin{column}{\linewidth}
				\input{../Figures/tree1.tikz}
			\end{column}
			\hspace*{-0.5\linewidth}
			\begin{column}{0.5\linewidth}
				\only<2>{
					\vspace{0.5cm}
					\begin{block}{Problem}
						Number of nodes $= \bigO(M^N)$\\
						For branching factor $M$ and prediction horizon $N$
					\end{block}
				}
			\end{column}
		\end{columns}
	\end{frame}
	
	\begin{frame}{Stopped process}
		\centering
		\input{../Figures/treeRobust.tikz}
	\end{frame}
	
	\begin{frame}{Scenario tree generation}
		\begin{block}{General idea}
			Minimize some \textit{distance} between the scenario tree and the actual distribution.
		\end{block}
		Different methods:
		\begin{itemize}
			\item (Conditional) random sampling
			\item Quasi-Monte Carlo sampling
			\item Bound-based construction
			\item Moment matching
		\end{itemize}
		Or: Rely on engineering insight
	\end{frame}
	
	\begin{frame}{Discrete risk measures}
		Risk measure $\rho\colon Z \to \REAL$\\
		\onslide<2->{Discrete random variable: $Z\colon \Omega \to \REAL$, $Z(\omega_i) \mapsto Z_i$\\
			Probability vector $p$: $P(\omega_i)=p_i$}
		
		%	\begin{exmp*}<3>
		%		Suppose we are at stage $N-1$ in the tree, and there are three possible levels of $S_{in}$.\\
		%		$\rightarrow \Omega = \{S_{in,1}, S_{in,2}, S_{in,3}\}$\\
		%		$\rightarrow Z(\omega_i) = \ell_f(x_{N,i}) = \ell_f(f(x_{N-1},u_{N-1},S_{in,i}))$
		%	\end{exmp*}
		
	\end{frame}
	
	\begin{frame}{Coherent measures}
		Axioms coherent risk measures\footcite[Sec 6.3]{SHAPIRO}:
		\pause
		\begin{enumerate}[<+->]
			\itemsep1em
			\item Convexity:
			$\rho(tZ+(1-t)Z') \leq t\rho(Z) + (1-t)\rho(Z') \quad \forall t\in [0,1]$
			\item Monotonicity: If $Z_i \leq Z_i'$ for all $i \iffalse \in \N_{[1,n]}\fi$, then $\rho(Z) \leq \rho(Z')$
			\item Translational equivariance: $\rho(Z+a)=\rho(Z)+a$ with $a\in\REAL$
			\item Positive homogeneity: $\rho(tZ)=t\rho(Z)$ with $0<t\in\REAL$
		\end{enumerate}
	\end{frame}
	
	\begin{frame}{Discrete coherent risk measures}
		\begin{itemize}
			\item Dual representation using ambiguity set\footcite[Thm. 6.5]{SHAPIRO} $\ambiguity(p)$ : 
			$$\rho[Z] = \max_{\mu \in \ambiguity(p)}\EX_{\mu}[Z] = \max_{\mu \in \ambiguity(p)}\mu^\intercal Z$$
			\item Ambiguity set for $\AVAR_{\alpha}$:
			\[\ambiguity_{\alpha}^{\AVAR}(p) = \left\{\mu \in \REAL^n \;\middle|\; \sum_{i=1}^n \mu_i=1,\quad 0 \leq \alpha\mu_i \leq p_i\right\}\]
		\end{itemize}
	\end{frame}
	
	\begin{frame}{Discrete coherent risk measures}
		Conic representation of  $\ambiguity(p)$ for coherent risk measures\footcite{SopasakisPantelis2019Rroc}:
		\[\ambiguity = \{\mu \in \REAL^n \mid \exists \nu \in \REAL^r  \colon E\mu + F\nu \preccurlyeq_{\K}b\}\]
		Where $\K$ is a closed convex cone and $E$, $F$ ar matrices.
		\onslide<2->{
			$$\rho[Z] = \max_{\mu \in \ambiguity(p)}\mu^\intercal Z$$
		}
		\onslide<3>{
			$$\implies \rho[Z] = \max_{\mu \in \REAL^n, \nu \in \REAL^r} \left\{\mu^\intercal Z \mid E\mu + F\nu \preccurlyeq_{\K}b\right\}$$
		}
		
	\end{frame}
	
	\iffalse
	\begin{frame}{Discrete risk measure}
		Conic representation $\avar$:
		\begin{equation*}
			\avar_{\alpha}[Z] = \max_{\mu \in \REAL^n} \left\{\mu^\intercal Z \mid
			\begin{bmatrix}
				1_n^\intercal\\
				-1_n^\intercal\\
				I_n\\
				-I_n
			\end{bmatrix}
			\mu \preccurlyeq_{\K}
			\begin{bmatrix}
				1\\
				-1\\
				\frac{1}{\alpha}p\\
				0_n
			\end{bmatrix}
			\right\}
		\end{equation*}
		with $\K=\REAL_{\geq0}^{2n+2}$.
	\end{frame}
	\fi
	
	\begin{frame}{Tractable formulation}
		Using a coherent risk measure and its conic representation:
		\begin{itemize}
			\item Nested formulation becomes tractable
			\item Risk measures become conic constraints
			\item Overall problem still non-convex because of $x_{i+1} = f(x_i, u_i, \xi_{i+1})$
		\end{itemize}
	\end{frame}
	
	\iffalse
	\section{Probabilistic constraints}
	\begin{frame}{Probabilistic constraints}
		Define $G_i = \phi(x_i, u_i)$ at stage $i$.\\
		Suppose we want $G_i \leq 0$ in a probabilistic sense.
		\begin{itemize}
			\item Chance constraints
			\begin{itemize}
				\item Require that $P[G_i >0] \leq \alpha$ ($\rightarrow \VAR$)
				\item Leads to integer programming $\rightarrow$ computationally demanding\footcite{SHEN}
			\end{itemize}
			\item Risk constraints\footcite{SopasakisPantelis2019Rroc}:
			\begin{itemize}
				\item Stage-wise risk constraints: \[r_{\mid \xi_{i-1}}[G_i]\leq 0\]
				\item Multistage nested risk constraints: \[\bar{r}_t[G_i] = r_{\mid\xi_0}[r_{\mid\xi_1}[\dots r_{\mid \xi_{i-1}}[G_i]]]\leq 0\]
			\end{itemize}
		\end{itemize}
	\end{frame}
	\fi
	
	\section{Conclusions}
	\begin{frame}{Overview}
		At every time step:
		\begin{itemize}
			\item Construct scenario tree
			\item Solve optimization problem
			\item Apply first control input
		\end{itemize}
	\end{frame}
	
	\begin{frame}{Outlook}
		\begin{itemize}
			\item Implementation in \textsc{Matlab} using Marietta toolbox \footnote{\href{https://github.com/kul-forbes/risk-averse}{https://github.com/kul-forbes/risk-averse}}
			\item Look into:
			\begin{itemize}
				\item Impact of design of scenario tree on complexity and performance
				\item Choice of $\alpha$ for $\AVAR$
				\item Which solver to use
				\item Which cost function
				\item Which constraints
				\item How to calculate $x_{i+1} = f(x_i, u_i, \xi_{i+1})$
				\item Including other sources of uncertainty
			\end{itemize}
		\end{itemize}
	\end{frame}
	
	\begin{frame}[t,allowframebreaks]
		\frametitle{References}
		\printbibliography
	\end{frame}
	
\end{document}