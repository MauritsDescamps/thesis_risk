\documentclass[]{beamer}
\usepackage[backend=biber, style=authoryear, doi=false,isbn=false,url=false,eprint=false,
sorting=none]{biblatex}
\addbibresource{../biblioThesis.bib}

\usepackage{multirow}
\usepackage{lmodern}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{bm}
\usepackage{standalone}
\usepackage{siunitx}
\usepackage{breqn}
\usepackage{pgfplots}
\pgfplotsset{compat=1.17}
\usepgfplotslibrary{groupplots}
\usepackage{mathtools}
\usepackage{tikz}
\usetikzlibrary{external}
\tikzexternalize[prefix=./build/] 
\usepackage{forest}
\usetikzlibrary{calc}
\usetikzlibrary{arrows.meta}

\DeclarePairedDelimiterX{\rvect}[1]{[}{]}{\,\makervect{#1}\,}
\ExplSyntaxOn
\NewDocumentCommand{\makervect}{m}
{
	\seq_set_split:Nnn \l_tmpa_seq { , } { #1 }
	\setlength\arraycolsep{3pt}\begin{matrix}
		\seq_use:Nn \l_tmpa_seq { & }
	\end{matrix}
}
\ExplSyntaxOff
\beamertemplatenavigationsymbolsempty
%\includeonlyframes{do}

\input{../colors.tex}

%Math operators
\DeclareMathOperator{\avar}{AV@R}
\DeclareMathOperator{\evar}{EV@R}
\DeclareMathOperator{\var}{V@R}
\DeclareMathOperator{\essmax}{ess\,max}
\DeclareMathOperator{\EX}{\mathbb{E}}% expected value
\DeclareMathOperator{\R}{\mathbb{R}}% real numbers
\DeclareMathOperator{\N}{\mathbb{N}}% natural numbers
\DeclareMathOperator{\K}{\mathcal{K}}% cone
\DeclareMathOperator{\A}{\mathcal{A}}% ambiguity set
\DeclareMathOperator{\bigO}{\mathcal{O}}% landau set

\theoremstyle{definition}
\newtheorem*{exmp*}{Example}
\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}

%\usetheme{Madrid}
\usetheme{Frankfurt}
%\usetheme{kuleuven2}

%\definecolor{WITcolor}{cmyk}{0.9, 0.94, 0.02, 0.07}
%\setbeamercolor{section in head/foot}{bg=WITcolor}


\begin{document}


\section{Problem setting}
\begin{frame}[label=do]{Fed-batch reactor}
	\begin{columns}
		\begin{column}{0.6\textwidth}
			\begin{itemize}
				\item Initial concentration of biomass $X$ and substrate $S$ 
				\item Biomass grows and consumes S\\
				$\rightarrow$ production of product $P$
				\item Continuous feed with substrate concentration $S_{in}$
				\item Process control by varying feed rate $u$ $[\si{\liter/\hour}]$
				\item Goal: maximize $P$ at $T_{final}$
			\end{itemize}
		\end{column}
		\begin{column}{0.4\textwidth}
			\tikzexternaldisable
			\input{../Figures/reactor-fed-batch.tikz}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Mathematical description}
	\begin{columns}
		\begin{column}{0.58\textwidth}
			\begin{block}{System dynamics\footnotemark}
				\begin{align*}
					\dot{X} &= \mu(S)X - \frac{u}{V}X,\\
					\dot{S} &= \frac{-\mu(S)}{Y_x}X - \frac{v(S)}{Y_p}X + \frac{u}{V}(S_{in}-S)\\
					\dot{P} &= v(S)X - \frac{u}{V}P\\
					\dot{V} &= u\\
					\midrule
					&\mu(S) =\frac{\mu_{max}S}{K_m + S + S^2/K_i}\\
					&v(S) = \frac{v_{max}S}{K_v + S}
				\end{align*}
			\end{block}
		\end{column}
		\begin{column}{0.42\textwidth}
			\begin{table}\centering
				\begin{tabular}{lll}
					\multicolumn{3}{c}{Notation}\\\midrule
					$X$ & $\si{\gram/\liter}$ &Biomass conc.\\
					$S$ & $\si{\gram/\liter}$ & Substrate conc.\\
					$P$ &  $\si{\gram/\liter}$ &Product conc.\\
					$V$& $\si{\liter}$&Volume\\
					$S_{in}$ &  $\si{\gram/\liter}$ &\begin{tabular}[t]{@{}c@{}}Feed conc. \end{tabular}\\
					$u$ & $\si{\liter/\hour}$  & Feed rate\\
					$Y_x$ &  $1$ &Yield coefficient\\
					$Y_p$ &  $1$ &Yield coefficient\\
					$\mu$ & $\si{1/\hour}$ &Growth rate\\
					$v$&$\si{1/\hour}$&Production rate
				\end{tabular}
			\end{table}
		\end{column}
	\end{columns}
	\footnotetext{\cite{SRINIVASAN2}}
\end{frame}

\begin{frame}{Optimal control}
	\begin{columns}
		\begin{column}{0.3\textwidth}
			Analytical solution for
			\begin{align*}
				T_{final} &= \SI{150}{\hour},\\
				X_0 &= \SI{1}{\gram/\liter},\\
				 S_0 &= \SI{0.5}{\gram/\liter},\\
				 P_0 &= \SI{0}{\gram/\liter},\\
				 V_0 &= \SI{150}{\liter},\\
				 X_{max} &= \SI{3.7}{\gram / \liter}.
			\end{align*}
		\end{column}
		\begin{column}{0.7\textwidth}
			\begin{figure}
				\centering
				\input{../Figures/optimalSolution.tikz}
				\caption{Reproduced from \cite{SRINIVASAN2}}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}{Risk measures}
	\begin{itemize}
		\item A risk measure $\rho$ maps random variables  $Z$ to real numbers\\
		$\rightarrow$ just like the average and maximum operators
		\item Interpolation between stochastic and robust approach
	\end{itemize}
	\begin{figure}\centering
			\input{../Figures/risk.tikz}
	\end{figure}
\end{frame}

\begin{frame}{Coherent risk measures}
	Special class of risk measures with nice properties.
	\begin{block}{Requirements for coherent risk measures\footnotemark}
		\begin{enumerate}
			\itemsep1em
			\item Convexity:
			$\rho(\lambda Z+(1-\lambda)Z') \leq \lambda\rho(Z) + (1-\lambda)\rho(Z') \quad \forall \lambda\in [0,1]$
			\item Monotonicity: If $Z_i \leq Z_i'$ for all $i \iffalse \in \N_{[1,n]}\fi$, then $\rho(Z) \leq \rho(Z')$
			\item Translational equivariance: $\rho(Z+a)=\rho(Z)+a$ with $a\in\R$
			\item Positive homogeneity: $\rho(tZ)=t\rho(Z)$ with $0<t\in\R$
		\end{enumerate}
	\end{block}
	Examples: 
	\begin{itemize}
		\item Average value-at-risk ($\avar$)
		\item Entropic value-at-risk ($\evar$)
	\end{itemize}
	\footnotetext{\cite[Sec. 6.3]{lectures}}
\end{frame}






\begin{frame}{Used scenario tree}
	\begin{figure}
		\input{../Figures/tree4.tikz}
		\vspace*{-6mm}
		\caption{Scenario tree with three possible values of $S_{in}$, branching horizon of three and 27 scenarios.}
	\end{figure}
\end{frame}

\begin{frame}{Optimal feed rate}
		\begin{figure}
		\centering
		\includegraphics[width=8cm]{../../Code/results/stochastic_CxMax1000_CsMax1_RC0.2_initialized/inputs.jpg}
		\caption{Optimized feed rate profile for risk constraint $\alpha=0.2$.}
	\end{figure}
\end{frame}

\begin{frame}
	Similar to analytical solution
	\begin{figure}
		\centering
		\hspace*{-1cm}
		\vspace*{-2cm}
		\input{../Figures/optimalSolution.tikz}
	\end{figure}
\end{frame}

\begin{frame}{Corresponding concentrations}
	\begin{figure}
		\centering
		\includegraphics[width=9cm]{../../Code/results/stochastic_CxMax1000_CsMax1_RC0.2_initialized/States.jpg}
		\caption{Optimal biomass, substrate and product concentrations with risk constraint $\alpha=0.2$.}
	\end{figure}
\end{frame}






\end{document}