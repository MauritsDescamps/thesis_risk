% Simulate RRMPC in different settings
clc
clear
close all
import casadi.*
%rng('default')
rng(0)
addpath('helpFunctions')
%Hyper options
stepMethod = 'crankNicolson'; %euler, RK2, RK4, implicitEuler, crankNicolson
nuType = 'dynamic_nu'; %dynamic_nu or const_nu

%basePath='Results/';
basePath = '../latex/figData/MonteCarlo2';

horizonLength=30;
branchingHorizon=2;
plotFigs=true;
%%
N_MC = 1;
branchingHorizon=1;
constraint_options.type = 'avar'; % regular, avar
constraint_options.alpha = 0.99;
constraint_options.relaxWeight = 0;
objective_options.type = 'stochastic';
objective_options.alpha = 1;
[tt, X_simulated, U_simulated, avgTimes, flags, Nvars] = MPC_MC(N_MC, horizonLength, branchingHorizon, objective_options, constraint_options, nuType, stepMethod, plotFigs);



%%
savePath = getSavePath(basePath, objective_options, constraint_options, stepMethod, horizonLength, branchingHorizon, nuType, N_MC);
if ~exist(savePath, 'dir')
    mkdir(savePath)
end
[f, fzoom] = plotMC(tt, X_simulated,U_simulated);
saveas(f,strcat(savePath,'/fig.png'))
saveas(fzoom,strcat(savePath,'/figZoom.png'))
saveResults(savePath, tt, X_simulated,U_simulated, avgTimes, flags)



%%
basePath = '../latex/figData/MonteCarlo3';
N_MC = 80;
plotFigs=true;
constraint_options.type = 'regular'; % regular, avar
constraint_options.relaxWeight = 0;
alpha_Obj = [0.01 0.05 0.10 0.30 0.60 0.90 0.95 0.99];
%alpha_Obj = [0.05 0.5 0.95];
objective_options.type = 'avar';
branchingHorizon=2;
for a =alpha_Obj
    objective_options.alpha = a;
    fprintf('Robust horizon: %i, Regular objective, Alpha constraints: %4.2f\n', branchingHorizon,a)
    [tt, X_simulated, U_simulated, avgTimes, flags] = MPC_MC(N_MC, horizonLength, branchingHorizon, objective_options, constraint_options, nuType, stepMethod, plotFigs);
    savePath = getSavePath(basePath, objective_options, constraint_options, stepMethod, horizonLength, branchingHorizon, nuType, N_MC);
    if ~exist(savePath, 'dir')
        mkdir(savePath)
    end
    [f, fzoom] = plotMC(tt, X_simulated,U_simulated);
    saveas(f,strcat(savePath,'/fig.png'))
    saveas(fzoom,strcat(savePath,'/figZoom.png'))
    saveResults(savePath, tt, X_simulated,U_simulated, avgTimes, flags)
end

%%
basePath = '../latex/figData/MonteCarlo3';
plotFigs=true;
N_MC = 60;
horizonLength=30;
constraint_options.type = 'avar'; % regular, avar
constraint_options.relaxWeight = 0;
alpha_RC = flip([0.01 0.10 0.30 0.60 0.90 0.99]);
stepMethod = 'euler'; %euler, RK2, RK4, implicitEuler, crankNicolson
%alpha_RC = [0.05 0.5 0.95];
objective_options.type = 'stochastic';
branchingHorizon=1;
for a =alpha_RC
    constraint_options.alpha = a;
    fprintf('Robust horizon: %i, Stochastic objective, Avar constraints: %4.2f\n', branchingHorizon,a)
    [tt, X_simulated, U_simulated, avgTimes, flags] = MPC_MC(N_MC, horizonLength, branchingHorizon, objective_options, constraint_options, nuType, stepMethod, plotFigs);
    savePath = getSavePath(basePath, objective_options, constraint_options, stepMethod, horizonLength, branchingHorizon, nuType, N_MC);
    if ~exist(savePath, 'dir')
        mkdir(savePath)
    end
    [f, fzoom] = plotMC(tt, X_simulated,U_simulated);
    saveas(f,strcat(savePath,'/fig.png'))
    saveas(fzoom,strcat(savePath,'/figZoom.png'))
    saveResults(savePath, tt, X_simulated,U_simulated, avgTimes, flags)
end
%%

N_MC = 80;
horizonLength=30;
branchingHorizon=1;
constraint_options.type = 'avar'; % regular, avar
constraint_options.relaxWeight = 0;
alpha_RC = [0.10 0.30 0.60 0.90 0.95];
objective_options.type = 'avar';
alpha_Obj = [0.05 0.10 0.10 0.30 0.60 0.90 0.95];
for a_RC =alpha_RC
    constraint_options.alpha = a_RC;
    for a_Obj= alpha_Obj
        objective_options.alpha = a_Obj;
        fprintf('Alpha objective: %4.2f,\t Alpha constraints: %4.2f\n', a_Obj, a_RC)
        [tt, X_simulated, U_simulated, avgTimes, flags] = MPC_MC(N_MC, horizonLength, branchingHorizon, objective_options, constraint_options, nuType, stepMethod, plotFigs);
        savePath = getSavePath(basePath, objective_options, constraint_options, stepMethod, horizonLength, branchingHorizon, nuType, N_MC);
        if ~exist(savePath, 'dir')
            mkdir(savePath)
        end
        [f, fzoom] = plotMC(tt, X_simulated,U_simulated);
        saveas(f,strcat(savePath,'/fig.png'))
        saveas(fzoom,strcat(savePath,'/figZoom.png'))
        saveResults(savePath, tt, X_simulated,U_simulated, avgTimes, flags)
    end
end


%%
branchingHorizon=1;
N_MC = 200;
constraint_options.relaxWeight = 1e4;
constraint_options.type = 'regular'; % regular, avar
objective_options.type = 'nominal';

constraint_options.alpha = a;
fprintf("Nominal NMPC, shouldn't take long")
[tt, X_simulated, U_simulated, avgTimes, flags] = MPC_MC(N_MC, horizonLength, branchingHorizon, objective_options, constraint_options, nuType, stepMethod, plotFigs);
savePath = getSavePath(basePath, objective_options, constraint_options, stepMethod, horizonLength, branchingHorizon, nuType, N_MC);
if ~exist(savePath, 'dir')
    mkdir(savePath)
end
[f, fzoom] = plotMC(tt, X_simulated,U_simulated);
saveas(f,strcat(savePath,'/fig.png'))
saveas(fzoom,strcat(savePath,'/figZoom.png'))
saveResults(savePath, tt, X_simulated,U_simulated, avgTimes, flags)











