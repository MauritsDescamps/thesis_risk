% Simulate nominal MPC
clc
clear
close all
import casadi.*
rng('default')

Tfinal = 150;
N = 150;
dT = Tfinal/N;
integratorSteps = 1; %Number of integrator steps per dT

% Dynamics options
stepMethod = 'implicitEuler'; %euler, RK2, RK4, implicitEuler, crankNicolson
nuType = 'dynamic_nu'; %dynamic_nu or const_nu
[dXdt, dx, x, u, Sin] = getDynamics(nuType); %Define dynamics
nx = numel(x);
nu = numel(u);

horizonLength = [5 10 20 30 50 80];
compTimes = zeros(size(horizonLength));
variables = zeros(size(horizonLength));
%horizonLength=5;
tt = linspace(0,Tfinal,1+N*integratorSteps);
X_simulated = zeros(nx,numel(tt)-1,numel(horizonLength));
U_simulated = zeros(nu,N,numel(horizonLength));

%Objective type
Q = zeros(4,4); Q(3,3) = -1;
R = 0.5;

% State bounds
relaxWeight = 1e5;
Cx_relax = 3.7;
if relaxWeight>0
    Cx_max=Inf;
else
    Cx_max=3.7;
end
Cs_max = 1; % To prevent terminal jumps
Cp_max = 1000;
V_max = 1000;
x_min = zeros(4,1);
x_max = [Cx_max; Cs_max; Cp_max; V_max];
% Parameters
X0 = SX.sym('X0',4);
U_last = SX.sym('U_last',1);


for k = 1:numel(horizonLength)
    close all
    figure;
    sgtitle(strcat("Horizon: ",num2str(horizonLength(k))))
    hold on
    initialState = [1; 0.5; 0; 150];
    X_simulated(:,1,k) = initialState;
    u_init = 0.1;
    u_last = 0.028; %first u_{-1}
    allTimes = zeros(N,1);
    for t=0:(N-1)
        horizon = min(horizonLength(k), N-t);
        assert(numel(initialState)==width(Q))
        if t==0 || horizonLength(k)> N-t
            % State Variables
            XX2 = SX.sym('XX', nx,horizon);
            XX = [X0, XX2];
            lbx = repmat(x_min,1,horizon);
            ubx = repmat(x_max,1,horizon);
            % Input Variables
            UU = SX.sym('UU',nu, horizon);
            lbu = zeros(nu*horizon,1);
            ubu = ones(nu*horizon,1);
            
            % Impose system dynamics
            %disp('Imposing dynamics')
            idx = 1;
            g_dynamics = SX.zeros(nx*horizon, 1);
            for i=2:(horizon+1) % traverse all but the root
                g_iplus = XX(:,i) - step(dXdt, XX(:, i-1), XX(:, i), UU(:, i-1), 200, dT, stepMethod);
                g_dynamics(idx:idx+nx-1) = g_iplus;
                idx = idx+nx;
            end
            lbg_dynamics = zeros(nx*horizon,1);
            ubg_dynamics = zeros(nx*horizon,1);
            % Prepare objective
            %disp('Building objective')
            objective = 0;
            for stage = 1:(horizon+1)
                if stage==1
                    dU = UU(stage) - U_last;
                    objective = objective+...
                        mtimes(mtimes(XX(:, stage)',Q),XX(:,stage))+ mtimes(mtimes(dU,R),dU);
                elseif stage==horizon+1
                    objective = objective+mtimes(mtimes(XX(:, stage)',Q),XX(:,stage));
                else
                    dU = UU(stage) - UU(stage-1);
                    objective = objective+mtimes(mtimes(XX(:, stage)',Q),XX(:,stage)) + mtimes(mtimes(dU,R),dU);
                end
            end
            
            
            if relaxWeight>0
                w_soft = SX.sym('w_soft', horizon,1);
                w0_soft = zeros(numel(w_soft),1);
                lbw_soft = zeros(numel(w_soft),1);
                ubw_soft = Inf*ones(numel(w_soft),1);
                g_soft = XX(1,2:end)'-3.7-w_soft;
                lbg_soft = -Inf*ones(numel(g_soft),1);
                ubg_soft = zeros(numel(g_soft),1);
                objective = objective+relaxWeight*sum(w_soft);
            else
                w_soft = [];
                lbw_soft = [];
                ubw_soft = [];
                w0_soft = [];
                g_soft = [];
                lbg_soft = [];
                ubg_soft = [];
            end
            
            w = [XX2(:); UU(:); w_soft(:)];
            lbw = [lbx(:); lbu(:); lbw_soft];
            ubw = [ubx(:); ubu(:); ubw_soft];
            g = [g_dynamics; g_soft];
            lbg = [lbg_dynamics; lbg_soft];
            ubg = [ubg_dynamics; ubg_soft];
            prob = struct('f', objective, 'x', w,'p',[X0; U_last],'g', g);
            solverOptions.ipopt.print_level = 0;
            solverOptions.ipopt.warm_start_init_point = 'yes';
            solverOptions.print_time = 0;
            solver = nlpsol('solver', 'ipopt', prob, solverOptions);
            lam_x = ones(size(w));
            lam_g = ones(size(g));
        end
        if t==0
            w0_x = repmat(initialState,1,horizon);
            w0_u = repmat(u_init,1,horizon);
        else %Except at first stage, initialize using previous solution
            w0_x = Xopt(:,1:horizon);
            w0_u = Uopt(:,1:horizon);
        end
        w0 = [w0_x(:); w0_u(:); w0_soft]; %Bundle initial values of optimization variables
        if t==0
            variables(k) = numel(w0);
        end
        tic
        sol = solver('x0', w0,'p',[initialState; u_last],'lbx', lbw, 'ubx', ubw,...
            'lbg', lbg, 'ubg', ubg, 'lam_x0', lam_x, 'lam_g0', lam_g);
        solverStats = solver.stats();
        time = toc;
        allTimes(t+1) = time;
        fprintf('t: %i,\tHorizon: %i,  Solved in %5.2f s, #Iterations: %i,\tStatus: %s\n', t, min(horizon, N-t),time, solverStats.iter_count, solverStats.return_status)
        %lam_g = sol.lam_g; %Uncomment to reuse
        %lam_x = sol.lam_x;
        nuVar = numel(UU);
        w_opt = full(sol.x);
        if ~solverStats.success
            g_func = Function('g',{w, X0}, {g});
            g_opt = full(g_func(w_opt, initialState));
            
            lbg_violations = find((lbg-g_opt)>1e-4)';
            if numel(lbg_violations)
                fprintf('Lbg violations in: ');
                fprintf(strcat(repmat('%i ', 1, numel(lbg_violations)),'\n'), lbg_violations);
            end
            ubg_violations = find((ubg-g_opt)<-1e-4)';
            if numel(ubg_violations)
                fprintf('Ubg violations in: ')
                fprintf(strcat(repmat('%i ', 1, numel(ubg_violations)),'\n'), ubg_violations);
            end
            lbw_violations = find((lbw-w_opt)>1e-4)';
            if numel(lbw_violations)
                fprintf('Lbw violations in: ')
                fprintf(strcat(repmat('%i ', 1, numel(lbw_violations)),'\n'), lbw_violations);
            end
            ubw_violations = find((ubw-w_opt)<-1e-4)';
            if numel(ubw_violations)
                fprintf('Ubw violations in: ')
                fprintf(strcat(repmat('%i ', 1, numel(ubw_violations)),'\n'), ubw_violations);
            end
        end
        numberOfStateVars = numel(XX2);
        Xopt = w_opt(1:numberOfStateVars);
        Xopt = [initialState, reshape(Xopt, nx, horizon)];
        Uopt = w_opt(numberOfStateVars+1:numberOfStateVars+nuVar)';
        Uopt = reshape(Uopt, nu, horizon);
        u0_star = Uopt(:,1);
        U_simulated(:,1+t,k) = u0_star;
        Sin_sampled = 200;
        options.grid = linspace(dT*t, dT*(t+1), integratorSteps+1);
        problem = struct('x',x,'p',[u; Sin],'ode',dx);
        F = integrator('F', 'idas', problem, options);
        integratorResults = F('x0', initialState, 'p',[u0_star;Sin_sampled]);
        X_simulated(:, 2+t*integratorSteps:1+(t+1)*integratorSteps,k) = max(0,full(integratorResults.xf));
        initialState = max(0,full(integratorResults.xf(:,end)));
        tt(1+t*integratorSteps:1+(t+1)*integratorSteps) = options.grid;
        u_init = u0_star;
        u_last = u0_star;
        plotU = subplot(3,2,[1 3 5]);
        cla(plotU)
        hold on
        stairs(0:t, U_simulated(1,1:t+1,k),'b');
        stairs(t:(t+horizon-1),Uopt(1,:));
        xlim([0,Tfinal]);
        ylim([0,0.15]);
        plotX = subplot(3,2,2);
        cla(plotX)
        hold on
        plot(tt(1:1+(t+1)*integratorSteps), X_simulated(1, 1:1+(t+1)*integratorSteps,k),'b');
        plot(t:(t+horizon),Xopt(1,:));
        plot([0 150], [3.7,3.7],'--r')
        xlim([0,Tfinal]);
        ylim([0,4]);
        plotS = subplot(3,2,4);
        cla(plotS)
        hold on
        plot(tt(1:1+(t+1)*integratorSteps), X_simulated(2, 1:1+(t+1)*integratorSteps,k),'b');
        plot(t:(t+horizon),Xopt(2,:));
        xlim([0,Tfinal]);
        ylim([0,0.6]);
        plotP = subplot(3,2,6);
        cla(plotP)
        hold on
        plot(tt(1:1+(t+1)*integratorSteps), X_simulated(3, 1:1+(t+1)*integratorSteps,k),'b');
        plot(t:(t+horizon),Xopt(3,:));
        xlim([0,Tfinal]);
        ylim([0,1.8]);
    end
    compTimes(k) = mean(allTimes);
end
%% Save results
basePath='../latex/figData/Nominal'; %Lacation f
savePath = sprintf('%s/%s_%s',basePath, stepMethod, nuType);
if relaxWeight>0
    savePath = sprintf('%s_%2.0e', savePath, relaxWeight);
end
if ~exist(savePath, 'dir')
    mkdir(savePath)
end
infoData = [horizonLength' squeeze(X_simulated(3,end,:)) variables' 1000*compTimes'];
fid = fopen(strcat(savePath,'/info.txt'), 'wt');
fprintf(fid,'%i\t%5.3f\t%5.0f\t%5.0f\n',infoData');
fclose(fid);

headerFormat = strcat("Time", repmat('\t%i', 1,numel(horizonLength)), '\n');
bodyFormat = strcat('%5.2f', repmat('\t%8.6f', 1,numel(horizonLength)), '\n');

U_out = [tt(1:end-1)' squeeze(U_simulated)];
fid = fopen(strcat(savePath,'/inputs.txt'), 'wt');
fprintf(fid, headerFormat, horizonLength);
fprintf(fid, bodyFormat, U_out');
fclose(fid);

for i=1:nx
    Xi_out = [tt' squeeze(X_simulated(i,:,:))];
    filename = sprintf('%s/X%i.txt',savePath,i);
    fid = fopen(filename, 'wt');
    fprintf(fid, headerFormat, horizonLength);
    fprintf(fid, bodyFormat, Xi_out');
    fclose(fid);
end