% Get average iteration time and number of optimization variables for
% different tobust horizons
N_MC = 1;
objective_options.alpha = 0.60;
constraint_options.alpha = 0.60;
constraint_options.relaxWeight = 0;
horizonLength=30;
plotFigs=true;
stepMethod = 'crankNicolson'; %euler, RK2, RK4, implicitEuler, crankNicolson
nuType = 'dynamic_nu'; %dynamic_nu or const_nu

Times = zeros(9,1);
numberOfVars = zeros(9,1);

for branchingHorizon=1:3
    constraint_options.type = 'avar';
    objective_options.type = 'stochastic';
    [tt, X_simulated, U_simulated, avgTimes, flags, Nvars] = MPC_MC(N_MC, horizonLength, branchingHorizon, objective_options, constraint_options, nuType, stepMethod, plotFigs);
    Times(1) = avgTimes;
    numberOfVars(1) = Nvars;
    constraint_options.type = 'regular';
    objective_options.type = 'stochastic';
    [tt, X_simulated, U_simulated, avgTimes, flags, Nvars] = MPC_MC(N_MC, horizonLength, branchingHorizon, objective_options, constraint_options, nuType, stepMethod, plotFigs);
    Times(2) = avgTimes;
    numberOfVars(2) = Nvars;
    constraint_options.type = 'avar';
    objective_options.type = 'avar';
    [tt, X_simulated, U_simulated, avgTimes, flags, Nvars] = MPC_MC(N_MC, horizonLength, branchingHorizon, objective_options, constraint_options, nuType, stepMethod, plotFigs);
    Times(3) = avgTimes;
    numberOfVars(3) = Nvars;
    disp(Times')
    disp(numberOfVars')
end

