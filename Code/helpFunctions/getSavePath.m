%Create path string to save Monte Carlo results
function savePath = getSavePath(basePath, objective_options, constraint_options, stepMethod, horizonLength, branchingHorizon, nuType, N_MC)
savePath = sprintf('%s/%s',basePath, objective_options.type);
if strcmp(objective_options.type,'avar')
    savePath = sprintf('%s%i',savePath,100*objective_options.alpha);
end
if strcmp(constraint_options.type,'avar')
    savePath = sprintf('%s_RC%i',savePath,100*constraint_options.alpha);
end
if strcmp(objective_options.type,'nominal')
    nScenarios = 1;
else
    nScenarios = 3^branchingHorizon;
end
savePath = sprintf('%s_%s_%s_N%i_Scenarios%i_%i',savePath, stepMethod,nuType, horizonLength,nScenarios, N_MC);

if constraint_options.relaxWeight>0
    savePath = sprintf('%s_soft%1.0e', savePath, constraint_options.relaxWeight);
end
end

