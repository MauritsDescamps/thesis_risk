function xNext = step(dXdt, x_i, x_iplus, u, w, dT, method)
import casadi.*
switch method
    case 'RK2'
        k1 = dXdt(x_i,u,w);
        xNext = x_i + (dT/2)*(k1 + dXdt(x_i+dT*k1,u,w));
    case 'RK4'
        k1 = dXdt(x_i,u,w);
        k2 = dXdt(x_i+.5*k1*dT,u,w);
        k3 = dXdt(x_i+.5*k2*dT,u,w);
        k4 = dXdt(x_i+k3*dT,u,w);
        xNext = x_i+((k1+2*k2+2*k3+k4)/6)*dT;
    case 'cvodes'
        assert(0)
        stepOptions.grid = [0 dT];
        F = integrator('F', 'cvodes', dXdt, stepOptions);
        integratorResults = F('x0', x_i, 'p',[u;w]);
        xNext = integratorResults.xf;
    case 'implicitEuler'
        xNext = x_i + dT*dXdt(x_iplus, u,w);
    case 'crankNicolson'
        xNext = x_i + (dT/2)*(dXdt(x_iplus, u,w)+dXdt(x_i, u,w));
    otherwise %Euler
        xNext = x_i + dT*dXdt(x_i,u,w);
        
end
end