function objective = makeStochasticObjective(tree, Xk, U,objective_options, u_previous)
objective = 0;
Q = objective_options.Q;
R = objective_options.R;
p_scenarios = tree.getProbabilityOfNode(tree.getLeaveNodes);
for s=1:tree.getNumberOfScenarios
    for nodeId = tree.getScenarioWithID(s)'
        if nodeId==1
            dU = U(nodeId) - u_previous;
            objective = objective+...
                p_scenarios(s)*(mtimes(mtimes(Xk(:, nodeId)',Q),Xk(:,nodeId))+ mtimes(mtimes(dU,R),dU));
        elseif tree.getStageOfNode(nodeId)==tree.getHorizon
            objective = objective+p_scenarios(s)*(mtimes(mtimes(Xk(:, nodeId)',Q),Xk(:,nodeId)));
        else
            ancId = tree.getAncestorOfNode(nodeId);
            dU = U(nodeId) - U(ancId);
            objective = objective+p_scenarios(s)*(mtimes(mtimes(Xk(:, nodeId)',Q),Xk(:,nodeId)) ...
                + mtimes(mtimes(dU,R),dU));
        end
    end
end
end