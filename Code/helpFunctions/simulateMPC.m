% Simulate MPC, stochastic or risk-averse, risk constrained
function [U_simulated, X_simulated, avgTime, flag, Nvars] = simulateMPC(initialState, horizonLength, branchingHorizon,tt, N,objective_options, dXdt, dT, integrator_options, X0, U_last, S_in_options, stepMethod, constraintOptions, solverOptions, u_last, plotFigs, silent)
import casadi.*
flag=0;
u_init = 0.1;
integratorSteps = integrator_options.Nsteps;
nx = numel(initialState);
X_simulated = zeros(numel(tt)-1,nx);
U_simulated = zeros(numel(tt)-1,1);
X_simulated(1,:) = initialState;
totalTime = 0;

tree_options.horizonLength = horizonLength;%min(horizonLength, N-t);
tree_options.branchingHorizon = branchingHorizon;
tree = marietta.ScenarioTreeFactory.generateTreeFromIid(S_in_options.discrete_dist, tree_options);
for t=0:(N-1)
    tic
    assert(numel(initialState)==width(objective_options.Q))
    if t==0% || horizonLength> N-t
        opti = optiVars(); %optiVars is an custom class for keeping track of optimization variables, see definition in optiVars.m
        % State Variables
        XX2 = SX.sym('XX2', nx,tree.getNumberOfNodes-1);
        XX = [X0, XX2];
        lbx = repmat(constraintOptions.x_min,1,tree.getNumberOfNodes-1);
        ubx = repmat(constraintOptions.x_max,1,tree.getNumberOfNodes-1);
        opti = opti.append_w(XX2, lbx, ubx);
        % Input Variables
        UU = SX.sym('UU',1, tree.getNumberOfNonleafNodes);
        lbu = zeros(tree.getNumberOfNonleafNodes,1);
        ubu = ones(tree.getNumberOfNonleafNodes,1);
        opti = opti.append_w(UU, lbu, ubu);
             
        % Impose system dynamics
        %disp('Imposing dynamics')
        idx = 1;
        g_dynamics = SX.zeros(nx*(tree.getNumberOfNodes()-1), 1);
        for iplus=2:tree.getNumberOfNodes() % traverse all but the root
            i = tree.getAncestorOfNode(iplus);
            g_iplus = XX(:,iplus) - step(dXdt, XX(:, i), XX(:, iplus), UU(:, i), S_in_options.values{tree.getValueOfNode(iplus)}, dT, stepMethod);
            g_dynamics(idx:idx+nx-1) = g_iplus;
            idx = idx+nx;
        end
        lbg_dynamics = zeros(nx*(tree.getNumberOfNodes()-1),1);
        ubg_dynamics = zeros(nx*(tree.getNumberOfNodes()-1),1);
        opti = opti.append_g(g_dynamics, lbg_dynamics, ubg_dynamics);
        
        % Prepare objective
        switch objective_options.type
            case 'avar'
                [objective, opti_RO, w0_Obj] = makeRiskObjective(tree, XX, UU, objective_options, U_last);
                opti = opti.append(opti_RO);
            otherwise
                w0_Obj=[];
                objective = makeStochasticObjective(tree, XX, UU, objective_options, U_last);
        end
        
        % Stage-wise risk constraints, soft if relaxWeight>0
        w0_RC = [];
        if strcmp(constraintOptions.type,'avar')
            [soft_obj, opti_RC, w0_RC] = makeRiskConstraints(tree, XX, constraintOptions.Cx_RC.pRisk, constraintOptions.Cx_RC.function, constraintOptions.relaxWeight);
            opti = opti.append(opti_RC);
            objective = objective+soft_obj;
        end
        
        %Soft regular constaints
        w0_soft = [];
        if strcmp(constraintOptions.type,'regular') && constraintOptions.relaxWeight>0
            [obj_soft, opti_soft, w0_soft] = setupSoftConstraints(tree, XX, constraintOptions.Cx_soft, constraintOptions.relaxWeight);
            opti = opti.append(opti_soft);
            objective = objective+obj_soft;
        end
        
        prob = struct('f', objective, 'x', opti.w,'p',[X0; U_last],'g', opti.g);
        solver = nlpsol('solver', 'ipopt', prob, solverOptions);
        %lam_x0 = ones(size(opti.w));
        %lam_g0 = ones(size(opti.g));
        Nvars = numel(opti.w);
    end
    if t==0 || reinitialize
        w0_x = repmat(initialState,1,tree.getNumberOfNodes-1);
        w0_u = repmat(u_init,1,tree.getNumberOfNonleafNodes);
    else %Except at first stage, initialize using previous solution
        w0_x = Xopt(:,2:tree.getNumberOfNodes);
        w0_u = Uopt(:,1:tree.getNumberOfNonleafNodes);
    end
    w0 = [w0_x(:); w0_u(:); w0_Obj; w0_RC; w0_soft]; %Bundle initial values of optimization variables
    sol = solver('x0', w0,'p',[initialState; u_last],'lbx', opti.lbw, 'ubx', opti.ubw,...
        'lbg', opti.lbg, 'ubg', opti.ubg);%, 'lam_x0', lam_x0, 'lam_g0', lam_g0);
    solverStats = solver.stats();
    solTime = toc;
    totalTime = totalTime+solTime;
    if ~silent
        fprintf('t: %i,\tN: %3i,  Time %5.2f s, #Iter: %i,\tStatus: %s, Obj: %g\n', t, min(horizonLength, N-t),solTime, solverStats.iter_count, solverStats.return_status, full(sol.f))
    end
    %lam_g0 = sol.lam_g; %Uncomment to reuse
    %lam_x0 = sol.lam_x;
    nuVar = numel(UU);
    w_opt = full(sol.x);
    reinitialize =  solverStats.iterations.inf_pr(end)>1e-2;
    if reinitialize
        fprintf(2,'Big infeasibility\n')
    end
    if ~solverStats.success
        flag=1;
        g_func = Function('g',{opti.w, X0}, {opti.g});
        dispViolations(solverStats, opti.lbg, opti.ubg, opti.lbw, opti.ubw, w_opt, initialState, g_func)
    end
    numberOfStateVars = numel(XX2);
    Xopt = w_opt(1:numberOfStateVars);
    Xopt = [initialState, reshape(Xopt, nx, tree.getNumberOfNodes-1)];
    Uopt = w_opt(numberOfStateVars+1:numberOfStateVars+nuVar)';
    Uopt = reshape(Uopt, 1, tree.getNumberOfNonleafNodes);
    u0_star = Uopt(:,1);
    U_simulated(1+t) = u0_star;
    Sin_sampled = random(S_in_options.dist);
    options.grid = linspace(dT*t, dT*(t+1), integratorSteps+1);
    F = integrator('F', 'idas', integrator_options.problem, options);
    integratorResults = F('x0', initialState, 'p',[u0_star;Sin_sampled]);
    if min(full(integratorResults.xf))<0
        fprintf(2,'Negative concentration!\n')
    end
    X_simulated(2+t*integratorSteps:1+(t+1)*integratorSteps,:) = full(integratorResults.xf);
    initialState = max(0,full(integratorResults.xf(:,end)));
    tt(1+t*integratorSteps:1+(t+1)*integratorSteps) = options.grid;
    u_init = u0_star;
    u_last = u0_star;
    if plotFigs
        plotMPC(tree, tt, t, integratorSteps, tt(end), Xopt, Uopt, X_simulated, U_simulated)
    end
end
avgTime = totalTime/N;