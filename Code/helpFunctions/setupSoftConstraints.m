function [obj_soft, opti_soft, w0] = setupSoftConstraints(tree, XX, Xmax, relaxWeight)
import casadi.*
opti_soft = optiVars();
w = SX.sym('w_soft', tree.getNumberOfNodes-1,1);
w0 = zeros(numel(w),1);
lbw = zeros(numel(w),1);
ubw = Inf*ones(numel(w),1);
opti_soft = opti_soft.append_w(w, lbw, ubw);

g = XX(1,2:end)'-Xmax-w;
lbg = -Inf*ones(numel(g),1);
ubg = zeros(numel(g),1);
opti_soft = opti_soft.append_g(g, lbg, ubg);

obj_soft = relaxWeight*sum(w);
end