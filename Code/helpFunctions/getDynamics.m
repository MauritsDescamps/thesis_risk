function [dXdt, dx, x, u, Sin] = getDynamics(nuType)
import casadi.*
% Create dynamics
x = SX.sym('x',4);
u = SX.sym('u');
Sin = SX.sym('Sin'); % Inlet substrate concentration
% Bioprocess kinetic parameters
Yx = 0.5; Yp = 1.2;
v_max = 0.004; Kv=1e-7;
X = x(1); S = x(2); P = x(3); V = x(4);
% Haldane Kinetics
mu_max = 0.02; Km = 0.05; Ki = 5; % Haldane Kinetics Parameters
mu = mu_max*S/(Km + S + (S.^2)/Ki);
switch nuType
    case 'const_nu'
        v=v_max;
    otherwise
        v = v_max*S/(Kv+S);
end

dX = mu.*X - (u./V).*X;
dS = - mu.*X/Yx - v.*X/Yp + (u./V)*(Sin - S);
dP = v*X - (u./V)*P;
dV = u;
dx = [dX;dS;dP;dV];

dXdt = Function('ODE',{x,u,Sin},{dx});

end

