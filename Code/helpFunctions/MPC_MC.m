% Run N_MC Monte Carlo simulations
function [tt, X_simulated, U_simulated, avgTimes, flags, Nvars] = MPC_MC(N_MC, horizonLength, branchingHorizon, objective_options, constraintOptions, nuType, stepMethod, plotFigs)
import casadi.*
%Objective type
Q = zeros(4,4); Q(3,3) = -1;
objective_options.Q = Q;
objective_options.R = 0.5;

%Stochastic options
S_in_options.values = {150,200,250};
S_in_options.discrete_dist = [0.25 0.5 0.25];
Sin_dist = makedist('Normal');
Sin_dist.mu = 200;
Sin_dist.sigma = 25;
Sin_dist = truncate(Sin_dist, Sin_dist.mu-2*Sin_dist.sigma, Sin_dist.mu+2*Sin_dist.sigma);
S_in_options.dist = Sin_dist;

[dXdt, dx, x, u, Sin] = getDynamics(nuType); %Define dynamics
integrator_options.problem = struct('x',x,'p',[u; Sin],'ode',dx);
nx = numel(x);

Tfinal = 150;
N = 150;
dT = Tfinal/N;
integrator_options.Nsteps = 1; %Number of integrator steps per dT

%solverOptions.ipopt.tol=1e-12;
solverOptions.ipopt.print_level = 0;
solverOptions.ipopt.warm_start_init_point = 'no';
solverOptions.print_time = 0;

if strcmp(objective_options.type,'nominal')
    S_in_options.discrete_dist = [0 1 0]; %Results in only one branch
    branchingHorizon = 1; %Branching horizon should be zero, but that is not allowed in Marietta
end


% Constraints
switch constraintOptions.type
    case 'avar'
        constraintOptions.Cx_RC.function = @(x) x(1)-3.7;
        constraintOptions.Cx_RC.pRisk = marietta.ParametricRiskFactory.createParametricAvarAlpha(constraintOptions.alpha);
        Cx_max = 5;
    case 'regular'
        if constraintOptions.relaxWeight>0
            Cx_max=5;
            constraintOptions.Cx_soft=3.7;
        else
            Cx_max=3.7;
        end
end

initialState = [1; 0.5; 0; 150];
Cs_max = 1; % To prevent terminal jumps
Cp_max = 2.5;
V_max = 180;
constraintOptions.x_min = [0; 0; 0; 0.9*initialState(4)];
constraintOptions.x_max = [Cx_max; Cs_max; Cp_max; V_max];
% Parameters
X0 = SX.sym('X0',4);
U_last = SX.sym('U_last',1);
X_simulated = zeros(N_MC,1+N*integrator_options.Nsteps,nx);
U_simulated = zeros(N_MC,N);
tt = linspace(0,Tfinal,1+N*integrator_options.Nsteps);
avgTimes = zeros(1,N_MC);
flags = zeros(1,N_MC);
silent=true;
for k = 1:N_MC
    close all
    if plotFigs
        figure;
        hold on
        sgtitle(sprintf("MC trial: %i",k))
    end
    u_last = 0.03; %first u_{-1}
    MPCtimer = tic;
     [U, X, avgTime, flag, Nvars] = simulateMPC(initialState, horizonLength, branchingHorizon, tt, N, objective_options, dXdt, dT, integrator_options, X0, U_last, S_in_options, stepMethod, constraintOptions, solverOptions, u_last, plotFigs, silent);
    fprintf('MonteCarlo iteration: %i out of %i\t %5.2f s\n', k, N_MC,toc(MPCtimer))
    X_simulated(k,:,:) = X;
    U_simulated(k,:) = U;
    avgTimes(k) = avgTime;
    flags(k) = flag;
end