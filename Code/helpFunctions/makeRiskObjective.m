% Create nested risk objective and accompanying optimization variables (optiRO) and
% initial values (w0_RO)
function [objective, opti_RO, w0_RO] = makeRiskObjective(tree, Xk, U, objective_options, U_last)
import casadi.*
opti_RO = optiVars();
pRisk = marietta.ParametricRiskFactory.createParametricAvarAlpha(objective_options.alpha);
Q = objective_options.Q;
R = objective_options.R;

risk_cache = repmat(pRisk(1), tree.getNumberOfNonleafNodes, 1);
dual_dim = zeros(tree.getNumberOfNonleafNodes, 1);
iterNonLeaf = tree.getIteratorNonleafNodes();
while iterNonLeaf.hasNext()
    nodeId = iterNonLeaf.next();
    pi = tree.getConditionalProbabilityOfChildren(nodeId);
    risk = pRisk(pi);
    %risk.compress;   % (optional step)
    risk_cache(nodeId) = risk;
    dual_dim(nodeId) = length(risk.getData.b);
end
dual_dim_cumsum = cumsum(dual_dim);
idx_dual_vector = [1; 1+dual_dim_cumsum(1:end)];

% Dual Variable for AVAR Dual Cone: Positive Orthant
Y = SX.sym('Y_RO',(sum(dual_dim)));
lbY = zeros(numel(Y),1);  %Dual Cone: Positive Orthant Y>=0
ubY = inf*ones(numel(Y),1);
Y0 = ones(numel(Y),1);
opti_RO = opti_RO.append_w(Y, lbY, ubY);
% Relaxed Variable: S
S = SX.sym('S_RO',(tree.getNumberOfNodes));
lbS = -inf*ones(numel(S),1);
ubS = inf*ones(numel(S),1);
S0 = ones(numel(S),1);
opti_RO = opti_RO.append_w(S, lbS, ubS);
% Relaxed Variable: T
T = SX.sym('T',(tree.getNumberOfNodes));
lbT = -inf*ones(numel(T),1);
ubT = inf*ones(numel(T),1);
T0 = ones(numel(T),1);
opti_RO = opti_RO.append_w(T, lbT, ubT);

% Relaxation
size_g_RO_relax = tree.getNumberOfNodes+tree.getNumberOfNonleafNodes-1;
g_RO_relax = SX.zeros(size_g_RO_relax,1);
lbg_RO_relax = zeros(size_g_RO_relax,1);
ubg_RO_relax = zeros(size_g_RO_relax,1);
iterNonLeaf.restart()
index=1;
while iterNonLeaf.hasNext()
    nodeId = iterNonLeaf.next();
    riskDataNodeId = risk_cache(nodeId).getData;
    childNodeIds = tree.getChildrenOfNode(nodeId);
    idx_range_dual = idx_dual_vector(nodeId):idx_dual_vector(nodeId+1)-1;
    relaxed_Cons1 = riskDataNodeId.E' * Y(idx_range_dual) - (T(childNodeIds) + S(childNodeIds));
    idx_range_g = index:index+numel(childNodeIds)-1;
    g_RO_relax(idx_range_g) = relaxed_Cons1;
    lbg_RO_relax(idx_range_g) = zeros(numel(relaxed_Cons1),1);
    ubg_RO_relax(idx_range_g) = zeros(numel(relaxed_Cons1),1);
    relaxed_Cons2 = Y(idx_range_dual)'*riskDataNodeId.b - S(nodeId);
    index = idx_range_g(end)+1;
    g_RO_relax(index) = relaxed_Cons2;
    lbg_RO_relax(index) = -inf;
    ubg_RO_relax(index) = 0;
    index = index+1;
end
opti_RO = opti_RO.append_g(g_RO_relax, lbg_RO_relax, ubg_RO_relax);

% Terminal Cost Relaxation
nodesAtStageN = tree.getNodesAtStage(tree.getHorizon);
g_RO_terminal = SX.zeros(numel(nodesAtStageN),1);
lbg_RO_terminal = -inf*ones(numel(nodesAtStageN),1);
ubg_RO_terminal = zeros(numel(nodesAtStageN),1);
iterNonLeaf.restart()
for i = 1:numel(nodesAtStageN)
    gTermCost = mtimes(mtimes(Xk(:,nodesAtStageN(i))',Q),Xk(:,nodesAtStageN(i))) - S(nodesAtStageN(i));
    g_RO_terminal(i) = gTermCost;
end
opti_RO = opti_RO.append_g(g_RO_terminal, lbg_RO_terminal, ubg_RO_terminal);

% (2) Stage t=0..N-1: Z_t <= tau_t+1
size_g_RO_stage = tree.getNumberOfNodes-1;
g_RO_stage = SX.zeros(size_g_RO_stage,1);
lbg_RO_stage = -inf*ones(size_g_RO_stage,1);
ubg_RO_stage = zeros(size_g_RO_stage,1);
index=1;
iterNonLeaf.restart();
while iterNonLeaf.hasNext()
    nodeId = iterNonLeaf.next();
    childNodeIds = tree.getChildrenOfNode(nodeId);
    for j = 1:numel(childNodeIds)
        i_plus = childNodeIds(j);
        if tree.getStageOfNode(i_plus) == tree.getHorizon
            gStageCons = mtimes(mtimes(Xk(:, nodeId)',Q),Xk(:,nodeId)) - T(i_plus);
        else
            dU = U(i_plus) - U(nodeId);
            gStageCons = mtimes(mtimes(Xk(:, nodeId)',Q),Xk(:,nodeId)) ...
                + mtimes(mtimes(dU,R),dU) - T(i_plus);
        end
        g_RO_stage(index) = gStageCons;
        index= index+1;
    end
end
opti_RO = opti_RO.append_g(g_RO_stage, lbg_RO_stage, ubg_RO_stage);

dU0 = U(1)-U_last;
objective = mtimes(mtimes(dU0,R),dU0) + S(1);
w0_RO = [Y0(:); S0(:); T0(:)];
end