% Class for keeping track of optimization variables
classdef optiVars
    properties
        g
        lbg
        ubg
        w
        lbw
        ubw
    end
    
    methods
        function obj = optiVars()
            obj.g = [];
            obj.lbg=[];
            obj.ubg=[];
            obj.w = [];
            obj.lbw=[];
            obj.ubw=[];
        end
        
        function obj = append(obj,opti) % Combine two optiVars
            obj.g = [obj.g; opti.g];
            obj.lbg = [obj.lbg; opti.lbg];
            obj.ubg = [obj.ubg; opti.ubg];
            obj.w = [obj.w; opti.w];
            obj.lbw = [obj.lbw; opti.lbw];
            obj.ubw = [obj.ubw; opti.ubw];
        end
        
        function obj = append_w(obj, w, lbw, ubw)
            assert(numel(w)==numel(lbw));
            assert(numel(ubw)==numel(lbw));
            obj.w = [obj.w; w(:)];
            obj.lbw = [obj.lbw; lbw(:)];
            obj.ubw = [obj.ubw; ubw(:)];
        end
        
        function obj = append_g(obj, g, lbg, ubg)
            assert(numel(g)==numel(lbg));
            assert(numel(ubg)==numel(lbg));
            obj.g = [obj.g; g(:)];
            obj.lbg = [obj.lbg; lbg(:)];
            obj.ubg = [obj.ubg; ubg(:)];
        end
    end
end

