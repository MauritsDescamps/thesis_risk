% Create risk constraints and soft objective term (will be zero if
% relaxWeight==0)
function [soft_objective, opti_RC, w0_RC] = makeRiskConstraints(tree, Xk, pRisk, G_costFunc, relaxWeight)
    import casadi.*
    opti_RC = optiVars();
    soft_objective = 0;
    risk_cache = repmat(pRisk(1), tree.getHorizon, 1);
    dual_dim = zeros(tree.getHorizon, 1);
    for i =1:tree.getHorizon
       probabilities = tree.getProbabilityOfNode(tree.getNodesAtStage(i));
       risk = pRisk(probabilities);
       risk_cache(i) = risk;
       dual_dim(i) = length(risk.getData.b);
    end

    % Index of vector Y
    dual_dim_cumsum = cumsum(dual_dim);
    idx_dual_vector = [1; 1+dual_dim_cumsum(1:end)];

    % Dual Variable for AVAR Dual Cone: Positive Orthant
    Y = SX.sym('Y_RC',(sum(dual_dim)));
    Y0 = ones(numel(Y),1);
    lbY = zeros(numel(Y),1);  %Dual Cone: Positive Orthant Y>=0
    ubY = inf*ones(numel(Y),1);
    opti_RC = opti_RC.append_w(Y, lbY, ubY);

    % Eta variable
    Eta = SX.sym('Eta',tree.getNumberOfNodes-1,1);
    Eta0 = ones(numel(Eta),1);
    lbEta = -inf*ones(numel(Eta),1);
    ubEta = inf*ones(numel(Eta),1);
    opti_RC = opti_RC.append_w(Eta, lbEta, ubEta);
    
    w0_soft = [];
    if relaxWeight>0
        % Relax variable
        soft = SX.sym('w_soft_RC',tree.getHorizon,1);
        w0_soft = zeros(tree.getHorizon,1);
        lbSoft = zeros(tree.getHorizon,1);
        ubw_RC = Inf*ones(tree.getHorizon,1);
        opti_RC = opti_RC.append_w(soft, lbSoft, ubw_RC);
        soft_objective = relaxWeight*sum(soft);
    end

    G = SX.zeros(tree.getNumberOfNodes,1);
    size_g_RC = 2*(tree.getNumberOfNodes-1)+tree.getHorizon;
    g_RC = SX.zeros(size_g_RC,1);
    lbg_RC = zeros(size_g_RC,1);
    ubg_RC = zeros(size_g_RC,1);
    idx = 1;
    for i =0:tree.getHorizon-1
        nodesNext = tree.getNodesAtStage(i+1)';
        for nodeId=nodesNext
            G(nodeId) = G_costFunc(Xk(:, nodeId));
        end
        riskNextkData = risk_cache(i+1).getData;
        idx_range_Y = idx_dual_vector(i+1):idx_dual_vector(i+2)-1;
        c1 = riskNextkData.E'*Y(idx_range_Y)-Eta(nodesNext-1);
        g_RC(idx:idx+numel(c1)-1) = c1;
        lbg_RC(idx:idx+numel(c1)-1) = zeros(numel(c1),1);
        ubg_RC(idx:idx+numel(c1)-1) = zeros(numel(c1),1);
        idx = idx+numel(c1);
        if relaxWeight>0
            c2 = Y(idx_range_Y)'*riskNextkData.b - soft(i+1);
        else
            c2 = Y(idx_range_Y)'*riskNextkData.b;
        end
        g_RC(idx) = c2;
        lbg_RC(idx) = -inf;
        ubg_RC(idx) = 0;
        idx = idx+1;
        c3 = G(nodesNext)-Eta(nodesNext-1);
        g_RC(idx:idx+numel(c3)-1) = c3;
        lbg_RC(idx:idx+numel(c3)-1) = zeros(numel(c3),1);
        ubg_RC(idx:idx+numel(c3)-1) = zeros(numel(c3),1);
        idx = idx+numel(c3);
    end
    opti_RC = opti_RC.append_g(g_RC, lbg_RC, ubg_RC);
    w0_RC = [Y0(:); Eta0(:); w0_soft];
end