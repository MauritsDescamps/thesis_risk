function [f, fzoom] = plotMC(tt, X_simulated,U_simulated) %Plot Monte Carlo
f = figure;
subplot(3,2,[1 3 5]);
stairs(tt', squeeze([U_simulated'; U_simulated(:,end)']),'b');
subplot(3,2,2);
hold on
plot(tt,squeeze(X_simulated(:,:,1)))
plot([0,150],[3.7, 3.7],'r--')
subplot(3,2,4);
plot(tt,squeeze(X_simulated(:,:,2)))
subplot(3,2,6);
plot(tt,squeeze(X_simulated(:,:,3)))
fzoom = figure;
subplot(2,1,1)
hold on
plot([0,150],[3.7, 3.7],'r--')
plot(tt,squeeze(X_simulated(:,:,1)))
xlim([70, 150])
ylim([3.68 3.72])
subplot(2,1,2)
hold on
plot([0,150],[1.68, 1.68],'r--')
plot(tt,squeeze(X_simulated(:,:,3)))
xlim([140, 150])
ylim([1.55 1.70])
end

