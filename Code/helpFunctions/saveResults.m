function saveResults(savePath, tt, X_simulated,U_simulated, avgTimes, flags)
[N_MC,~,nx] = size(X_simulated);
infoOut = [avgTimes' flags'];
fid = fopen(strcat(savePath,'/info.txt'), 'wt');
fprintf(fid,'AvgTime\tFlag\n');
format = strcat('%5.2f\t%i\n');
fprintf(fid, format, infoOut');
fclose(fid);

U_out = [tt(1:end-1)' squeeze(U_simulated)'];
fid = fopen(strcat(savePath,'/inputs.txt'), 'wt');
format = strcat('%5.2f', repmat('\t%9.6f', 1,N_MC), '\n');
fprintf(fid, format, U_out');
fclose(fid);

for i=1:nx
    Xi_out = [tt' squeeze(X_simulated(:,:,i))'];
    filename = sprintf('%s/X%i.txt',savePath,i);
    fid = fopen(filename, 'wt');
    format = strcat('%5.2f', repmat('\t%9.6f', 1,N_MC), '\n');
    fprintf(fid, format, Xi_out');
    fclose(fid);
end
end

