function plotOnTree(tree, z, t0)
if (~exist('t0', 'var'))
    t0 = 0;
end
hold on; grid on; plot_spec_ = struct('marker', '.', 'color', [0,0,0,1],'linewidth', 1.5);
for t=1:tree.getHorizon
    iterNodesStageT = tree.getIteratorNodesAtStage(t);
    while iterNodesStageT.hasNext()
        iNode = iterNodesStageT.next();
        ancNode = tree.getAncestorOfNode(iNode);
        plot_spec_.color(4) = 0.05+0.95*tree.getProbabilityOfNode(iNode);
        if iNode > numel(z), return; end
        plot(t0+[t-1 t], [z(ancNode), z(iNode)], plot_spec_);
    end
end
end