function [] = plotMPC(tree, tt, t, integratorSteps, Tfinal, Xopt, Uopt, X_simulated, U_simulated)
plotU = subplot(3,2,[1 3 5]);
cla(plotU)
stairs((0:(t+1))', [U_simulated(1:t+1); U_simulated(t+1)],'b');
plotOnTree(tree, Uopt,t);
xlim([0,Tfinal]);
ylim([0,0.15]);
plotX = subplot(3,2,2);
cla(plotX)
plot(tt(1:1+(t+1)*integratorSteps)', X_simulated(1:1+(t+1)*integratorSteps,1),'b');
%plotOnTree(tree, Xopt(1,:),t);
hold on
xlim([0,Tfinal]);
ylim([3.65,3.75]);
plot([0,150],[3.7,3.7],'r')
plotS = subplot(3,2,4);
cla(plotS)
plot(tt(1:1+(t+1)*integratorSteps)', X_simulated(1:1+(t+1)*integratorSteps,2),'b');
%plotOnTree(tree, Xopt(2,:),t);
xlim([0,Tfinal]);
ylim([0,0.6]);
plotP = subplot(3,2,6);
cla(plotP)
plot(tt(1:1+(t+1)*integratorSteps)', X_simulated(1:1+(t+1)*integratorSteps,3),'b');
%plotOnTree(tree, Xopt(3,:),t);
xlim([0,Tfinal]);
ylim([0,1.8]);
end

