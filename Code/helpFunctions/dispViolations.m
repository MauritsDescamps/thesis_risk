% Display indices of u and w violations
function dispViolations(solverStats, lbg, ubg, lbw, ubw, w_opt, initialState, g_func)
fprintf('Primal infeasibility: %g\n', solverStats.iterations.inf_pr(end))
%fprintf('Dual infeasibility: %g\n', solverStats.iterations.inf_du(end))
g_opt = full(g_func(w_opt, initialState));

lbg_violations = find((lbg-g_opt)>1e-4)';
if numel(lbg_violations)
    fprintf('Lbg violations in: ');
    fprintf(strcat(repmat('%i ', 1, numel(lbg_violations)),'\n'), lbg_violations);
end
ubg_violations = find((ubg-g_opt)<-1e-4)';
if numel(ubg_violations)
    fprintf('Ubg violations in: ')
    fprintf(strcat(repmat('%i ', 1, numel(ubg_violations)),'\n'), ubg_violations);
end
lbw_violations = find((lbw-w_opt)>1e-4)';
if numel(lbw_violations)
    fprintf('Lbw violations in: ')
    fprintf(strcat(repmat('%i ', 1, numel(lbw_violations)),'\n'), lbw_violations);
end
ubw_violations = find((ubw-w_opt)<-1e-4)';
if numel(ubw_violations)
    fprintf('Ubw violations in: ')
    fprintf(strcat(repmat('%i ', 1, numel(ubw_violations)),'\n'), ubw_violations);
end
end