% Calculate equilibrium paths for constant and monod production rate
clc
clear
syms S Sdot
Xmax = 3.7;
V = 150;
v = 0.004;
S_in = 200;
Yx = 0.5; Yp = 1.2;
mu_max = 0.02; Km = 0.05; Ki = 5;
mu = @(S) mu_max*S./(Km + S + (S.^2)/Ki);
u = mu(S)*V;
Sdot = -mu(S)*Xmax/Yx - v*Xmax/Yp + mu(S).*(S_in-S);

Sol1 = double(solve(Sdot==0));
S1 = min(Sol1);
u1 = V*mu(S1);
fprintf('Constant production rate:\n')
fprintf('Equilibrium S: %4.4e\n',S1)
fprintf('Equilibrium u: %4.4e*V\n',mu(S1))
fprintf('Pdot=%4.4e - %4.3e*P\n\n', v*Xmax, u1/V)


nu_max = 0.004;
K_nu = 1e-7;
nu = @(S) nu_max*S/(S+K_nu);
Sdot2 = -mu(S)*Xmax/Yx - nu(S)*Xmax/Yp + (u/V).*(S_in-S);

Sol2 = double(solve(Sdot2==0));
S2 = Sol2(2);

fprintf('Monod production rate:\n')
fprintf('Equilibrium S: %4.4e\n',S2)
fprintf('Equilibrium u: %4.4eV\n',mu(S2))
fprintf('Pdot=%4.4e - %4.3e*P\n', nu(S2)*Xmax, mu(S2))