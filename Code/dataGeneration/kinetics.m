% Generate data for kinetics plots (Monod, Haldane, Blackman, Contois)
close all
clc
Smax = 2;
S = 0:0.01:Smax;
m = 1;
Kmonod = 0.05;
Kb = 0.1;
Kc = 0.05;
K0 = 1;
Km = 0.05;
Ki = 5;
monod = @(s) m*S./(Kmonod+S);
haldane = @(s) m*K0*S./(Km+S+(S.^2)/Ki);
blackman = @(s) (S<=Kb).*(m*S/Kb)+(S>Kb).*m;
X = 0:0.5:4;
contois = @(s,x) m*S./(Kc*x+S);

figure
hold on
plot(S,haldane(S),'b','DisplayName','Haldane');
plot(S,blackman(S),'g','DisplayName','Blackman');
dataContois = zeros(numel(S), numel(X)+1);
dataContois(:,1) = S';
for i=1:numel(X)
    plot(S,contois(S,X(i)),'k','DisplayName',sprintf('Contois, X=%g',X(i)));
    dataContois(:,i+1) = contois(S,X(i)); 
end
dataContois(isnan(dataContois))=0;
legend
plot(S,monod(S),'r','DisplayName','Monod');
plot(S,blackman(S),'g','DisplayName','Blackman');
xlim([-0.2 Smax])
ylim([0 m])
savePath = sprintf('../latex/figData/Kinetics/');
if ~exist(savePath, 'dir')
       mkdir(savePath)
end
dataRegular = [S' monod(S)' haldane(S)' blackman(S)'];

fid = fopen(strcat(savePath,'/inputs.txt'), 'wt');
format = strcat('%5.2f', repmat('\t%6.4f', 1,3), '\n');
fprintf(fid,'S\tMonod\tHaldane\tBlackman\n');
fprintf(fid, format, dataRegular');
fclose(fid);

fid = fopen(strcat(savePath,'/contois.txt'), 'wt');
format = strcat('%5.2f', repmat('\t%6.4f', 1,numel(X)), '\n');
fprintf(fid, format, dataContois');
fclose(fid);

