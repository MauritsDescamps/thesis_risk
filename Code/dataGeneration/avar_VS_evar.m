%Generate data for AVAR/EVAR comparison plot
clc
clear
close all
alpha = 0:0.001:1;
probDist = [0.2 0.3 0.5]';
Z = [100 10 1]';

riskResults = zeros(numel(alpha), 2);
for i=1:numel(alpha)
    Evar = marietta.ConicRiskFactory.createEvar(probDist, alpha(i));
    Avar = marietta.ConicRiskFactory.createAvar(probDist, alpha(i));
    riskResults(i,1) = Avar.risk(Z);
    riskResults(i,2) = Evar.risk(Z);
end
figure
hold on
plot(alpha, riskResults);
yl=yline(max(Z),'--','Max');
yl.LabelHorizontalAlignment = 'left';
yl=yline(probDist'*Z,'--','Mean');
yl.LabelHorizontalAlignment = 'left';
ylim([0,1.2*max(Z)])
xlabel("\alpha")
ylabel("Risk")
legend("AV@R", "EV@R")
dataOut = [alpha' riskResults];
fileName = strcat('AvarEvar', num2str(numel(alpha)));
writematrix(dataOut, strcat('../latex/figData/',fileName,'.txt'), 'Delimiter','tab');


