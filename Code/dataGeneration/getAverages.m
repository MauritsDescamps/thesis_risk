% Collect information such as final product concentration and 
% number of constraint violations from multiple files
clc
clear
aa = 100*[0.01 0.05 0.10 0.30 0.60 0.90 0.95 0.99];
base = '../latex/FigData/MonteCarlo';

%%
averages_Obj = zeros(size(aa));
cv_Obj = zeros(size(aa));
for i=1:numel(aa)
    directory = sprintf('%s/avar%i_crankNicolson_dynamic_nu_N30_Scenarios3_150',base,aa(i));
    X3 = readmatrix(strcat(directory,'/X3.txt'));
    PP = X3(end,2:end);
    averages_Obj(i) = mean(PP);
    X1 = readmatrix(strcat(directory,'/X1.txt'));
    XX = X1(end,2:end);
    cv_Obj(i) = sum(XX>3.7);
end
dataOut = [aa/100; averages_Obj; cv_Obj];
fid = fopen(strcat(base,'/results_AVAR_OBJ.txt'), 'wt');
format = '%.2f %5.3f %i\n';
fprintf(fid, format, dataOut);
fclose(fid);





%%
averages_RC = zeros(size(aa));
cv_RC = zeros(size(aa));
for i=1:numel(aa)
    directory = sprintf('%s/stochastic_RC%i_crankNicolson_dynamic_nu_N30_Scenarios3_150',base,aa(i));
    X3 = readmatrix(strcat(directory,'/X3.txt'));
    PP = X3(end,2:end);
    averages_RC(i) = mean(PP);
    X1 = readmatrix(strcat(directory,'/X1.txt'));
    XX = X1(:,2:end);
    cv_RC(i) = sum(sum(XX>3.7,1)>0);
end
dataOut = [aa/100; averages_RC; cv_RC];
fid = fopen(strcat(base,'/results_AVAR_RC.txt'), 'wt');
format = '%.2f %5.3f %i\n';
fprintf(fid, format, dataOut);
fclose(fid);

%%
directory = sprintf('%s/stochastic_crankNicolson_dynamic_nu_N30_Scenarios3_150/',base);
X3 = readmatrix(strcat(directory,'X3.txt'));
PP = X3(end,2:end);
X1 = readmatrix(strcat(directory,'X1.txt'));
X1 = X1(:,2:end);

