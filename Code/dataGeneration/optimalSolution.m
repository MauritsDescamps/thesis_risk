close all
clc
clear
TSPAN = [0 150];
x0 = [1; 0.5; 0; 150];
S_in = 200;
Yx = 0.5; Yp = 1.2;
mu_max = 0.02; Km = 0.05; Ki = 5;
mu = @(S) mu_max*S./(Km + S + (S.^2)/Ki);
v = 0.004;
S_star = sqrt(Ki*Km);
mu_star = mu(S_star);
Se = 1.6060e-04;

[tt,xx] = ode23s(@(t,x) dxdt_opt_function(t,x), TSPAN, x0);

uu = zeros(size(xx(:,1)));
for i = 1:numel(xx(:,1))
   if tt(i)<76
       uu(i) = (xx(i,4)/(S_in - S_star))*(mu_star/Yx+v/Yp)*xx(i,1);
   elseif tt(i)<80.8
       uu(i) = 0;
   else
       uu(i) = mu(Se)*156;
   end
end

figure
subplot(221);
plot(tt,xx(:,1))
subplot(222);
plot(tt,xx(:,2))
subplot(223);
plot(tt,xx(:,3))
subplot(224);
plot(tt,uu(:))

filename = '../latex/FigData/optimalSolution.txt';
fid = fopen(filename, 'wt');
fprintf(fid, '%f %f %f %f %f %f\n', [tt xx uu]');
fclose(fid);


function dxdt_opt = dxdt_opt_function(t,x)
    S_in = 200;
    Yx = 0.5; Yp = 1.2;
    mu_max = 0.02; Km = 0.05; Ki = 5;
    mu = @(S) mu_max*S/(Km + S + (S.^2)/Ki);
    v = 0.004;
    S_star = sqrt(Ki*Km);
    mu_star = mu(S_star);
    Se = 1.6060e-04;
    %v_max = 0.004; Kv=0.00001;
    %v = @(S) v_max*S/(Kv+S);
    if t<76
        u = (x(4)/(S_in - S_star))*(mu_star*x(1)/Yx+v*x(1)/Yp);
    elseif t<80.8
       u = 0;
    else
       u = mu(Se)*156;
    end
    dxdt_opt = zeros(4,1);
    dxdt_opt(1) = (mu(x(2))-u/x(4))*x(1);
    dxdt_opt(2) = -(mu(x(2))/Yx + v/Yp)*x(1) +(u/x(4))*(S_in-x(2));
    dxdt_opt(3) = v*x(1) - u*x(3)/x(4);
    dxdt_opt(4) = u;
end


