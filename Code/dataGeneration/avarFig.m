% Generate AVAR figs (based on beta distribution)
close all
alpha=[0.15 0.5 0.6 0.9];
var = zeros(size(alpha));
avar = zeros(size(alpha));
delta = 0.01;

f = @(x) betapdf(x,2,5);
xf = @(x) x.*f(x);
intfx = @(x) integral(xf, x, 1);

for i=1:length(var)
    intf = @(t) integral(f, t, 1)-alpha(i);
    var(i) = fsolve(intf, 1-alpha(i));
    avar(i) = intfx(var(i))/alpha(i);
end

x = 0:delta:1;
y = f(x);

plot(x,y,'k')%,'LineWidth',1)
matOut = [x;y];
filename = '../latex/FigData/beta.txt';
fid = fopen(filename, 'wt');
fprintf(fid, '%f %f\n', matOut);
fclose(fid);
alpha
var
avar

