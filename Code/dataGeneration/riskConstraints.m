% Calculations for the examples in the risk constraint chapter

clc
clear
close all
p = 0.70;
q = 0.63;
W = -2;
L = 120;
N=4;
alpha=0.8;

solverOptions = sdpsettings;
solverOptions.cachesolvers=1;
alphaStarOptions.solverOptions = solverOptions;

tree_options.horizonLength = N;
tree_p = marietta.ScenarioTreeFactory.generateTreeFromIid([p 1-p], tree_options);
pi4_p = tree_p.getProbabilityOfNode(tree_p.getNodesAtStage(N));
tree_q = marietta.ScenarioTreeFactory.generateTreeFromIid([q 1-q], tree_options);
pi4_q = tree_q.getProbabilityOfNode(tree_p.getNodesAtStage(N));
G = W*ones(tree_p.getNumberOfNodesAtStage(N),1);
G(end) = L;
expected_p = pi4_p'*G;
expected_q = pi4_q'*G;
fprintf('Expected cost with nominal probabilities: \t%6.2f\n', expected_p)
fprintf('Expected cost with real probabilities: \t\t%6.2f\n', expected_q)

avar = marietta.ParametricRiskFactory.createParametricAvarAlpha(alpha);
risk4 = avar(pi4_p);
sw_risk = risk4.risk(G);
fprintf('Stage wise AV@R: \t\t\t\t%6.2f\n', sw_risk)

risk = getNestedRisk(tree_p, G, alpha, N);
fprintf('Multistage nested AV@R: \t\t\t%6.2f\n', risk)


%% Calculations for the example of the fact that MNRM are not law invariant
clc
alpha=0.8;
p = 0.7;
N=2;
tree_options.horizonLength = 2;
tree = marietta.ScenarioTreeFactory.generateTreeFromIid([p 1-p], tree_options);
riskNested = getNestedRisk(tree, [2 1 1 0], alpha, N);


avar = marietta.ParametricRiskFactory.createParametricAvarAlpha(alpha);
risk4 = avar([p^2 2*p*(1-p) (1-p)^2]);
riskEnd = risk4.risk([2 1 0]);

fprintf('Nested risk: \t%6.4f\n', riskNested)
fprintf('End risk: \t%6.4f\n', riskEnd)


