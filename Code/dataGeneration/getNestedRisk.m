% Calculate nested AVAR risk
function [risk] = getNestedRisk(tree, G, alpha, stage)
    assert(tree.getHorizon>=stage)
    assert(tree.getNumberOfNodesAtStage(stage)==numel(G));
    avar_param = marietta.ParametricRiskFactory.createParametricAvarAlpha(alpha);
    for t=(stage-1):-1:0
        nodes = tree.getNodesAtStage(t);
        Y = zeros(numel(nodes),1);
        idx_Y = 1;
        idx_G=1;
        for i=nodes'
            %children = tree.getNumberOfChildren(i);
            prob = tree.getConditionalProbabilityOfChildren(i);
            avar = avar_param(prob);
            Y(idx_Y) = avar.risk(G(idx_G:(idx_G+numel(prob)-1)));
            idx_Y = idx_Y+1;
            idx_G = idx_G+numel(prob);
        end
        G = Y;
    end
    risk = G;
end