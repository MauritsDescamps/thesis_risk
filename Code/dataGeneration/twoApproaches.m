close all
delta = 0.01;
xmax = 1;
x = 0:delta:1;
y1 = betapdf(x,2,3);
y2 = betapdf(x,12,12);

f1 = figure;
axes1 = axes('Parent',f1);
hold on
plot(x,y1,'color',[255, 123, 91]/255)
plot(x,y2,'color',[52, 52, 178]/255)
set(axes1,'XTick',[],'YTick',[])
matOut = [x;y1;y2];

%%
filename = '../latex/FigData/worstcase.txt';
fid = fopen(filename, 'wt');
fprintf(fid, '%f %f %f\n', matOut);
fclose(fid);
