function [betaStar, count, a, b] = findBeta(tree, beta_nested, lb, ub, betaStarOptions)
count = 0;
a=max(0,lb);
b=min(1,ub);
if beta_nested==1 || tree.getNumberOfNodesAtStage(betaStarOptions.stage)==1
    betaStar=1;
    return
elseif beta_nested==0
    betaStar=0;
    return
end
count=1;
epiNested = getEpi_nested(tree, beta_nested, betaStarOptions);
while count<betaStarOptions.max_iter
    tic
    c =(a+b)/2;
    epiSW = getEpi_stagewise(tree, c, betaStarOptions);
    result_c = epiNested.contains(epiSW);
    if (b-a)<betaStarOptions.tol_bisection
        if result_c
            betaStar=c;
            break;
        else
            c=a;
            epiSW = getEpi_stagewise(tree, c, betaStarOptions);
            result_c = epiNested.contains(epiSW);
            if result_c
                betaStar=c;
                break
            else
                disp("No value found");
                betaStar=20;
                break
                
            end
        end
    elseif result_c
        a=c;
    else
        b=c;
    end
    fprintf('Iter: %i, time: %.2f\n', count, toc)
    count = count+1;
end
if count>=betaStarOptions.max_iter
    disp("No value found");
end
end