function [epi_nested,P_nested]  = getEpi_nested(tree, beta, betaStarOptions)
numberOfNodes = 0;
for s=0:betaStarOptions.stage
    numberOfNodes = numberOfNodes+tree.getNumberOfNodesAtStage(s);
end
nodeVariables = sdpvar(numberOfNodes, 1);
pAvar = marietta.ParametricRiskFactory.createParametricAvarAlpha(beta);
constraints_nested = [];

for s = betaStarOptions.stage-1:-1:0
    iterNodes =  tree.getIteratorNodesAtStage(s);
    while iterNodes.hasNext
        currNode = iterNodes.next;
        probdist_child = tree.getConditionalProbabilityOfChildren(currNode);
        
        riskMeasure = pAvar(probdist_child);
        E = riskMeasure.getData.E;
        b = riskMeasure.getData.b;
        dualDim = length(b);
        yt = sdpvar(dualDim, 1);
        
        childVars = nodeVariables(tree.getChildrenOfNode(currNode));
        currVar = nodeVariables(currNode);
        constraints_nested = [constraints_nested;
            yt>=0;
            E'*yt <= childVars;
            E'*yt >= childVars;
            b'*yt <= currVar];
    end
end
P_nested = Polyhedron(constraints_nested, betaStarOptions.solverOptions);
%P_nested.minHRep;
epi_nested = P_nested.projection([tree.getNodesAtStage(betaStarOptions.stage)',1], betaStarOptions.projection_method);
end

