function SW_array = findBetaArray(tree, nested_array, a, b, betaStarOptions)
    SW_array = zeros(size(nested_array));
    if numel(nested_array)==1
        [SW_array(1), count] = findBeta(tree, nested_array(1), a, b, betaStarOptions);
        fprintf("Beta nested: %5.3f, beta sw: %5.3f\n", nested_array(1), SW_array(1))
    elseif numel(nested_array)==2
        [SW_array(1), count] = findBeta(tree, nested_array(1), a, b, betaStarOptions);
        fprintf("Beta nested: %5.3f, beta sw: %5.3f\n", nested_array(1), SW_array(1))
        [SW_array(2), count] = findBeta(tree, nested_array(2), SW_array(1), b, betaStarOptions);
        fprintf("Beta nested: %5.3f, beta sw: %5.3f\n", nested_array(2), SW_array(2))
    else
        midIndex = ceil(numel(nested_array)/2);
        [SW_array(midIndex), count] = findBeta(tree, nested_array(midIndex), a, b, betaStarOptions);
        fprintf("Beta nested: %5.3f, beta sw: %5.3f\n", nested_array(midIndex), SW_array(midIndex))
        SW_array(1:midIndex-1) = findBetaArray(tree, nested_array(1:midIndex-1), a, SW_array(midIndex), betaStarOptions);
        SW_array(midIndex+1:end) = findBetaArray(tree, nested_array(midIndex+1:end), SW_array(midIndex), b, betaStarOptions);
    end
end

