clc
clear
close all
solverOptions = sdpsettings;
solverOptions.cachesolvers=1;

betaStarOptions.tol_bisection=1e-3;
betaStarOptions.projection_method="fourier";
betaStarOptions.max_iter = 20;
betaStarOptions.lambda=0;
betaStarOptions.solverOptions = solverOptions;

%%
pp = 0:0.05:0.5;
beta_nested = 0:0.05:1;
horizons = 1:2;
for k=1:numel(horizons)
    betaStar = zeros(numel(pp),numel(beta_nested));
    fprintf('Horizon: %i\n', horizons(k))
    for i=1:numel(pp)
        p = pp(i);
        probDist= [p 1-p]';
        tree_options.horizonLength = horizons(k);
        tree_options.branchingHorizon = horizons(k);
        tree = marietta.ScenarioTreeFactory.generateTreeFromIid(probDist, tree_options);
        betaStarOptions.stage=horizons(k);
        tic
        betaStar(i,:) = findBetaArray(tree, beta_nested, 0, 1, betaStarOptions);
    end
    fprintf("p: %g, elapsed time: %g seconds\n", p, toc)
    fileName = sprintf('3D_horizon%i_branch%i_tol%.2e_numberOfBeta%i_numerOfp%i.txt', horizons(k), numel(probDist), betaStarOptions.tol_bisection, numel(beta_nested),numel(pp));
    [pp_mesh,beta_nested_mesh] = ndgrid(pp,beta_nested);
    dataOut = [pp_mesh(:) beta_nested_mesh(:) betaStar(:)];
    %writematrix(dataOut, strcat('../latex/figData/betaStar/',fileName), 'Delimiter','tab');
end

%%
probDist = [0.25 0.5 0.25]';
tree_options.horizonLength = 3;
tree_options.branchingHorizon = 3;
tree = marietta.ScenarioTreeFactory.generateTreeFromIid(probDist, tree_options);
beta_nested = 0:0.1:1;
stages=1; % Calculate results for these horizons (should not exceed the horizon of used the tree)
betaStar = zeros(numel(beta_nested), numel(stages));
executionTimes = zeros(numel(stages),1);
for i=1:numel(stages)
    betaStarOptions.stage=stages(i);
    fprintf('Horizon length: %i\n', stages(i))
    tic
    betaStar(:,i) = findBetaArray(tree, beta_nested, 0, 1, betaStarOptions);
    elapsed = toc;
    fprintf("Average execution time per beta*: %5.2f seconds\n\n", elapsed/numel(beta_nested))
    executionTimes(i) = elapsed;
    fileName = sprintf('horizon%i_branch%i_tol%.2e_numberOfBeta%i.txt', stages(i), numel(probDist), betaStarOptions.tol_bisection, numel(beta_nested));
    dataOut = [beta_nested' squeeze(betaStar(:,i))];
    %writematrix(dataOut, strcat('../latex/figData/betaStar/',fileName), 'Delimiter','tab');
end



