function [epi_stagewise,P_stagewise]  = getEpi_stagewise(tree, beta, betaStarOptions)
pi = tree.getProbabilityOfNode(tree.getNodesAtStage(betaStarOptions.stage));
n=numel(pi);
if beta==0
    epi_stagewise = Polyhedron('A', [eye(n) -ones(n,1)], 'b', zeros(n,1));
    return
end

lambda = betaStarOptions.lambda;
Y_gamma = sdpvar(n+1,1);
t = sdpvar(1,1);
xi = sdpvar(n,1);
if lambda==0
    constraints_stagewise = [xi>=0;
                             Y_gamma(1:n,1)-ones(n,1)*t<=xi;
                             (1-lambda)*(t+(1/beta)*pi'*xi)+lambda*pi'*Y_gamma(1:n,1)<=Y_gamma(n+1,1)];
else
    constraints_stagewise = [xi>=0;
                             beta*t+pi'* Y_gamma(1:n,1)<=beta*Y_gamma(n+1,1)];
end
P_stagewise = Polyhedron(constraints_stagewise, betaStarOptions.solverOptions);
%P_stagewise.minHRep;
epi_stagewise = P_stagewise.projection(1:(n+1), betaStarOptions.projection_method);
end